﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
   public class ResponceResult
    {
        public string returnStatus { get; set; }
        public string UserID { get; set; }
        public int HttpStatusCode { get; set; }
        public int siteId { get; set; }
        public string REFSessionId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public bool CanAccessEssApplication { get; set; }
        public bool ForcePasswordChange { get; set; }
    }
}
