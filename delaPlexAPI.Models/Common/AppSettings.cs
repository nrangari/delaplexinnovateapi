﻿
using System.Configuration;
using System.Web.Configuration;


namespace delaPlexAPI.Models.Common
{
    public static class AppSettings
    {
        // We have all configuration settings in the appSettings section of MBS._config (pointed to from Web._config)
        private const string Configfile = "/Web._config";
        private static Configuration _config = null;

        /// <summary>
        /// Refreshes the MBS._config data so that any changes will be acknowledged when read using this _config class.
        /// </summary>
        private static void Refresh()
        {
            // Force a reload of the changed section. This makes the new values available for reading.
            // This is needed so that the Ops team does not have to restart the app
            // Note: This ONLY makes values available if read from THIS _config namespace; if you
            // read values using the standard Configuration.Appsettings[x] call, it will continue to
            // return the cached value.
            _config = WebConfigurationManager.OpenWebConfiguration(Configfile);
        }

        /// <summary>
        /// Setting
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string Setting(string key)
        {
            // Force a reload of the configuration settings
            Refresh();

            var setting = _config.AppSettings.Settings[key] != null ? _config.AppSettings.Settings[key].Value.ToString() : string.Empty;

            return setting;
        }

       
        /// <summary>
        /// API SERVER URL
        /// </summary>
        public static string JdaServerUrl
        {
            get
            {
                return Setting("jdaServerUrl");
            }
        }

        /// <summary>
        /// API VERSION
        /// </summary>
        public static string JdaApiVersion
        {
            get
            {
                return Setting("jdaApiVersion");
            }
        }

        /// <summary>
        /// </summary>
        public static string ConsumerKey(string key)
        {
            return Setting(key);
        }

        /// <summary>
        /// </summary>
        public static string SecreteKey(string key)
        {
            return Setting(key);
        }

    }
}
