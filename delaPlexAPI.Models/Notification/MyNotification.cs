﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class MyNotification
    {
        public Message Message { get; set; }
        public int Priority { get; set; }
        public string FromFirstName { get; set; }
        public string FromLastName { get; set; }
        public int ID { get; set; }
        public bool IsSystemAlert { get; set; }
        public bool IsRead { get; set; }
        public DateTime Date { get; set; }
    }

    public class Message
    {
        public string Parameters { get; set; }
        public string Text { get; set; }
    }

    public class DeleteNotification
    {
        public string __TYPE { get; set; }
        public int MailID { get; set; }
    }

    public class UpdateNotification
    {
        public int MailID { get; set; }
        public bool read { get; set; }
    }

    public class DeleteNotificationParam
    {
        public string refSessionId { get; set; }
        public List<int> MailId { get; set; }
        public int siteId { get; set; }
    }

}
