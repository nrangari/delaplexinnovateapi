﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class UserInformation
    {

        public int employeeId { get; set; }
        // public string loginName { get; set; }
        public string BadgeNumber { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string nickName { get; set; }
        public string suffix { get; set; }
        public string title { get; set; }
        public int AddressId { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string stateCode { get; set; }
        public int countryId { get; set; }
        public string countryCode { get; set; }
        public string country { get; set; }
        public string zipCode { get; set; }
        public string homePhone { get; set; }
        public string cellPhone { get; set; }
        public string workPhone { get; set; }
        public string email { get; set; }
        public DateTime birthDate { get; set; }
        public DateTime hireDate { get; set; }
        public DateTime seniorityDate { get; set; }
        public string emergencyContactName { get; set; }
        public string emergencyContactNumber { get; set; }
        public string relationship { get; set; }
        public string payRate { get; set; }
        public string shiftPickup { get; set; }
        public DateTime lastPunchDate { get; set; }
        public string SSN { get; set; }
        public string status { get; set; }
        public string LastActivePayPeriod { get; set; }

    }
}
