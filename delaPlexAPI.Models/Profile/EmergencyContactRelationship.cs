﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class EmergencyContactRelationship
    {
        public string ContactCode { get; set; }
        public string ContactType { get; set; }
    }
}
