﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class UpdateUserInformation
    {
        [Required]
        public int id { get; set; }
        public string addressLine2 { get; set; }
        public string AddressLine1 { get; set; }
        public string city { get; set; }

        [Required]
        public string state { get; set; }

        [Required]
        public string countryCode { get; set; }

        public string zipCode { get; set; }
        public string homePhone { get; set; }
        public string cellPhone { get; set; }
        public string workPhone { get; set; }
        public string email { get; set; }
        public string emergencyContactName { get; set; }
        public string emergencyContactNumber { get; set; }

        [Required]
        public string relationship { get; set; }
        public string shiftPickup { get; set; }


    }
}
