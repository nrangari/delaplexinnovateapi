﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class Employee
    {
        [Required]
        public int id { get; set; }

        [Required]
        public string birthDate { get; set; }

        [Required]
        public string hireDate { get; set; }

        [Required]
        public string seniorityDate { get; set; }
        public string minorStatus { get; set; }
        public string managerPassword { get; set; }

        [Required]
        public string badgeNumber { get; set; }
        public string punchValidationCode { get; set; }
        public string schedulingTypeCode { get; set; }
        public string payrollSystemNumber { get; set; }
        public string payGroupID { get; set; }
        public string isManagement { get; set; }
        public string managerInSchedule { get; set; }
        public string generateException { get; set; }
        public string generateAlerts { get; set; }
        public string canWorkUnassignedJobs { get; set; }
        public string ignoreBiometricValidation { get; set; }

    }


    //"ID":1000161,"LastName":"Drake","FirstName":"Nathan","MiddleName":"","NickName":"Nate","Suffix":"Mr.       ",
    //"BadgeNumber":"102030","HomePhone":"22222212","WorkPhone":"","CellPhone":"","Email":"dpme@delaplex.com","SSN":"",
    //"BirthDate":"1987-05-08T00:00:00","EmergencyContactName":"Mrs Drake","EmergencyContactPhone":"+1 404.876.3334",
    //"EmergencyContactRelationship":"s","Uri":"employeeinformation/1000161"

    public class EmployeeInformationParam
    {
     
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        
        public string NickName { get; set; }
        public string Suffix { get; set; }
        public string BadgeNumber { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public string SSN { get; set; }
        public DateTime BirthDate { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string EmergencyContactRelationship { get; set; }
        public string Uri { get; set; }
        public Address Address { get; set; }
        public List<Notifications> Notifications { get; set; }
    }
    public class EmployeeInformation
    {
        public int siteId { get; set; }
        public string refSessionId { get; set; }
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string NickName { get; set; }
        public string Suffix { get; set; }
        public string BadgeNumber { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public string SSN { get; set; }
        public DateTime BirthDate { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string EmergencyContactRelationship { get; set; }
        public Address Address { get; set; }
        public List<Notifications> Notifications { get; set; }
    }

    public class Address
    {
        public int ID { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public country Country { get; set; }
        public  state state { get; set; }
    }

    public class country
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class state
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
    public class Notifications
    {
        public string ID { get; set; }
        public List<Methods> Methods { get; set; }
    }

    public class Methods
    {
        public string ID { get; set; }
        public bool Selected { get; set; }

    }
}

