﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class MyEditedPunches
    {
        public int PunchId { get; set; }
        public string MyTab { get; set; }
        public string ChangeType { get; set; }
        public string OriginalPunch { get; set; }
        public string NewPunch { get; set; }
        public string Job { get; set; }
        public string EditedBy { get; set; }
        public string Reason { get; set; }
        public string Comments { get; set; }
        public string LastEdit { get; set; }
    }

    public class EditedPunches
    {
        public DateTime ? ScheduledShiftEndTime { get; set; }
        public string Comments { get; set; }
        public DateTime ? ScheduledShiftStartTime { get; set; }
        public string ScheduledJob { get; set; }
        public string PunchTypeCode { get; set; }
        public string StatusCode { get; set; }
        public string Reason { get; set; }
        public string NewPunchTime { get; set; }
        public DateTime ? EditedOn { get; set; }
        public string ChangeType { get; set; }
        public string OldJob { get; set; }
        public int ID { get; set; }
        public DateTime ? OldPunchTime { get; set; }
        public string NewJob { get; set; }
        public string EditedBy { get; set; }
        public DateTime ? PunchDate { get; set; }
    }

    public class PunchTypeCode
    {
        public string Code { get; set; }
        public string PunchType { get; set; }
    }

    public class SetPunchEditStatus
    {
        public List<int> PunchEditID { get; set; }
        public int SiteId { get; set; }
        public string StatusType { get; set; }  
        public string REFSSessionID { get; set; }
        public string Comment { get; set; }
    }

    public class SetPunchEditStatusParamAcknowldge
    {
        public int PunchEditID { get; set; }
        public string __TYPE { get; set; }
        public string StatusType { get; set; }

    }
    public class SetPunchEditStatusParamDispute
    {
        public int PunchEditID { get; set; }
        public string __TYPE { get; set; }
        public string StatusType { get; set; }
        public string Comment { get; set; }

    }

}
