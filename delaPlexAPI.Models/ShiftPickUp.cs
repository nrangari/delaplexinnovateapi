﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class ShiftPickUp
    {
        public string ContactMethodTypeCode { get; set; }
        public string ContactMethodTypeName { get; set; }
    }

    public static class ShiftPickUpInformation
    {
        //static readonly List<ShiftPickUp> ListPickUps = new List<ShiftPickUp>();
        public static List<ShiftPickUp> GetShiftPickList()
        {
            List<ShiftPickUp> listPickUps = new List<ShiftPickUp>
            {
                new ShiftPickUp {ContactMethodTypeCode = "w", ContactMethodTypeName = "Work Phone"},
                new ShiftPickUp {ContactMethodTypeCode = "h", ContactMethodTypeName = "Home Phone"},
                new ShiftPickUp {ContactMethodTypeCode = "e", ContactMethodTypeName = "Email"},
                new ShiftPickUp {ContactMethodTypeCode = "c", ContactMethodTypeName = "Cell Phone"},
              //  new ShiftPickUp {ContactMethodTypeCode = "f", ContactMethodTypeName = "Fax"},
                new ShiftPickUp {ContactMethodTypeCode = "t", ContactMethodTypeName = "Text"}
            };
            return listPickUps;
        }
    }
}
