﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class MyUnpairedPunches
    {
        public DateTime Date { get; set; }
        public string Site { get; set; }
        public string Job { get; set; }
        public string Reason { get; set; }
        public int PunchedShiftID { get; set; }
        public DateTime ? ShiftStart { get; set; }


        //Todo - Remove from the list
        public DateTime ? ShiftEnd { get; set; }
        public DateTime ? MealStart { get; set; }
        public DateTime  ? MealEnd { get; set; }
        public DateTime ?  BreakStart { get; set; }
        public DateTime ? BreakEnd { get; set; }
        public string ShiftReason { get; set; }
        public string MealReason { get; set; }
        public string BreakReason { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }

    }
}
