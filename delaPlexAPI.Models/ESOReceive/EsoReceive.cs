﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models.ESOReceive
{
    public class EsoReceive
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public List<InProgress> InProgress { get; set; }
        public List<Completed> Completed { get; set; }
    }



    public class InProgress
    {
        //SupplierId":30041,"Supplier":"EBY Brown, Inc","ReceivingNumber":"68954739012578943957","Expected":"01:30 am","Expected":"Yesterday

        public int SupplierId { get; set; }
        public string Supplier { get; set; }
        public string ReceivingNumber { get; set; }
        public string Expected { get; set; }
        public string ExpectedDay { get; set; }
        
    }

    public class Completed
    {
        //"SupplierId":30041,"Supplier":"EBY ASN Complete","ReceivingNumber":"68954739012578943957","LastEdit":"01/01/19 01:30 am"

        public int SupplierId { get; set; }
        public string Supplier { get; set; }
        public string ReceivingNumber { get; set; }
        public string LastEdit { get; set; }
    }


}

