﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class EmployeeAccruals
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public int TimeOffTypeId { get; set; }

        [Required]
        public int EmployeeId { get; set; }
        public decimal AccruedAmount { get; set; }
        public decimal CarryOverAmount { get; set; }
        public decimal ServiceLevelCarryOverAmount { get; set; }
        public decimal UsedAmount { get; set; }
        public decimal ApprovedAmount { get; set; }
        public decimal RequestedAmount { get; set; }
        public decimal AvailableAmount { get; set; }
    }
}
