﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class EmployeeAvailability
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int employeeID { get; set; }
        public DateTime effectiveFrom { get; set; }
        public DateTime ? endsAfter { get; set; }
        public int numberOfWeeks { get; set; }
        public DateTime cycleBaseDate { get; set; }
        public List<GeneralAvailability> GeneralAvailability { get; set; }
        public List<PreferredAvailability> PreferredAvailability { get; set; }
    }

    public class GeneralAvailability
    {
        public int id { get; set; }
        public string endTimeOffset { get; set; }
        public string startTimeOffset { get; set; }
        public string dayOfWeek { get; set; }
        public int weekNumber { get; set; }

    }

    public class PreferredAvailability
    {
        public int id { get; set; }
        public string  endTimeOffset { get; set; }
        public string  startTimeOffset { get; set; }
        public string dayOfWeek { get; set; }
        public int weekNumber { get; set; }

    }


    
    //{"ID":0,"Status":"RequestedPreferred","RequestedTimestamp":"","Start":"2019-02-15T00:00:00","End":"2019-02-16T00:00:00",
    //"LastStatusUpdateUserID":0,"LastStatusUpdateUserName":"","LastStatusUpdateTimestamp":"","Comment":"","Rank":0,"
    //AvailabilityRequestRange":[
    //{"StartOffset":"00:00:00","EndOffset":"1.00:00:00","DayOfWeek":0,"AvailabilityType":"General"},
    //{"StartOffset":"00:00","EndOffset":"00:15","DayOfWeek":1,"AvailabilityType":"General"},
    //{"StartOffset":"00:00:00","EndOffset":"1.00:00:00","DayOfWeek":3,"AvailabilityType":"General"},
    //{"StartOffset":"00:00:00","EndOffset":"1.00:00:00","DayOfWeek":4,"AvailabilityType":"General"},
    //{"StartOffset":"00:00:00","EndOffset":"1.00:00:00","DayOfWeek":5,"AvailabilityType":"General"},
    //{"StartOffset":"00:00:00","EndOffset":"1.00:00:00","DayOfWeek":6,"AvailabilityType":"General"},
    //{"StartOffset":"00:00","EndOffset":"00:30","DayOfWeek":5,"AvailabilityType":"Preferred"}]}


    public class EmployeeAvailabilityPrivateApi
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public string RequestedTimestamp { get; set; }
        public DateTime Start { get; set; }
        public DateTime ? End { get; set; }
        public int LastStatusUpdateUserID { get; set; }
        public string LastStatusUpdateUserName { get; set; }
        public string LastStatusUpdateTimestamp { get; set; }
        public string Comment { get; set; }
        public int Rank { get; set; }
        public string RefSessionId { get; set; }
        public int SiteId { get; set; }
        public List<AvailabilityRequestRangeParam> availabilityRequestRange { get; set; }
    }


    public class EmployeeAvailabilityPrivateApiPrams
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public string RequestedTimestamp { get; set; }
        public DateTime Start { get; set; }
        public DateTime ? End { get; set; }
        public int LastStatusUpdateUserID { get; set; }
        public string LastStatusUpdateUserName { get; set; }
        public string LastStatusUpdateTimestamp { get; set; }
        public string Comment { get; set; }
        public int Rank { get; set; }
        public List<AvailabilityRequestRangeParam> availabilityRequestRange { get; set; }
    }

    public class AvailabilityRequestRangeParam
    {
        public TimeSpan StartOffset { get; set; }
        public TimeSpan EndOffset { get; set; }
        public string AvailabilityType { get; set; }
        public int DayOfWeek { get; set; }
    }



    public class EmployeeNewAvailability
    {
        //[Required]
        //public int id { get; set; }
        [Required]
        //public int employeeID { get; set; }
        public DateTime effectiveFrom { get; set; }
        public DateTime? endsAfter { get; set; }
        //public int numberOfWeeks { get; set; }
        //public DateTime cycleBaseDate { get; set; }
        public List<GeneralPrefferdAvailability> Availability { get; set; }
    }

    public class GeneralPrefferdAvailability
    {
      //  public int id { get; set; }
        public string dayOfWeek { get; set; }
        public List<AvailabilityDetails> AvailabilityDetails { get; set; }
    }

    public class AvailabilityDetails
    {
        public int weekNumber { get; set; }
        public string AvailabilityType { get; set; }
        public string AvailabilityRangeTypeDesc { get; set; }
        public int AvailabilityRangeType { get; set; }
      //  public List<TimeSpans> TimeSpans { get; set; }
        public string endTimeOffset { get; set; }
        public string startTimeOffset { get; set; }
    }

    public class TimeSpans
    {
        public string endTimeOffset { get; set; }
        public string startTimeOffset { get; set; }
    }


    //public class CancelAvailabilityRequest
    //{
    //    public int siteId { get; set; }
    //    public string refSessionId { get; set; }
    //    public int ID { get; set; }
    //    public string Status { get; set; }
    //    public string RequestedTimestamp { get; set; }
    //    public string Start { get; set; }
    //    public string End { get; set; }
    //    public int LastStatusUpdateUserID { get; set; }
    //    public string LastStatusUpdateUserName { get; set; }
    //    public string LastStatusUpdateTimestamp { get; set; }
    //    public string Comment { get; set; }
    //    public int Rank { get; set; }
    //    public  List<AvailabilityRequestRangeParam> AvailabilityRequestRange { get; set; }
    //}

    //public class CancelAvailabilityRequestParam
    //{
    //    public int ID { get; set; }
    //    public string Status { get; set; }
    //    public string RequestedTimestamp { get; set; }
    //    public string Start { get; set; }
    //    public string End { get; set; }
    //    public int LastStatusUpdateUserID { get; set; }
    //    public string LastStatusUpdateUserName { get; set; }
    //    public string LastStatusUpdateTimestamp { get; set; }
    //    public string Comment { get; set; }
    //    public int Rank { get; set; }
    //    public List<AvailabilityRequestRangeParam> AvailabilityRequestRange { get; set; }
    //}
}
