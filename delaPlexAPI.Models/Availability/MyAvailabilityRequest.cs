﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class MyAvailabilityRequest
    {
        //"ID": 1000113,
        //"Start": "2018-09-23T00:00:00",
        //"LastStatusUpdateUserID": 1000162,
        //"LastStatusUpdateUserName": "Sullivan, HP2",
        //"LastStatusUpdateTimestamp": "2018-08-30T04:58:37",
        //"RequestedTimestamp": "2018-08-30T04:49:50",
        //"End": "2018-10-31T00:00:00",
        //"Comment": "My Availability for September and October Month.",
        //"Status": "Approved"

        [Required]
        public int ID { get; set; }
        public DateTime ? Start { get; set; }
        public int LastStatusUpdateUserID { get; set; }
        public string LastStatusUpdateUserName { get; set; }
        public DateTime ? LastStatusUpdateTimestamp { get; set; }
        public DateTime ? RequestedTimestamp { get; set; }
        public DateTime ? End { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public List<AvailabilityRequestRange> AvailabilityRequestRange { get; set; }
    }

    public class AvailabilityRequestRange
    {
        //"ID": 1000153,
        //"DayOfWeek": 1,
        //"StartOffset": "09:00:00",
        //"EndOffset": "17:00:00",
        //"AvailabilityType": "g"
        public int ID { get; set; }
        public int DayOfWeek { get; set; }
        public string StartOffset { get; set; }
        public string EndOffset { get; set; }
        public string AvailabilityType { get; set; }
    }
}
