﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class TimeOff
    {
        public int timeOffRequestID { get; set; }
        public int timeOffTypeID { get; set; }
        public int employeeID { get; set; }
        public string status { get; set; }
        public string comments { get; set; }
        public double hoursRequested { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string timeOffType { get; set; }
        public string LastModifiedBy { get; set; }

    }

    public class TimeOffType
    {
        public int timeOffTypeID { get; set; }
        public string timeOffType { get; set; }
    }


    public class TimeOffApprover
    {
        public long TimeOffId { get; set; }

        public string LastModifiedBy { get; set; }
    }



    public class TimeOffRequest
    {
        public int timeOffRequestID { get; set; }

        [Required]
        public int timeOffTypeID { get; set; }

        [Required]
        public int employeeID { get; set; }

        [Required]
        public string status { get; set; }

        [Required]
        public DateTime start { get; set; }

        /// <summary>
        /// END date
        /// </summary>
        [Description("End Date/Time"), Required]
        public DateTime end { get; set; }

        public string comments { get; set; }

        public string refSessionId { get; set; }

        public string siteId { get; set; }

        public bool HalfDay { get; set; }

        public DateTime LastModifiedTimestamp { get; set; }
    }

    public class TimeOffRequestValidation
    {
        public int min_days_notice_RTO { get; set; }

        public int tor_max_days { get; set; }

        public int MinDaysNoticeForTimeOffRequests { get; set; }

        public int MinDaysAvailableEmployeeThreshold { get; set; }
        
    }


    public class TorHistoryParam
    {
        public DateTime startTime { get; set; }

        public DateTime ? endTime { get; set; }

        public bool includeRequested { get; set; }

        public bool includeApproved { get; set; }

        public bool includeDenied { get; set; }

        public bool includeCanceled { get; set; }
    }
    public class TorHistoryApiParam
    {
        public DateTime startTime { get; set; }

        public string RefSessionId { get; set; }

        public int siteId { get; set; }
    }

    public class TorHistoryResponce
    {
        public DateTime Start { get; set; }

        public DateTime Submitted { get; set; }

        public DateTime? End { get; set; }

        public int TimeOffID { get; set; }

        public string StatusCode { get; set; }
    }


    public class TimeOffRequestData
    {
        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public bool HalfDay { get; set; }

        public bool IsTrackedAsWholeDays { get; set; }

        public string Comment { get; set; }

        public DateTime LastModifiedTimestamp { get; set; }

        public int EmployeeID { get; set; }

        public int ID { get; set; }

        public int PayAdjustmentID { get; set; }

        public string StatusCode { get; set; }

        public string PayAdjustmentName { get; set; }

        public List<TimeOffRequestComments> timeOffRequestComments { get; set; }
    }


    public class TimeOffRequestComments
    {
        public DateTime Timestamp { get; set; }
        public int AuthorID { get; set; }
        public string StatusCode { get; set; }
        public string AuthorFirstName { get; set; }
        public string AuthorLastName { get; set; }
        public string Comment { get; set; }
    }

    public class AddTimeOffRequest
    {
        public string __TYPE { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool HalfDay { get; set; }
        public int TimeOffTypeID { get; set; }
    }
    public class UpdateTimeOffRequest
    {
        public string __TYPE { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool HalfDay { get; set; }
        public string TimeOffTypeID { get; set; }
        public int TorID { get; set; }
        public string Comment { get; set; }
        public string LastModifiedTimestamp { get; set; }
        
    }
    public class CancelTimeOffRequest
    {
        public int TorID { get; set; }
        public string __TYPE { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TimeOffTypeID { get; set; }
        public DateTime LastModifiedTimestamp { get; set; }
    }

}
