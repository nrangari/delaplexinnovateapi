﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public static class TimeOffRequestStatusCode
    {
        public static string TimeOffStatusCode(string code)
        {
            string CodeDescription = string.Empty;
            switch (code)
            {
                case "r":
                    CodeDescription = "Pending";
                    break;
                case "a":
                    CodeDescription = "Approved";
                    break;
                case "d":
                    CodeDescription = "Denied";
                    break;
                case "c":
                    CodeDescription = "Cancelled";
                    break;
            }

            return CodeDescription;
        }

    }
}
