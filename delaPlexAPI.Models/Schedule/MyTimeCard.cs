﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class MyTimeCard
    {
        public decimal ? NetPunchedHours { get; set; }
        public decimal? NetPayAdjustmentHours { get; set; }
        
        public List<PayPunchedShift> payPunchedShifts { get; set; }
    }

    public class PayPunchedShift
    {

        public int PunchedShiftID { get; set; }
        public string shiftDate { get; set; }
        public DateTime ? Start { get; set; }
        public DateTime ? End { get; set; }
        public decimal ? NetPunchedHours { get; set; }
        public List<Details> details { get; set; }
    }


    public class Details
    {
        public int ID { get; set; }
        public string DetailType { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public decimal NetHours { get; set; }
    }

}
