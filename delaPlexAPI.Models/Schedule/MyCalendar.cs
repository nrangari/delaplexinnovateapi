﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class MyCalendar
    {
        public List<ScheduledJobs> MySchedules { get; set; }
        public List<PendingMyApprovals> PendingMyApprovals { get; set; }
        public List<AvailableShifts> AvailableShifts { get; set; }
        public List<RequestedByMe> RequestedByMe { get; set; }
        public MyTimeCard MyTimeCard { get; set; }
        
        public decimal NetScheduledHours { get; set; }
    }
}
