﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class ProcessShiftSwap
    {
        [Required]
        public string refSessionId { get; set; }

        [Required]
        public int siteId { get; set; }

        [Required]
        public List<int> RequestedScheduledShiftIDs { get; set; }

        [Required]
        public int OriginalScheduledShiftID { get; set; }
    }

    public class ProcessCollection
    {
        public string __TYPE { get; set; }
        public List<int> RequestedScheduledShiftIDs { get; set; }
        public int OriginalScheduledShiftID { get; set; }
    }
}
