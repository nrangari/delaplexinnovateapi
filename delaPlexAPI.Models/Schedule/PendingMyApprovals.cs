﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class PendingMyApprovals
    {
        public string BuUnit { get; set; }
        public string Swapper { get; set; }
        public string SwappedWith { get; set; }
        public string Status { get; set; }
        public string GainedShift { get; set; }
        public string LostShift { get; set; }
        public string Role { get; set; }
        public int SwapId { get; set; }
        public int GainedShiftId { get; set; }
        public int LostShiftId { get; set; }
        public string StatusCode { get; set; }
    }


    public class PendingMyApprovalData
    {
        public string BuUnit { get; set; }
        public string Swapper { get; set; }
        public string SwappedWith { get; set; }
        public string Status { get; set; }
        public DateTime GainedShiftShiftEnd { get; set; }
        public DateTime GainedShiftShiftStart { get; set; }
        public DateTime LostShiftShiftStart { get; set; }
        public DateTime LostShiftShiftEnd { get; set; }
        public string Role { get; set; }
        public int SwapId { get; set; }
        public int GainedShiftId { get; set; }
        public int LostShiftId { get; set; }
        public string DetailedStatus { get; set; }
    }

    public class RequestedByMe
    {
        public string BuUnit { get; set; }
        public string Swapper { get; set; }
        public string SwappedWith { get; set; }
        public string Status { get; set; }
        public string GainedShift { get; set; }
        public string LostShift { get; set; }
        public string Role { get; set; }
        public int SwapId { get; set; }
        public int GainedShiftId { get; set; }
        public int LostShiftId { get; set; }
        public string StatusCode { get; set; }
    }

}
