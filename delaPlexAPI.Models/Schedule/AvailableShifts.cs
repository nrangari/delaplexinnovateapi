﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class AvailableShifts
    {
        public int ShiftId { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public string Date { get; set; }
        public string Job { get; set; }
        public string Site { get; set; }
    }
}
