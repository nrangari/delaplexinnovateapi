﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class MySchedule
    {
        public ScheduledShiftResource ScheduledShiftResource { get; set; }
    }

    public class ScheduledShiftResource
    {
        public int ScheduledShiftId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int SiteId { get; set; }
        public ScheduledJobResource scheduledJobResource { get; set; }
    }

    public class ScheduledJobResource
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int JobId { get; set; }
        public List<ScheduledDetailResource> ListScheduledDetailResource { get; set; }
    }

    public class ScheduledDetailResource
    {
        public string DetailTypeCode { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }


    public class ScheduledJobs
    {
        public int scheduledShiftId { get; set; } 
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public string Hours { get; set; }
        public List<ScheduledDetail> scheduledDetails { get; set; }
    }

    public class ScheduledDetail
    {
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public string detailTypeCode { get; set; }
    }

    public class ScheduleDates
    {
        public int Dates { get; set; }
    }

    public class ScheduleHours
    {
        public int ShiftId { get; set; }
        public DateTime Date { get; set; }
        public string Hours { get; set; }
        public decimal DayHour { get; set; }
        
    }

}
