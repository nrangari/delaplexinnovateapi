﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Models
{
    public class ShiftSwap
    {
        public string ShiftEnd { get; set; }
        public string ShiftDate { get; set; }
        public string ShiftStart { get; set; }
        public bool FixedShift { get; set; }
        public decimal NetHoursDelta { get; set; }
        public string JobNames { get; set; }
        public int ShiftID { get; set; }
        public string EmployeeName { get; set; }
        public string JobColor { get; set; }
    }

    public class UpdateShiftSwap
    {
        [Required]
        public string refSessionId { get; set; }

        [Required]
        public int siteId { get; set; }

        [Required]
        public int SwapShiftID { get; set; }

        [Required]
        public string ActionFlag { get; set; }

        [Required]
        public int RequestedScheduledShiftID { get; set; }
    }

    public class ProcessShiftSwapRequest
    {
        public string __TYPE { get; set; }
        public int SwapShiftID { get; set; }
        public int RequestedScheduledShiftID { get; set; }
        public string RequestStatus { get; set; }
        
    }
}
