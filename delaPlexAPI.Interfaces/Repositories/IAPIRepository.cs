﻿using delaPlexAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Interfaces.Repositories
{
    public interface IApiRepository
    {
        UserInformation GetEmployeeProfile(string employeeId);
        string UpdateUserProfileInformation(UpdateUserInformation userInformation);
        UserInformation GetEmployeeInformation(int EmployeeId, string employeeRefSessionId, int siteId);
        List<State> GetStateList(int employeeId);
        List<MyUnpairedPunches> GetUnpairedPunches(string refSessionId, int siteId);//(int employeeId);
        List<ShiftPickUp> GetShiftPickUp();
        List<EmergencyContactRelationship> GetEmergencyContactRelationshipCode();
        string GetMySchedule(int employeeId, DateTime date);
        List<ShiftSwap> GetMyShiftSwaps(int employeeId);
        List<MyEditedPunches> GetMyEditedPunches(int employeeId);
        List<TimeOff> GetMyTimeOff(int employeeId, string flag);
        List<ScheduledJobs> GetMyScheduleFormatted(int employeeId, DateTime date);
        List<EmployeeAccruals> GetEmployeeAccruals(int employeeId);
        List<ScheduleDates> GetMonthlyScheduleDate(int month);
        List<TimeOffType> GetAdjustmentTypes(int siteId, string refSessionId);
        string CreateTimeOffRequest(TimeOffRequest timeOff);
        List<PendingMyApprovals> GetPendingMyApprovals(int employeeId);
        List<RequestedByMe> RequestedByMe(int employeeId);
        List<AvailableShifts> GetAvailableShift(int employeeId);
        int GetRTOPeriod();
        MyTimeCard GetMyTimeCard(string refSessionId, int siteId, DateTime date);
        string ClaimAvailableShift(ClaimShift claimShift);
        List<ShiftSwap> ShiftSwapAvailableShift(string refSessionId, int shiftID, int siteId);
        List<EditedPunches> GetEditedPunches(string refSessionId, int siteId, DateTime date, string statusCode);
        List<MyAvailabilityRequest> GetMyAvailabilityRequest(string refSessionId, int siteId);
        List<MyNotification> GetMyNotification(string refSessionId, int siteId);
        string DeleteMyNotification(DeleteNotificationParam deleteNotificationParam);
        string ProcessShiftSwap(ProcessShiftSwap processShiftSwap);
        EmployeeAvailability GetMyCurrentAvailability(int Employeeid, DateTime date);
        string UpdateSwapShiftRequest(UpdateShiftSwap updateShiftSwap);
        string CreateEmployeeAvailability(EmployeeAvailability availability);
        string MarkNotificationReadUnread(UpdateNotification updateNotification);
        decimal GetScheduledHours(string refSessionId, int siteId, DateTime date);
        List<PunchTypeCode> GetPunchTypeCode();
        List<AvailableShifts> GetUnfilledShift(int employeeId, DateTime date, int siteId, string refSessionId);
        TimeOffRequestValidation GetRTOandTorPeriod(int siteId, string refSessionId);
        string UpdateTimeOffRequest(TimeOffRequest timeOff);
        string CancelTimeOffRequest(TimeOffRequest timeOff);
        string UpdateTimeOffRequest_v1(TimeOffRequest timeOff);
        string CreateTimeOffRequest_v1(TimeOffRequest timeOff);

        List<TimeOff> GetFutureTimeOff(int employeeId, string refSessionId, int siteId);

        string CreateEmployeeAvailabilityUsingPrivateAPI(EmployeeAvailabilityPrivateApi availability);
        string UpdateEmployeeInformation(EmployeeInformation employeeInformation);
        List<PendingMyApprovals> GetPendingApprovals(int siteId, string refSesssionId);
        List<RequestedByMe> RequestedByMe(int siteId, string refSesssionId);
        List<State> GetStates(int siteId, int countryId, string refSessionId);
        EmployeeNewAvailability GetMyAvailability(int siteId, string refSessionId);//(int Employeeid, DateTime date);
        string UpdateEmployeeAvailabilityRequest(EmployeeAvailabilityPrivateApi cancelAvailabilityRequest);
        List<TimeOff> GetMyHistoryTimeOff(int siteId, string refSessionId);
    }
}