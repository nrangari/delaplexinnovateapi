﻿using delaPlexAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.DataAccess
{
    public class SqlDataHelper
    {
        SqlConnection _SqlConn;
        DataSet _ds;
        protected bool _disposed = false;

       
        private SqlConnection GetConnection()
        {
            string str = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            SqlConnection sqlconn = new SqlConnection(str);
            
            return sqlconn;
        }

        public DataSet ExecuteDataset(SqlParameter[] sqlparam, string sp)
        {
            _SqlConn = (SqlConnection)GetConnection();

            using (_SqlConn = (SqlConnection)GetConnection())
            {
                using (SqlDataAdapter _sqlDa = new SqlDataAdapter())
                {
                    SqlCommand cmd = new SqlCommand();

                    foreach (SqlParameter sqlparams in sqlparam)
                    {
                        cmd.Parameters.Add(sqlparams);
                    }

                    try
                    {
                        cmd.CommandText = sp;
                        _sqlDa.SelectCommand = cmd;
                        _sqlDa.SelectCommand.Connection = _SqlConn;
                        _sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                        _ds = new DataSet();
                        _sqlDa.Fill(_ds);
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }

            return _ds;
        }
        public DataSet ExecuteDataset(string sp)
        {
            _SqlConn = (SqlConnection)GetConnection();

            using (_SqlConn = (SqlConnection)GetConnection())
            {
                using (SqlDataAdapter _sqlDa = new SqlDataAdapter())
                {
                    SqlCommand cmd = new SqlCommand();
                    try
                    {
                        cmd.CommandText = sp;
                        _sqlDa.SelectCommand = cmd;
                        _sqlDa.SelectCommand.Connection = _SqlConn;
                        _sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                        _ds = new DataSet();
                        _sqlDa.Fill(_ds);
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }

            return _ds;
        }

        public DataSet ExecuteDatasetQuery(string sp)
        {
            _SqlConn = (SqlConnection)GetConnection();

            using (_SqlConn = (SqlConnection)GetConnection())
            {
                using (SqlDataAdapter _sqlDa = new SqlDataAdapter())
                {
                    SqlCommand cmd = new SqlCommand();
                    try
                    {
                        cmd.CommandText = sp;
                        _sqlDa.SelectCommand = cmd;
                        _sqlDa.SelectCommand.Connection = _SqlConn;
                        _sqlDa.SelectCommand.CommandType = CommandType.Text;
                        _ds = new DataSet();
                        _sqlDa.Fill(_ds);
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }

            return _ds;
        }

        public void ExecuteNonQuery(SqlParameter[] sqlparam, string sp)
        {
            _SqlConn = (SqlConnection)GetConnection();

            using (_SqlConn = (SqlConnection)GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    try
                    {
                        cmd.CommandText = sp;
                        cmd.Connection = _SqlConn;
                        cmd.CommandType = CommandType.StoredProcedure;

                        foreach (SqlParameter sqlparams in sqlparam)
                        {
                            cmd.Parameters.Add(sqlparams);
                        }
                        _SqlConn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                // Need to dispose managed resources if being called manually
                if (disposing)
                {
                    if (_SqlConn != null)
                    {
                        _SqlConn.Dispose();
                        _SqlConn = null;
                    }
                }
                _disposed = true;
            }
        }

        #endregion
    }
}
