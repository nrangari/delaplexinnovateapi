﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(delaPlexAPI.Startup))]

namespace delaPlexAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
