﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using delaPlexAPI.Interfaces.Repositories;
using delaPlexAPI.Models;
using Newtonsoft.Json;
using System.Linq;
using System.Runtime.CompilerServices;


namespace delaPlexAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/MobileUser")]
    public class MobileUserController : ApiController
    {
        readonly IApiRepository _apiRepository;

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="apiRepository"></param>
        public MobileUserController(IApiRepository apiRepository)
        {
            _apiRepository = apiRepository;
        }


        #endregion

        #region Profile



        /// <summary>
        /// This will use to update the employee information using private API
        /// </summary>
        /// <param name="employeeInformation"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateEmployeeInformation")]
        public string UpdateEmployeeInformation(EmployeeInformation employeeInformation)
        {
            return _apiRepository.UpdateEmployeeInformation(employeeInformation);
        }


        /// <summary>
        /// Get User profile information - Using Private and public API
        /// </summary>
        /// <param name="employeeId">Internal Employee Id</param>
        /// <param name="refSessionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal Site Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("UserProfileInformation")]
        public UserInformation UserProfileInformation(int employeeId, string refSessionId, int siteId)
        {
            return _apiRepository.GetEmployeeInformation(employeeId, refSessionId, siteId);
            //return _apiRepository.GetEmployeeProfile(employeeId);
            //var userInformation = _apiRepository.GetEmployeeProfile(employeeId);
            //return JsonConvert.SerializeObject(userInformation);
        }

        /// <summary>
        /// Update user profile information - Using Pubic API
        /// </summary>
        /// <param name="userInformation"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateUserProfileInformation")]
        public string UpdateUserProfileInformation(UpdateUserInformation userInformation)
        {
            return _apiRepository.UpdateUserProfileInformation(userInformation);
        }

        /// <summary>
        /// This function will return the State list based on employeeId -- Using SP - Replaced with public API 
        /// </summary>
        /// <param name="employeeId">Internal Employee Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStateList")]
        public List<State> GetStateList(int employeeId)
        {
            return _apiRepository.GetStateList(employeeId);
        }

        /// <summary>
        /// Get the shift Pick up default options,coded in the middleware
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetShiftPickUp")]
        public List<ShiftPickUp> GetShiftPickUp()
        {
            return _apiRepository.GetShiftPickUp();
        }

        /// <summary>
        /// This will relation ship codes - Using SP
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetEmergencyContactRelationshipCode")]
        public List<EmergencyContactRelationship> GetEmergencyContactRelationshipCode()
        {
            return _apiRepository.GetEmergencyContactRelationshipCode();
        }


        #endregion

        #region UnPaired Punches


        /// <summary>
        /// Get Unpaired punches of employee - Using Private API
        /// </summary>
        /// <param name="refSessionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal siteId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUnpairedPunches")]
        public List<MyUnpairedPunches> GetUnpairedPunches(string refSessionId, int siteId)//(int employeeId)
        {
            return _apiRepository.GetUnpairedPunches(refSessionId,siteId);//(employeeId);
        }



        #endregion

        #region Commented Code


        //[HttpGet]
        //[Route("GetMySchedule")]
        //public string GetMySchedule(int employeeId, DateTime date)
        //{
        //    return _apiRepository.GetMySchedule(employeeId, date);
        //}


        /// <summary>
        /// This API will return edited Punches - using SP
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("GetMyEditedPunches")]
        //public List<MyEditedPunches> GetMyEditedPunches(int employeeId)
        //{
        //    return _apiRepository.GetMyEditedPunches(employeeId);
        //}

        ///// <summary>
        ///// This will return the requested Shift swap - Using SP
        ///// </summary>
        ///// <param name="employeeId">Employee Internal Id</param>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("GetMyShiftSwaps")]
        //public List<ShiftSwap>  GetMyShiftSwaps(int employeeId)
        //{
        //    return _apiRepository.GetMyShiftSwaps(employeeId);
        //}

        //[HttpGet]
        //[Route("GetAvailableShift")]
        //public List<AvailableShifts> GetAvailableShift(int employeeId)
        //{
        //    return _apiRepository.GetAvailableShift(employeeId);
        //}


        /// <summary>
        /// Get Request time off period - Using SP
        /// </summary>
        /// <returns></returns>
        //[HttpGet]
        //[Route("GetRTOPeriod")]
        //public int GetRTOPeriod()
        //{
        //    return _apiRepository.GetRTOPeriod();
        //}
        #endregion

        #region MySchedule


        /// <summary>
        /// This will the pending approvals using private API
        /// </summary>
        /// <param name="refSesssionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal Site Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPendingApprovals")]
        public List<PendingMyApprovals> GetPendingApprovals(int siteId, string refSesssionId)
        {
            return _apiRepository.GetPendingApprovals(siteId, refSesssionId);
        }


        /// <summary>
        /// RequestedByMe
        /// </summary>
        /// <param name="refSesssionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal Site Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("RequestedByMe")]
        public List<RequestedByMe> RequestedByMe(int siteId, string refSesssionId)
        {
            return _apiRepository.RequestedByMe(siteId, refSesssionId);
        }

        /// <summary>
        /// This API used to Approve/Deny the Shift swap request
        /// Using Private API for this
        /// </summary>
        /// <param name="processShiftSwap"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ProcessShiftSwap")]
        public string ProcessShiftSwap(ProcessShiftSwap processShiftSwap)
        {
            return _apiRepository.ProcessShiftSwap(processShiftSwap);
            //return _apiRepository.ProcessShiftSwap(refSessionId, siteId, RequestedScheduledShiftIDs, OriginalScheduledShiftID);
        }


        /// <summary>
        /// This method used to update the shift swap request
        /// Using Private API for this
        /// Flags to update the request
        /// "A" - requestStatus = "AwaitingManager";
        /// "D": requestStatus - "RecipientDenied";
        /// "C"  requestStatus = "SenderCanceled";+
        /// SwapId - SwapShiftID
        /// RequestedScheduledShiftID - GainedShiftId 
        /// </summary>
        /// <param name="updateShiftSwap"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSwapShiftRequest")]
        public string UpdateSwapShiftRequest(UpdateShiftSwap updateShiftSwap)
        {
            return _apiRepository.UpdateSwapShiftRequest(updateShiftSwap);
        }



        /// <summary>
        /// This return the Available Shift - Using private API 
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <param name="date">Day of week</param>
        /// <param name="refSessionId">Token to access Private API's</param>
        /// <param name="siteId">Employee site ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUnfilledShift")]
        public List<AvailableShifts> GetUnfilledShift(int employeeId, DateTime date, string refSessionId, int siteId)
        {
            return _apiRepository.GetUnfilledShift(employeeId, date, siteId, refSessionId);
        }


        /// <summary>
        /// Retrieve scheduled shifts of an active Employee for a labor week using the employee's internal ID
        /// API's are in the call
        /// GetMyScheduleFormatted , GetPendingMyApprovals ,RequestedByMe
        /// GetUnfilledShift ,GetMyTimeCard ,GetScheduledHours
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <param name="date">Day of week</param>
        /// <param name="refSessionId">Token to access Private API's</param>
        /// <param name="siteId">Employee site ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyCalendar")]
        public MyCalendar GetMyCalendar(int employeeId, DateTime date, string refSessionId, int siteId)
        {

            var mySchedule = _apiRepository.GetMyScheduleFormatted(employeeId, date);
            var myPendingApprovals = _apiRepository.GetPendingApprovals(siteId, refSessionId);
            //var myPendingApprovals = _apiRepository.GetPendingMyApprovals(employeeId);
            var requestedByMe = _apiRepository.RequestedByMe(siteId, refSessionId);
            // var availableShift = _apiRepository.GetAvailableShift(employeeId);

            var availableShift = _apiRepository.GetUnfilledShift(employeeId, date, siteId, refSessionId);
            var myTimeCard = _apiRepository.GetMyTimeCard(refSessionId, siteId, date);
            var totalScheduledHours = Math.Round(_apiRepository.GetScheduledHours(refSessionId, siteId, date), 2);

            var myCalendar = new MyCalendar
            {
                MySchedules = mySchedule,
                PendingMyApprovals = myPendingApprovals,
                RequestedByMe = requestedByMe,
                AvailableShifts = availableShift,
                MyTimeCard = myTimeCard,
                NetScheduledHours = totalScheduledHours
            };

            return myCalendar;
        }

        /// <summary>
        /// This will return schedule of the employee - Using Public API
        /// </summary>
        /// <param name="employeeId">Employee Internal ID</param>
        /// <param name="date">Schedule Date</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMySchedule")]
        public List<ScheduledJobs> GetMySchedule(int employeeId, DateTime date)
        {
            return _apiRepository.GetMyScheduleFormatted(employeeId, date);
        }

        /// <summary>
        /// This will return monthly scheduled dates - Using SP
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("GetMonthlyScheduleDate")]
        //public List<ScheduleDates> GetMonthlyScheduleDate(int month)
        //{
        //    return _apiRepository.GetMonthlyScheduleDate(month);
        //}

        /// <summary>
        /// This will return the time card of the Employee - Using Private API
        /// </summary>
        /// <param name="refSessionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal Site Id</param>
        /// <param name="date">Calender Date</param>
        /// <returns></returns>
        [HttpGet]
        [Route("MyTimeCard")]
        public MyTimeCard MyTimeCard(string refSessionId, int siteId, DateTime date)
        {
            return _apiRepository.GetMyTimeCard(refSessionId, siteId, date);
        }

        /// <summary>
        /// This will return the pending Approvals - Using SP
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("GetPendingMyApprovals")]
        //public List<PendingMyApprovals> GetPendingMyApprovals(int employeeId)
        //{
        //    return _apiRepository.GetPendingMyApprovals(employeeId);
        //}


        /// <summary>
        /// This will return the request requested by Employee - Using SP
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("GetRequestedByMe")]
        //public List<RequestedByMe> GetRequestedByMe(int employeeId)
        //{
        //    return _apiRepository.RequestedByMe(employeeId);
        //}



        /// <summary>
        /// This method used to claim the available Shifts
        /// </summary>
        /// <param name="claimShift"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ClaimAvailableShift")]
        public string ClaimAvailableShift(ClaimShift claimShift)
        {
            return _apiRepository.ClaimAvailableShift(claimShift);
        }



        /// <summary>
        /// This method will return the all available shift to swap with the current shift
        /// </summary>
        /// <param name="refSessionId">Token to access private API</param>
        /// <param name="shiftID">Internal shift ID</param>
        /// <param name="siteId">Internal Site ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAvailableShiftForShiftSwap")]
        public List<ShiftSwap> GetAvailableShiftForShiftSwap(string refSessionId, int shiftID, int siteId)
        {
            return _apiRepository.ShiftSwapAvailableShift(refSessionId, shiftID, siteId);
        }

        #endregion

        #region TimeOff
        /// <summary>
        /// This will return available time off Types - using Private API of persona
        /// </summary>
        /// <param name="refSessionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal Site Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTimeOffTypes")]
        public List<TimeOffType> GetTimeOffTypes(int siteId, string refSessionId)
        {
            return _apiRepository.GetAdjustmentTypes(siteId, refSessionId);
        }



        /// <summary>
        /// Update the timeoff Request using public API 
        /// </summary>
        /// <param name="timeOff"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateTimeOffRequest")]
        public string UpdateTimeOffRequest(TimeOffRequest timeOff)
        {
            return _apiRepository.UpdateTimeOffRequest(timeOff);
        }


        /// <summary>
        /// Update time off request using Private API
        /// </summary>
        /// <param name="timeOff"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateTimeOffRequest_v1")]
        public string UpdateTimeOffRequest_v1(TimeOffRequest timeOff)
        {
            return _apiRepository.UpdateTimeOffRequest_v1(timeOff);
        }

        /// <summary>
        /// Cancel the time off request using public API
        /// </summary>
        /// <param name="timeOff"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CancelTimeOffRequest")]
        public string CancelTimeOffRequest(TimeOffRequest timeOff)
        {
            return _apiRepository.CancelTimeOffRequest(timeOff);
        }



        /// <summary>
        /// To create new time off request - Using Private API
        /// Earlier  request was processed by client admin role 
        /// </summary>
        /// <param name="timeOff"> New Timeoff </param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateTimeOffRequest_v1")]
        public string CreateTimeOffRequest_v1(TimeOffRequest timeOff)
        {
            return _apiRepository.CreateTimeOffRequest_v1(timeOff);
        }


        /// <summary>
        /// To create new time off request - Using Public API
        /// </summary>
        /// <param name="timeOff"> New Timeoff </param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateTimeOffRequest")]
        public string CreateTimeOffRequest(TimeOffRequest timeOff)
        {
            return _apiRepository.CreateTimeOffRequest(timeOff);
        }

        /// <summary>
        /// Get List of Time Off Requests for the given employeeId in a specfic year
        /// This will return the future time off - using Public API and SP
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("GetMyFutureTimeOff")]
        //public List<TimeOff> GetMyFutureTimeOff(int employeeId)
        //{
        //    return _apiRepository.GetMyTimeOff(employeeId, "f");
        //}

        ///// <summary>
        /////  Get List of Time Off Requests for the given employeeId in a specfic year
        ///// This will return the future time off - using Public API and SP
        ///// </summary>
        ///// <param name="employeeId">Employee Internal Id</param>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("GetMyPastTimeOff")]
        //public List<TimeOff> GetMyPastTimeOff(int employeeId)
        //{
        //    return _apiRepository.GetMyTimeOff(employeeId, "h");
        //}


        /// <summary>
        /// Get Future time off Request using private API
        /// </summary>
        /// <param name="employeeId">Employee Internal ID</param>
        /// <param name="refSessionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal Site Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFutureTimeOff")]
        public List<TimeOff> GetFutureTimeOff(int employeeId, string refSessionId, int siteId)
        {
            return _apiRepository.GetFutureTimeOff(employeeId, refSessionId, siteId);
        }

        /// <summary>
        /// To Get the past time off request using private API
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="refSessionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyHistoryTimeOff")]
        public List<TimeOff> GetMyHistoryTimeOff(int siteId, string refSessionId)
        {
            return _apiRepository.GetMyHistoryTimeOff(siteId, refSessionId);
        }



        #endregion

        #region MyAccurals

        /// <summary>
        /// This will return the employee Accruals - Using Public API
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetEmployeeAccruals")]
        public List<EmployeeAccruals> GetEmployeeAccruals(int employeeId)
        {
            return _apiRepository.GetEmployeeAccruals(employeeId);
        }


        #endregion

        #region Edited Punches

        /// <summary>
        /// This methods will return the punch type
        /// Using SP for this
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPunchTypeCode")]
        public List<PunchTypeCode> GetPunchTypeCode()
        {
            return _apiRepository.GetPunchTypeCode();
        }


        /// <summary>
        /// This method will return the all Edited punches
        /// </summary>
        /// <param name="refSessionId">Token to access private API</param>
        /// <param name="siteId">Internal Site ID</param>
        /// <param name="date">Date</param>
        /// <param name="statusCode">Acknowldge =  "a" , Unread = "r", Disputed = i</param>
        /// 
        /// <returns></returns>
        [HttpGet]
        [Route("GetEditedPunches")]
        public List<EditedPunches> GetEditedPunches(string refSessionId, int siteId, DateTime date, string statusCode)
        {
            return _apiRepository.GetEditedPunches(refSessionId, siteId, date, statusCode);
        }

        #endregion
        
        #region Availability
        
        /// <summary>
        /// Get the all Availability request
        /// </summary>
        /// <param name="refSessionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal Site Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyAvailabilityRequest")]
        public List<MyAvailabilityRequest> GetMyAvailabilityRequest(string refSessionId, int siteId)
        {
            return _apiRepository.GetMyAvailabilityRequest(refSessionId, siteId);
        }

        /// <summary>
        /// This API return the current Availability of the Employee
        /// Using Public API
        /// </summary>
        /// <param name="Employeeid">Internal Employee id </param>
        /// <param name="date">Day of week</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyCurrentAvailability")]
        public EmployeeAvailability GetMyCurrentAvailability(int Employeeid , DateTime date)
        {
            return _apiRepository.GetMyCurrentAvailability(Employeeid, date);
        }

        /// <summary>
        /// This API will use to return the Availability in one array
        /// </summary>
        /// <param name="refSessionId">Authentication token to access the private API</param>
        /// <param name="siteId">Internal Site Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyAvailability")]
        public EmployeeNewAvailability GetMyAvailability(int siteId, string refSessionId)//(int Employeeid, DateTime date)
        {
            return _apiRepository.GetMyAvailability(siteId, refSessionId);
        }

        /// <summary>
        /// This method is used to create new availability
        /// Using public API for this
        /// All the dates(yyyy-MM-dd) inputs should be calender dates without time
        /// </summary>
        /// <param name="availability"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateEmployeeAvailability")]
        public string CreateEmployeeAvailability(EmployeeAvailability availability)
        {
            return _apiRepository.CreateEmployeeAvailability(availability);
        }

        /// <summary>
        /// Create Employee availability using private API
        /// </summary>
        /// <param name="availability"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateEmployeeAvailabilityUsingPrivateAPI")]
        public string CreateEmployeeAvailabilityUsingPrivateAPI(EmployeeAvailabilityPrivateApi availability)
        {
            return _apiRepository.CreateEmployeeAvailabilityUsingPrivateAPI(availability);
        }


        /// <summary>
        ///  Cancel Employee Availability Request using private API
        /// </summary>
        /// <param name="cancelAvailabilityRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateEmployeeAvailabilityRequest")]
        public string UpdateEmployeeAvailabilityRequest(EmployeeAvailabilityPrivateApi cancelAvailabilityRequest)
        {
            return _apiRepository.UpdateEmployeeAvailabilityRequest(cancelAvailabilityRequest);
        }

        #endregion

        #region Notification
        /// <summary>
        /// This methods provides the Notification for the employee
        /// Using Private API for this
        /// </summary>
        /// <param name="refSessionId">Token to access private API </param>
        /// <param name="siteId">Internal Site ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyNotification")]
        public List<MyNotification> GetMyNotification(string refSessionId, int siteId)
        {
            return _apiRepository.GetMyNotification(refSessionId, siteId);
        }


        /// <summary>
        /// This method is used to delete the notification
        /// Using Private API for this
        /// </summary>
        /// <param name="deleteNotificationParam"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteMyNotification")]
        public string DeleteMyNotification(DeleteNotificationParam deleteNotificationParam)
        {
            return _apiRepository.DeleteMyNotification(deleteNotificationParam);
        }



        /// <summary>
        /// This method is used to mark to notification Read/Undread
        /// Using SP for this
        /// </summary>
        /// <param name="updateNotification"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("MarkNotificationReadUnread")]
        public string MarkNotificationReadUnread(UpdateNotification updateNotification)
        {
            return _apiRepository.MarkNotificationReadUnread(updateNotification);
        }



        #endregion

    }
}
