﻿using delaPlexAPI.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using delaPlexAPI.Services.PrivateApi;
using delaPlexAPI.Models;

namespace delaPlexAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Private")]
    public class PrivateController : ApiController                                
    {

        readonly IEssPrivateService _IEssPrivateService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iEssPrivateService"></param>
        public PrivateController(IEssPrivateService iEssPrivateService)
        {
            _IEssPrivateService = iEssPrivateService;
        }

        /// <summary>
        /// This will return the time card of the Employee
        /// </summary>
        /// <param name="refSessionId"></param>
        /// <param name="siteId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("MyTimeCard")]
        public MyTimeCard MyTimeCard(string refSessionId, int siteId, DateTime date)
        {
            return _IEssPrivateService.GetMyTimeCard(refSessionId, siteId, date);
        }


        /// <summary>
        /// This method used to update the shift swap request
        /// Flags to update the request
        /// "A" - requestStatus = "AwaitingManager";
        /// "D": requestStatus - "RecipientDenied";
        /// "C"  requestStatus = "SenderCanceled";
        /// </summary>
        /// <param name="updateShiftSwap"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateSwapShiftRequest")]
        public string UpdateSwapShiftRequest(UpdateShiftSwap updateShiftSwap)
        {
            return _IEssPrivateService.UpdateSwapShiftRequest(updateShiftSwap);
        }

        /// <summary>
        /// This API used to Approve/Deny the Shift swap request
        /// </summary>
        /// <param name="processShiftSwap"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ProcessShiftSwap")]
        public string ProcessShiftSwap(ProcessShiftSwap processShiftSwap)
        {
            return _IEssPrivateService.ProcessShiftSwap(processShiftSwap);
        }

        /// <summary>
        /// This method is used to delete the notification
        /// </summary>
        /// <param name="deleteNotificationParam"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteMyNotification")]
        public string DeleteMyNotification(DeleteNotificationParam deleteNotificationParam)
        {
            return _IEssPrivateService.DeleteMyNotification(deleteNotificationParam);
        }

        /// <summary>
        /// This methods provides the Notification for the employee
        /// </summary>
        /// <param name="refSessionId">Token to access private API </param>
        /// <param name="siteId">Internal Site ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyNotification")]
        public List<MyNotification> GetMyNotification(string refSessionId, int siteId)
        {
            return _IEssPrivateService.GetMyNotification(refSessionId, siteId);
        }


        /// <summary>
        /// This method will return the all available shift to swap with the current shift
        /// </summary>
        /// <param name="refSessionId">Token to access private API</param>
        /// <param name="shiftID">Internal shift ID</param>
        /// <param name="siteId">Internal Site ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAvailableShiftForShiftSwap")]
        public List<ShiftSwap> GetAvailableShiftForShiftSwap(string refSessionId, int shiftID, int siteId)
        {
            return _IEssPrivateService.ShiftSwapAvailableShift(refSessionId, shiftID, siteId);
        }

        /// <summary>
        /// This will return available time off Types 
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="refSessionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTimeOffTypes")]
        public List<TimeOffType> GetTimeOffTypes(int siteId, string refSessionId)
        {
            return _IEssPrivateService.GetAdjustmentTypes(siteId, refSessionId);
        }

        /// <summary>
        /// Get the all Availability request
        /// </summary>
        /// <param name="refSessionId"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        [HttpGet]

        [Route("GetMyAvailabilityRequest")]
        public List<MyAvailabilityRequest> GetMyAvailabilityRequest(string refSessionId, int siteId)
        {
            return _IEssPrivateService.GetMyAvailabilityRequest(refSessionId, siteId);
        }


        /// <summary>
        /// This method will return the all Edited punches
        /// </summary>
        /// <param name="refSessionId">Token to access private API</param>
        /// <param name="siteId">Internal Site ID</param>
        /// <param name="date">Date</param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetEditedPunches")]
        public List<EditedPunches> GetEditedPunches(string refSessionId, int siteId, DateTime date, string statusCode)
        {
            return _IEssPrivateService.GetEditedPunches(refSessionId, siteId, date, statusCode);
        }


        /// <summary>
        /// This return the Available Shift 
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <param name="date">Day of week</param>
        /// <param name="refSessionId">Token to access Private API's</param>
        /// <param name="siteId">Employee site ID</param>
        [HttpGet]
        [Route("GetUnfilledShift")]
        public List<AvailableShifts> GetUnfilledShift(int employeeId, DateTime date, string refSessionId, int siteId)
        {
            return _IEssPrivateService.GetUnfilledShift(employeeId, date, siteId, refSessionId);
        }

        /// <summary>
        /// To Update the punch status of the Edited punches
        /// Flag to Mark as Acknowldge - "a"
        /// Dispute ="r"
        /// </summary>
        /// <param name="setPunchEditStatus">SetPunchEditStatus</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetPunchEditStatus")]
        public string SetPunchEditStatus(SetPunchEditStatus setPunchEditStatus)
        {
            return _IEssPrivateService.SetPunchEditStatus(setPunchEditStatus);
        }

        /// <summary>
        /// Get the Employee time off request history
        /// </summary>
        /// <param name="torHistoryApiParam"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetEmployeeTORHistory")]
        public  List<TorHistoryResponce> GetEmployeeTORHistory(TorHistoryApiParam torHistoryApiParam)
        {
            return _IEssPrivateService.GetEmployeeTORHistory(torHistoryApiParam);
        }


        /// <summary>
        /// Get Employee Time off request by request ID
        /// </summary>
        /// <param name="siteId">Internal site ID</param>
        /// <param name="requestID">Internal Time off request id</param>
        /// <param name="refSessionId">Authentication token</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetEmployeeTimeOffRequest")]
        public List<TimeOffRequestData> GetEmployeeTimeOffRequest(int siteId, int requestID, string refSessionId)
        {
            return _IEssPrivateService.GetEmployeeTimeOffRequest(siteId, requestID, refSessionId);
        }

    }
}
