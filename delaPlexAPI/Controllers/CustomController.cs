﻿using delaPlexAPI.Interfaces.Repositories;
using delaPlexAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace delaPlexAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Custom")]
    public class CustomController : ApiController
    {

        readonly IApiRepository _apiRepository;

        public CustomController(IApiRepository apiRepository)
        {
            _apiRepository = apiRepository;
        }


        /// <summary>
        /// Get User profile information 
        /// </summary>
        /// <param name="employeeId">Internal Employee Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("UserProfileInformation")]
        public UserInformation UserProfileInformation(string employeeId)
        {
            return _apiRepository.GetEmployeeProfile(employeeId);
        }

        /// <summary>
        /// This function will return the State list based on employeeId 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStateList")]
        public List<State> GetStateList(int employeeId)
        {
            return _apiRepository.GetStateList(employeeId);
        }

        /// <summary>
        /// Get Unpaired punches of employee 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("GetUnpairedPunches")]
        //public List<MyUnpairedPunches> GetUnpairedPunches(int employeeId)
        //{
        //    return _apiRepository.GetUnpairedPunches(employeeId);
        //}

        /// <summary>
        /// This will relation ship codes 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetEmergencyContactRelationshipCode")]
        public List<EmergencyContactRelationship> GetEmergencyContactRelationshipCode()
        {
            return _apiRepository.GetEmergencyContactRelationshipCode();
        }

        /// <summary>
        /// This API will return edited Punches
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyEditedPunches")]
        public List<MyEditedPunches> GetMyEditedPunches(int employeeId)
        {
            return _apiRepository.GetMyEditedPunches(employeeId);
        }


        /// <summary>
        /// This will return the requested Shift swap
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMyShiftSwaps")]
        public List<ShiftSwap> GetMyShiftSwaps(int employeeId)
        {
            return _apiRepository.GetMyShiftSwaps(employeeId);
        }

        /// <summary>
        /// This will return monthly scheduled dates 
        /// </summary>
        /// <param name="month">Month as MM</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMonthlyScheduleDate")]
        public List<ScheduleDates> GetMonthlyScheduleDate(int month)
        {
            return _apiRepository.GetMonthlyScheduleDate(month);
        }


        /// <summary>
        /// This will return the pending Approvals
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPendingMyApprovals")]
        public List<PendingMyApprovals> GetPendingMyApprovals(int employeeId)
        {
            return _apiRepository.GetPendingMyApprovals(employeeId); 
        }

        /// <summary>
        /// This will return the request requested by Employee 
        /// </summary>
        /// <param name="employeeId">Employee Internal Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRequestedByMe")]
        public List<RequestedByMe> GetRequestedByMe(int employeeId)
        {
            return _apiRepository.RequestedByMe(employeeId);
        }


        /// <summary>
        /// Get Request RTO and TOR period for TimeoffRequest
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRTOandTORPeriod")]
        public TimeOffRequestValidation GetRTOandTORPeriod(int siteId, string refSessionId)
        {
            return _apiRepository.GetRTOandTorPeriod(siteId, refSessionId);
        }

        /// <summary>
        /// This method is used to mark to notification Read/Undread
        /// </summary>
        /// <param name="updateNotification"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("MarkNotificationReadUnread")]
        public string MarkNotificationReadUnread(UpdateNotification updateNotification)
        {
            return _apiRepository.MarkNotificationReadUnread(updateNotification);
        }


        /// <summary>
        /// This methods will return the punch type
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPunchTypeCode")]
        public List<PunchTypeCode> GetPunchTypeCode()
        {
            return _apiRepository.GetPunchTypeCode();
        }
    }
}
