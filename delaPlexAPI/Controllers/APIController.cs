﻿using System.Web.Http;
using delaPlexAPI.Interfaces.Repositories;
using Newtonsoft.Json;
using delaPlexAPI.Models;

namespace delaPlexAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/api")]
    public class APIController : ApiController
    {
        #region Global Variables

        readonly IApiRepository _apiRepository;
        UserInformation _userInformation;
        #endregion

        public APIController(IApiRepository apiRepository)
        {
            _apiRepository = apiRepository;
        }

        //[HttpGet]
        //[Route("UserProfileInformation")]
        //public string UserProfileInformation(string employeeId)
        //{
        //    _userInformation = _apiRepository.GetEmployeeProfile(employeeId);
        //    return JsonConvert.SerializeObject(_userInformation);
        //}

        //[HttpPost]
        //[Route("UpdateUserProfileInformation")]
        //public string UpdateUserProfileInformation(UpdateUserInformation userInformation)
        //{
        //    return _apiRepository.UpdateUserProfileInformation(userInformation);
        //}

       
        
        #region API
        #endregion


    }
}
