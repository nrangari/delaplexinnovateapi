﻿using delaPlexAPI.Interfaces.Repositories;
using delaPlexAPI.Models;
using delaPlexAPI.Services.MobileUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using delaPlexAPI.Models.ESOReceive;

namespace delaPlexAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/ESOReceive")]
    public class ESORecieveController : ApiController
    {

        readonly IReceiveService _IReceiveService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iReceiveService"></param>
        public ESORecieveController(IReceiveService iReceiveService)
        {
            _IReceiveService = iReceiveService;
        }



        /// <summary>
        /// To Get the receiving data 
        /// </summary>
        /// <param name="siteId">Internal Site ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReceivingData")]
        public EsoReceive GetReceivingData(int siteId)
        {
            return _IReceiveService.GetReceivingData(siteId);
        }



    }
}
