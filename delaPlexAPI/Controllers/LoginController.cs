﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using delaPlexAPI.Business.Repositories;
using delaPlexAPI.Models;
using delaPlexAPI.Services.MobileUser;
using Newtonsoft.Json;

namespace delaPlexAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Login")]
    public class LoginController : ApiController
    {

        ResponceResult _responce;
        readonly IMobileUserService _iMobile;
        private ApiRepository apiRepository = new ApiRepository();
        public LoginController(IMobileUserService mobile)
        {
            _iMobile = mobile;
        }

        [HttpGet]
        [Route("LoginUser")]
        public ResponceResult LoginUser()
        {
            string userName = Request.Headers.FirstOrDefault(x => x.Key == "userName").Value?.FirstOrDefault();
            string password = Request.Headers.FirstOrDefault(x => x.Key == "password").Value?.FirstOrDefault();
            _responce = _iMobile.LoginUser(userName, password);
            //if (Convert.ToInt32(_responce.UserID) > 0)
            //{
            //    _responce.siteId = apiRepository.GetSiteId(Convert.ToInt32(_responce.UserID));
            //    //UserInformation obj = apiRepository.GetEmployeeProfile(_responce.UserID);
            //    //_responce.firstName = obj.firstName;
            //    //_responce.middleName = obj.middleName;
            //    //_responce.lastName = obj.lastName;
            //}
            //return JsonConvert.SerializeObject(_responce);

            return _responce;
        }
    }
}
