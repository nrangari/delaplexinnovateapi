USE [RETAIL]
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetAvailability]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[delaPlex_API_GetAvailability] 
(
	 @employeeId INT 
)
AS
SET NOCOUNT ON 
BEGIN
	
SELECT CONVERT(date, ear.requested_timestamp) as [Requested On]
,CASE 
		WHEN Ear.status_code = 'p' and (ear.start_date > current_timestamp)
		THEN 'Awaiting Approval'
		WHEN Ear.status_code = 'p' and (ear.start_date < current_timestamp)
		THEN concat('Expired by System on ',convert(date,ear.start_date))
		WHEN Ear.status_code = 'c' 
		THEN CONCAT('Cancelled by ',usr.last_name,' ,' ,usr.first_name ,' on ',CONVERT(date,ear.last_modified_timestamp))
		WHEN Ear.status_code = 'a' 
		THEN CONCAT('Approved by ',usr.last_name,' ,' ,usr.first_name ,' on ',CONVERT(date,ear.last_modified_timestamp))
	END as "Status"
,CONVERT(date, ear.start_date) as [Start_date]
,CONVERT(date, ear.end_date) as [End_date]
FROM [dbo].[Lab_Employee_Availability_Request] Ear
JOIN [dbo].[Rad_Sys_User] usr 
on usr.user_id = ear.last_modified_user_id
WHERE employee_id =@employeeId


SELECT earr.availability_type_code  as [Type]
,CONVERT(date, ear.start_date) as [Start_date]
,CONVERT(date, ear.end_date) as [End_date]
,dow.name as [All Day]
,earr.start_time_offset as [From]
,case 
		WHEN earr.end_time_offset = '1.00:00'
		THEN '23:59'
		ELSE earr.end_time_offset
	END as [To]  
,ear.last_status_update_comment as [Comments]
FROM [dbo].[Lab_Employee_Availability_Request_Range] earr
JOIN [dbo].[Lab_Employee_Availability_Request] ear 
on earr.availability_request_id = ear.availability_request_id
JOIN SYNC_RETAIL_wh_Time_Day_Of_Week dow 
on dow.day_of_week_id = earr.day_of_week_id
WHERE employee_id =@employeeId

END

GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetAvailableShift]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC [dbo].[delaPlex_API_GetAvailableShift]  1000161
CREATE  PROCEDURE [dbo].[delaPlex_API_GetAvailableShift] 
(
	 @EmployeeID INT 
)
AS
SET NOCOUNT ON 
BEGIN
SELECT  DISTINCT
		ufs.scheduled_shift_id AS ShiftId
		,CONVERT(DATE,ufs.in_timestamp) AS "Date" 
		,RIGHT(CONVERT(varchar,ufs.in_timestamp, 100),7) AS "ShiftStart" 
		,RIGHT(CONVERT(varchar,ufs.out_timestamp, 100),7) AS "ShiftEnd" 
		,job.name AS "Job" 
		,rsd.name AS "Site" 
		--,ufs.in_timestamp as "Date"
		--,ufs.in_timestamp  AS "ShiftStart" 
		--,ufs.out_timestamp AS "ShiftEnd" 
	FROM [dbo].[lab_scheduled_shifts_unfilled_extended_view] ufs 
	JOIN Lab_Employee_Job_List  ejl ON ufs.job_id = ejl.job_id 
	JOIN lab_job job ON ejl.job_id = job.job_id 
	JOIN Rad_Sys_Data_Accessor rsd ON ufs.business_unit_id = rsd.Data_accessor_id 
	--JOIN [dbo].[plt_workday_bu_day_hours_view] wdh ON CONVERT(DATE,ufs.in_timestamp) = CONVERT(DATE,day_id) 
	--AND ufs.business_unit_id = wdh.business_unit_id 
	LEFT JOIN  lab_sched_shift lss
	ON lss.employee_id = ejl.employee_id
	WHERE ejl.employee_id = @EmployeeID
	AND CONVERT(DATE,ufs.in_timestamp) > CURRENT_TIMESTAMP
	AND CONVERT(DATE,ufs.in_timestamp) = CONVERT(DATE,lss.in_timestamp)
	AND (ufs.out_timestamp < lss.in_timestamp OR  ufs.in_timestamp > lss.out_timestamp)
	
END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetCountryState]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[delaPlex_API_GetCountryState] 
(
	 @employeeId INT 
)
AS
SET NOCOUNT ON 
BEGIN
	DECLARE @CountryId int 

	SELECT @CountryId  = rsc.country_id
	FROM EMPLOYEE e
	JOIN Rad_Sys_User rsu on e.employee_id=rsu.user_id
	JOIN Address a on rsu.address_id=a.address_id
	LEFT JOIN Rad_Sys_Country rsc on a.country_code=rsc.country_code
	WHERE e.employee_id = @employeeId

	SELECT state_id AS "StateId",
		   state_code AS "StateCode",
			[name] AS "StateName" 
	FROM Rad_Sys_State 
	WHERE country_id = @CountryId

END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetDailyPunchedhours]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [dbo].[delaPlex_API_GetDailyPunchedhours] 1000172

CREATE PROCEDURE [dbo].[delaPlex_API_GetDailyPunchedhours] 
(
	@employeeId int
)
AS
BEGIN
SELECT CONVERT(DATE,b.punch_timestamp) AS "Date"
		,CONCAT(CONVERT(DECIMAL(10,2),ROUND(Sum(Hours),2)),' Hrs') As "PunchedHours"
FROM Lab_Pay_Output a
JOIN lab_punch b 
ON a.shift_start_punch_id = b.punch_id
WHERE a.employee_id = @employeeId
GROUP BY CONVERT(DATE,b.punch_timestamp)

END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetDailyScheduledhours]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [dbo].[delaPlex_API_GetDailyScheduledhours]  1000172

CREATE PROCEDURE [dbo].[delaPlex_API_GetDailyScheduledhours] 
(
	@employeeId int
)
AS
BEGIN
SELECT scheduled_shift_id As ShiftId, CONVERT(DATE,in_timestamp) AS "Date"
	   ,CONCAT(CONVERT(DECIMAL(10,2),ROUND(COALESCE(net_shift_hours,salaried_net_shift_hours),2)),' Hrs') as "Hours"
	   ,CONVERT(DECIMAL(10,2),ROUND(COALESCE(net_shift_hours,salaried_net_shift_hours),2)) as "DayHour"
FROM [lab_scheduled_shifts_employee_extended_view]
WHERE employee_id = @employeeId



-- SELECT scheduled_shift_id, in_timestamp ,CONVERT(DATE,in_timestamp) AS "Date"
--	   ,CONCAT(CONVERT(DECIMAL(10,2),ROUND(COALESCE(net_shift_hours,salaried_net_shift_hours),2)),' Hrs') as "Hours"
--FROM [lab_scheduled_shifts_employee_extended_view]
--WHERE employee_id = 1000161


END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetDeviceList]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[delaPlex_API_GetDeviceList]  
 -- Add the parameters for the stored procedure here  
  @BusinessID INT  = 0
AS  
BEGIN  
   IF(@BusinessID =0)
   BEGIN
		SELECT device_id AS DeviceID, name AS [DeviceName] from Device --where business_unit_id = @BusinessID  
   END
   ELSE
		SELECT device_id AS DeviceID, name AS [DeviceName] from Device where business_unit_id = @BusinessID  
END  
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetDistinctTimeOffType]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
---EXEC delaPlex_API_GetDistinctTimeOffType 
-- =============================================
CREATE PROCEDURE [dbo].[delaPlex_API_GetDistinctTimeOffType] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT DISTINCT lpa.pay_adjustment_id AS [AdjustmentID]
, lpa.name AS [TimeoffType]
FROM Lab_Pay_Adjustment lpa 
JOIN Lab_Pay_Rule_Pay_Adj lprpa ON lpa.pay_adjustment_id = lprpa.pay_adjustment_id
JOIN Lab_Pay_Rule_Definition lprd ON lprd.pay_rule_definition_id=lprpa.pay_rule_definition_id
WHERE lpa.pay_adjustment_type IN ('n','p') 
AND lprd.definition_type_code != 'i'

END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetEditedPunches]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[delaPlex_API_GetEditedPunches] 
(
	 @employeeId INT 
)
AS
SET NOCOUNT ON 
BEGIN
	SELECT pun.punch_edit_approval_id AS [PunchEditApprovalID]
			,CASE
				WHEN pun.approval_code = 'a' THEN 'Acknowledged'
				WHEN pun.approval_code = 'r' THEN 'Disputed'
				WHEN pun.approval_code = 'i' THEN 'Unread'
			 END AS "My Tab"
			,CASE 
				WHEN lar.audit_reason_id = '1' 
				THEN CASE 
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 's' THEN 'Job & Shift Start'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 't' THEN 'Job & Shift End'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 'm' THEN 'Job & Meal Start'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 'l'THEN 'Job & Meal End'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 'b' THEN 'Job & Break Start'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 'k' THEN 'Job & Break End'
					 END
				WHEN lar.audit_reason_id <> '1' 
				THEN CASE 
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 's' THEN 'Shift Start'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 't' THEN 'Shift End'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 'm' THEN 'Meal Start'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 'l'THEN 'Meal End'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 'b' THEN 'Break Start'
						WHEN  COALESCE(lpu.punch_type_code,lpa.punch_type_code)  = 'k' THEN 'Break End'
					END
			  END
			  AS "Change Type"
			,ISNULL(CONVERT(VARCHAR(30),lpa.punch_timestamp,121),'') AS "Original Punch"
			,ISNULL(CONVERT(VARCHAR(30),lpu.punch_timestamp,121),'') AS "New Punch"
			,job.name AS "Job"
			,CONCAT(rsu2.first_name, ' ', rsu2.last_name) AS "Edited By"
			,lar.name AS "Reason"
			,ISNULL(pun.comment,'  ') AS "Comments"
			,COALESCE(lpu.last_modified_timestamp,lpa.last_modified_timestamp) AS "Last Edit"
	FROM Lab_Punch_edit_approval AS pun
	LEFT JOIN Lab_Punch AS lpu ON pun.punch_id = lpu.punch_id
	LEFT JOIN Lab_Employee_Job_List ejl ON ejl.employee_id = pun.employee_id
	LEFT JOIN Lab_Job job ON job.job_id = ejl.job_id
	LEFT OUTER JOIN Lab_Punch_Audit lpa ON pun.comp_Punch_audit_id = lpa.Punch_audit_id
	LEFT JOIN Lab_Punch_Audit Lpa1 ON lpa1.Punch_audit_id = pun.Punch_audit_id
	LEFT OUTER JOIN Lab_Audit_Reason lar ON lar.audit_reason_id = lpa1.audit_reason_id
	LEFT OUTER JOIN lab_Punch_audit aud ON pun.Punch_audit_id = aud.Punch_audit_id
	LEFT JOIN rad_sys_user rsu2 ON rsu2.user_id = aud.audit_user_id
	WHERE pun.employee_id = @employeeId
END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetEmergencyRelationShip]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[delaPlex_API_GetEmergencyRelationShip] 
AS
SET NOCOUNT ON 
BEGIN
	SELECT value_code AS 'ContactCode', 
		   msg_text AS 'ContactType'
	FROM   [dbo].[rad_sys_valid_value_view] 
	WHERE  valid_value_group_id = 70 

END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetMonthlyScheduleDate]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[delaPlex_API_GetMonthlyScheduleDate] 
(
	@Month int
)
AS
BEGIN
	SELECT  DATEPART(DD, in_timestamp) AS Dates
	FROM [Lab_Sched_Shift]
	WHERE employee_id = 1000161 and DATEPART(MM,in_timestamp) = @Month
END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetNotification]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[delaPlex_API_GetNotification] 
(
	 @employeeId INT 
)
AS
SET NOCOUNT ON 
BEGIN
---My Notification -------------
SELECT rsd.name as "Site",job.name as "Role",
	CASE 
			WHEN mum.mail_id is NOT NULL THEN CONCAT(rad.last_name,', ',rad.first_name)
			ELSE  '[System]'
		END AS "From"
,m.subject  AS "Subject"
,CONVERT(date,m.entered_date) AS "Received"
,mul.user_id
FROM Mail_User_List mul
JOIN mail m on mul.mail_id=m.mail_id 
LEFT OUTER JOIN Mail_User_Msg mum ON mul.mail_id = mum.mail_id
LEFT OUTER JOIN rad_sys_user rad on rad.user_id  = m.last_modified_user_id
LEFT OUTER JOIN Mail_System_Msg smsg on smsg.mail_id = mul.mail_id
LEFT JOIN Lab_Employee_Business_Unit_List BU on bu.employee_id = mul.user_id
LEFT JOIN Rad_Sys_Data_Accessor rsd on BU.business_unit_id = rsd.Data_accessor_id
LEFT JOIN Lab_Employee_Job_List ejl on ejl.employee_id = mul.user_id
LEFT JOIN Lab_Job job on job.job_id = ejl.job_id 
WHERE mul.user_id = @employeeId 
and mul.mail_id not in (
						SELECT Mail_id FROM Mail_User_Msg msg
						WHERE from_user_id = @employeeId
						) 
------------------------------------------------------------------------
END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetPendingMyApprovals]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--execute delaPlex_api_GetPendingMyApprovals 1000161

CREATE  PROCEDURE [dbo].[delaPlex_API_GetPendingMyApprovals] 
(
	 @SwappeeId INT
)
AS
SET NOCOUNT ON 
BEGIN
	SELECT rsd.name as "BUUnit"
,concat(COALESCE(rad1.last_name,''),',', COALESCE(rad1.first_name, ''),' ',COALESCE(rad1.middle_name, '')) as "Swapper"
,concat(COALESCE(rad.last_name,''),',', COALESCE(rad.first_name, ''),' ',COALESCE(rad.middle_name, '')) as "SwappedWith"
,swap.swap_id as "SwapId"
,CASE 
		WHEN swap.shift_swappee_status_code = 'a' 
		THEN CONCAT('Approved by',' : ',concat(COALESCE(rad2.last_name,''),', ', COALESCE(rad2.first_name, ''),' ',COALESCE(rad2.middle_name, ''))
		, '   Schedules swapped')
		WHEN swap.shift_swappee_status_code = 'c' 
		THEN CONCAT('Request Canceled   ','Requested by',' : ',concat(COALESCE(rad1.last_name,' '),', ', COALESCE(rad1.nickname, rad1.first_name)))
		WHEN swap.shift_swappee_status_code = 'r' 
		THEN CONCAT('Requested by',' : ',concat(COALESCE(rad1.last_name,''),', ', COALESCE(rad1.first_name, ''),' ',COALESCE(rad1.middle_name, '')))
		WHEN swap.shift_swappee_status_code = 'y' 
		THEN CONCAT('Awaiting Approval by',' : Manager ,' ,'Requested By',' : ',concat(COALESCE(rad1.last_name,''),', ', COALESCE(rad1.first_name, ''),' ',COALESCE(rad1.middle_name, '')))
		WHEN swap.shift_swappee_status_code = 'x' 
		THEN CONCAT('Denied By',' : ',concat(COALESCE(rad3.last_name,''),', ', COALESCE(rad3.first_name, ''),' ',COALESCE(rad3.middle_name, '')),'  ',
		'Requested by',' : ',concat(COALESCE(rad1.last_name,''),', ', COALESCE(rad1.nickname, rad1.first_name)))
		WHEN swap.shift_swappee_status_code = 'i' 
		THEN CONCAT('Request Canceled   Schedule was modified','  ',
		'Requested by',' : ',concat(COALESCE(rad1.last_name,''),', ', COALESCE(rad1.nickname,rad1.first_name)))
	END as "Status"
--,swap.shift_swappee_status_code as "Status"
,sched1.scheduled_shift_id as "GainedShiftId"
,CONCAT(CONVERT(date,sched1.in_timestamp),' ',RIGHT(CONVERT(VARCHAR,sched1.in_timestamp, 100),7),'-',
RIGHT(CONVERT(VARCHAR,sched1.out_timestamp, 100),7)) AS "GainedShift"
,sched.scheduled_shift_id as "LostShiftId"
,CONCAT(CONVERT(date,sched.in_timestamp),' ',RIGHT(CONVERT(VARCHAR,sched.in_timestamp, 100),7),'-',
RIGHT(CONVERT(VARCHAR,sched.out_timestamp, 100),7)) AS "LostShift"
,job.name AS "Role"
FROM [dbo].[Lab_Sched_Shift_Swap_Swappee] swap
JOIN [dbo].[Lab_Sched_Shift] sched
on sched.scheduled_shift_id = swap.swappee_scheduled_shift_id
JOIN [dbo].[Lab_Sched_Shift_Swap] swpr
on swpr.Swap_id = swap.swap_id
JOIN [dbo].[Lab_Sched_Shift] sched1
on sched1.scheduled_shift_id = swpr.swapper_scheduled_shift_id
JOIN Rad_Sys_User rad
on rad.user_id  = swap.swappee_id
JOIN lab_job job
on job.job_id = sched.job_id
JOIN Rad_Sys_User rad1
on rad1.user_id = swpr.swapper_id
LEFT OUTER JOIN Rad_Sys_User rad2
on rad2.user_id = swpr.approver_id
JOIN Rad_Sys_Data_Accessor rsd
on sched.business_unit_id = rsd.Data_accessor_id
LEFT OUTER JOIN Rad_Sys_User rad3
on rad3.user_id = swap.last_modified_user_id
WHERE swap.swappee_id = @SwappeeId
and CONVERT(date,sched1.in_timestamp) > CURRENT_TIMESTAMP
and CONVERT(date,sched.in_timestamp) > CURRENT_TIMESTAMP
and swap.shift_swappee_status_code <> 'n'

END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetPuncheTypeCode]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[delaPlex_API_GetPuncheTypeCode] 
AS
BEGIN
			CREATE TABLE #TEMP (Code varchar(1), PunchType VARCHAR(20))
			INSERT INTO #TEMP (Code, PunchType) Values ('s' , 'Shift Start')
			INSERT INTO #TEMP (Code, PunchType) Values ('t' , 'Shift End')
			INSERT INTO #TEMP (Code, PunchType) Values ('m' , 'Meal Start')
			INSERT INTO #TEMP (Code, PunchType) Values ('l', 'Meal End')
			INSERT INTO #TEMP (Code, PunchType) Values ('b' , 'Break Start')
			SELECT * FROM #TEMP
			DROP TABLE #TEMP
END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetRequestedByMe]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [dbo].[delaPlex_API_GetRequestedByMe] 1000161

CREATE  PROCEDURE [dbo].[delaPlex_API_GetRequestedByMe] 
(
	 @swapperId INT
)
AS
SET NOCOUNT ON 
BEGIN
	SELECT rsd.name AS "BuUnit"
	,CONCAT(COALESCE(rad1.last_name,''),', ', COALESCE(rad1.first_name, '')) AS "Swapper"
	,CONCAT(COALESCE(rad.last_name,''),', ', COALESCE(rad.first_name, '')) AS "SwappedWith"
	,swpr.swap_id AS "SwapId"
	,CASE 
		WHEN swap.shift_swappee_status_code = 'a'
		THEN CONCAT('Approved By',' : ',concat(COALESCE(rad2.last_name,''),', ',COALESCE(rad2.first_name, ''))
		, '   Schedules swapped')
		WHEN swap.shift_swappee_status_code = 'c' 
		THEN 'Request Canceled'
		WHEN swap.shift_swappee_status_code = 'r' 
		THEN CONCAT('Awaiting Approval By',' : ',concat(COALESCE(rad.last_name,''),', ',COALESCE(rad.first_name, '')))
		WHEN swap.shift_swappee_status_code = 'y' 
		THEN CONCAT('Awaiting Approval By',' : Manager,' ,' Approved By',' : ',concat(COALESCE(rad3.last_name,''),', ', COALESCE(rad3.first_name, '')))
		WHEN swap.shift_swappee_status_code = 'x' OR swpr.shift_swap_status_code = 'x'
		THEN CONCAT('Denied By',' : ',CONCAT(COALESCE(rad3.last_name,''),', ', COALESCE(rad3.first_name, '')))
		WHEN swap.shift_swappee_status_code = 'i' 
		THEN 'Request Canceled  Schedule was modified'
	END as "Status"
 --,swpr.shift_swap_status_code AS " Status"
	,sched1.scheduled_shift_id AS "LostShiftId"
	,CONCAT(CONVERT(DATE,sched1.in_timestamp),' ',RIGHT(CONVERT(VARCHAR,sched1.in_timestamp, 100),7),'-',
	RIGHT(CONVERT(VARCHAR,sched1.out_timestamp, 100),7)) AS "LostShift"
	,sched.scheduled_shift_id AS "GainedShiftId"
	,CONCAT(CONVERT(DATE,sched.in_timestamp),' ',RIGHT(CONVERT(VARCHAR,sched.in_timestamp, 100),7),'-',
	RIGHT(CONVERT(VARCHAR,sched.out_timestamp, 100),7)) AS "GainedShift"
	,job.name AS "Role"
FROM [dbo].[Lab_Sched_Shift_Swap_Swappee] swap
JOIN [dbo].[Lab_Sched_Shift] sched  on sched.scheduled_shift_id = swap.swappee_scheduled_shift_id
JOIN [dbo].[Lab_Sched_Shift_Swap] swpr on swpr.Swap_id = swap.swap_id
JOIN [dbo].[Lab_Sched_Shift] sched1 on sched1.scheduled_shift_id = swpr.swapper_scheduled_shift_id
JOIN Rad_Sys_User rad on rad.user_id  = swap.swappee_id
JOIN lab_job job on job.job_id = sched.job_id
JOIN Rad_Sys_User rad1 on rad1.user_id = swpr.swapper_id
LEFT OUTER JOIN Rad_Sys_User rad2 on rad2.user_id = swpr.approver_id
JOIN Rad_Sys_Data_Accessor rsd on sched.business_unit_id = rsd.Data_accessor_id
LEFT OUTER JOIN Rad_Sys_User rad3 on rad3.user_id = swpr.last_modified_user_id
WHERE swpr.swapper_id = @swapperId 
and CONVERT(date,sched1.in_timestamp) > CURRENT_TIMESTAMP
and CONVERT(date,sched.in_timestamp) > CURRENT_TIMESTAMP


END


 
 --SELECT * FROM [Lab_Sched_Shift_Swap] WHERE swap_id = 1000163
 --SELECT * FROM [Lab_Sched_Shift_Swap_Swappee] WHERE  swap_id = 1000163
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_getRTOPeriod]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
      
-- =============================================      
CREATE PROCEDURE [dbo].[delaPlex_API_getRTOPeriod]      
      
AS      
BEGIN      
 SELECT min_days_notice_RTO , tor_max_days, 0 AS MinDaysNoticeForTimeOffRequests FROM wfm_client_parameters      
END 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetSiteId]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shakti Lamba
-- Create date: 24-09-2018
-- Description:	Get Site ID
-- =============================================
CREATE PROCEDURE [dbo].[delaPlex_API_GetSiteId] 
	@EmployeeId INT
---exec delaPlex_API_GetSiteId 1000161
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT business_unit_id as site_id,employee_id FROM Lab_Employee_Business_Unit_List
	WHERE employee_id = @EmployeeId
END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetTimeOff]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[delaPlex_API_GetTimeOff] 
(
	 @employeeId INT 
)
AS
SET NOCOUNT ON 
BEGIN
	SELECT 
			DISTINCT 
			etf.employee_id AS EmployeeId,
			etf.time_off_id AS TimeOffId,
			 CASE 
							WHEN etf.status_code = 'r' 
							THEN 'Pending' 
							WHEN etf.status_code = 'a' 
							THEN 'Approved' 
							WHEN etf.status_code = 'd' 
							THEN 'Denied' 
							WHEN etf.status_code = 'c' 
							THEN 'Cancelled' 
						END as [Status]
			,CONVERT(DATE, etf.start_timestamp) as [Start Date]
			,CASE 
				WHEN CONVERT(DATE, etf.start_timestamp) = CONVERT(DATE,etf.end_timestamp) 
				THEN ''
				ELSE CONVERT(CHAR(10), etf.end_timestamp, 120) 
				END AS [End Date]
			,etf.comment as [Comment]
			,e.name as [Type]
			,CONCAT(rsu.last_name, ', ', rsu.first_name) as [Last Modified By]
			,SUBSTRING(CONVERT(VARCHAR, etf.start_timestamp, 108),0,6) as [Start Time]
			,SUBSTRING(CONVERT(VARCHAR, etf.end_timestamp, 108), 0, 6) as [End Time] 
	FROM [dbo].[Lab_Employee_Time_Off] etf 
	JOIN Lab_Employee_Time_Off_Detail c 
	on CONCAT(c.employee_id, c.time_off_id) = CONCAT(etf.employee_id, etf.time_off_id)
	JOIN Lab_Pay_Adjustment e on e.pay_adjustment_id = etf.pay_adjustment_id 
	JOIN Rad_Sys_User rsu on etf.last_modified_user_id = rsu.user_id 
	WHERE etf.employee_id = @employeeId
END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetTimeOffApprover]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[delaPlex_API_GetTimeOffApprover] 
(
	 @employeeId INT 
)
AS
SET NOCOUNT ON 
BEGIN
	SELECT 
			DISTINCT 
			etf.time_off_id AS TimeOffId,
			CONCAT(rsu.last_name, ', ', rsu.first_name) as [LastModifiedBy]
	FROM [dbo].[Lab_Employee_Time_Off] etf 
	JOIN Rad_Sys_User rsu on etf.last_modified_user_id = rsu.user_id 
	WHERE etf.employee_id = @employeeId
END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetTimeOffDetails]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
---exec delaPlex_API_GetTimeOffDetails 1000161,1000131
-- =============================================
CREATE PROCEDURE [dbo].[delaPlex_API_GetTimeOffDetails] 
	@employeeID INT,@timeOffID INT
AS
BEGIN
SELECT DISTINCT
  etf.time_off_id AS [TimeOffID]
 ,CASE 
   WHEN etf.status_code = 'r' THEN 'Pending'
   WHEN etf.status_code = 'a' THEN 'Approved'
   WHEN etf.status_code = 'd' THEN 'Denied'
   WHEN etf.status_code = 'c' THEN 'Cancelled' 
   END as [Status]
  ,CONVERT(DATE,etf.start_timestamp) AS [Date]
  ,e.name as [Type]
  ,SUBSTRING(CONVERT(VARCHAR, etf.start_timestamp, 108),0,6) AS [Start Time]
  ,SUBSTRING(CONVERT(VARCHAR, etf.end_timestamp, 108),0,6) AS [End Time]
  ,etf.comment AS [Comment]
  ,CONCAT( CONCAT(rsu1.last_name, ', ' , rsu1.first_name) 
		,' '
		,CASE 
				WHEN aud.status_code = 'r' THEN 'Pending'
				WHEN aud.status_code = 'a' THEN 'Approved' END
		,':'
		,CONVERT(DATE,aud.audit_timestamp) 
		,' '
		,RIGHT(CONVERT(VARCHAR, aud.audit_timestamp, 100),7)
		,' '  
		,aud.comment
	 )	AS "Comment History"
  ,CONCAT(rsu.last_name, ', ' , rsu.first_name) AS [Last Modified By]
FROM [dbo].[Lab_Employee_Time_Off] etf 
JOIN Lab_Pay_Adjustment e
ON e.pay_adjustment_id = etf.pay_adjustment_id
JOIN Rad_Sys_User rsu ON etf.last_modified_user_id=rsu.user_id
JOIN Lab_Employee_Time_Off_Audit aud
ON aud.employee_id = etf.employee_id  and aud.time_off_id=etf.time_off_id
JOIN Rad_Sys_User rsu1
ON rsu1.user_id = aud.last_modified_user_id
WHERE etf.employee_id = @employeeID and etf.time_off_id = @timeOffID
END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetTimeOffType]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[delaPlex_API_GetTimeOffType] 
AS
BEGIN
	 SELECT pay_adjustment_id AS timeOffTypeID,[name] AS timeOffType  FROM Lab_Pay_Adjustment
END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_Api_GetUnpairedPunches]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec [delaPlex_Api_GetUnpairedPunches] 1000164

CREATE PROCEDURE [dbo].[delaPlex_Api_GetUnpairedPunches] @emp_id int
	-- Add the parameters for the stored procedure here

AS
BEGIN
----------DISTINCT DATE---------------------			
select distinct CONVERT(Date,a.punch_timestamp) as Date
,employee_id as "Employee_id" ,b.shift_start_punch_id as "Shift_id"
into #tmp_punch_det
from [dbo].[Lab_Punch] a
join [dbo].[Lab_Punch_Shift] b
on a.shift_start_punch_id = b.shift_start_punch_id
left join [dbo].[Lab_Punch] c
on b.shift_end_punch_id = c.punch_id 
left join [dbo].[Lab_Punch] d
on b.shift_end_punch_id = d.punch_id 
where employee_id = @emp_id
-----------------------------------------------------------------------

---------SHIFT START AND END TIME-----------------------------------
Select * into #tmp_shift_punch_time from(
select Dt_in,b.Row_num as "Row#1",c.Row_num as "Row#2",
 Dt_in_Max as "Start_time"
,Dt_out_Max as "End_Time"
,b.Job_ID as "Job_ID"
,coalesce(b.employee_id,c.employee_id) as "Employee_id"
,b.Shift_id
from 
	#tmp_punch_det a 
	 join (SELECT ROW_NUMBER() OVER(PARTITION BY CONVERT(Date,punch_timestamp) ORDER BY Max(a.punch_timestamp) ASC) AS Row_num,
	  CONVERT(Date,punch_timestamp) dt_in, Max(a.punch_timestamp) Dt_in_Max ,job_id as "Job_ID" 
					,b.shift_start_punch_id as "Shift_id"
				,b.employee_id as "Employee_id"
			    from lab_punch a
				join  [dbo].[Lab_Punch_Shift] b
				on a.shift_start_punch_id = b.shift_start_punch_id
				where punch_type_code in ('s')
				group by  punch_timestamp,CONVERT(Date,punch_timestamp),job_id,b.employee_id,b.shift_start_punch_id) b
		on a.Employee_id = b.Employee_id and a.shift_id = b.shift_id
	left join (SELECT ROW_NUMBER() OVER(PARTITION BY CONVERT(Date,punch_timestamp) ORDER BY Max(a.punch_timestamp) ASC) AS Row_num,
	 CONVERT(Date,punch_timestamp) dt_out, Max(a.punch_timestamp) Dt_out_Max ,job_id as "Job_ID"
				,b.employee_id as "Employee_id"
				,b.shift_start_punch_id as "Shift_id"
			    from lab_punch a
				join  [dbo].[Lab_Punch_Shift] b
				on a.shift_start_punch_id = b.shift_start_punch_id
				where punch_type_code in ('t') 
				group by  punch_timestamp,CONVERT(Date,punch_timestamp),job_id,b.employee_id,b.shift_start_punch_id) c
		on a.Employee_id = c.Employee_id and a.shift_id = c.shift_id
		)as tmp_shift_punch_time
----------------------------------------------------------------------------

---------MEAL TIMINGS---------------------
Select * into #tmp_meal_punch_time from(
select Dt_meal_in,b.Row_num as "Row#1",c.Row_num as "Row#2",
 Dt_meal_in_Max as "Start_time"
,Dt_meal_out_Max as "End_Time"
,b.Job_ID as "Job_ID"
,coalesce(b.employee_id,c.employee_id) as "Employee_id"
,b.Shift_id
from 
	#tmp_punch_det a 
	 join (SELECT ROW_NUMBER() OVER(PARTITION BY CONVERT(Date,punch_timestamp) ORDER BY Max(a.punch_timestamp) ASC) AS Row_num,
	  CONVERT(Date,punch_timestamp) dt_meal_in, Max(a.punch_timestamp) Dt_meal_in_Max ,job_id as "Job_ID" 
					,b.shift_start_punch_id as "Shift_id"
				,b.employee_id as "Employee_id"
			    from lab_punch a
				join  [dbo].[Lab_Punch_Shift] b
				on a.shift_start_punch_id = b.shift_start_punch_id
				where punch_type_code in ('m')
				group by  punch_timestamp,CONVERT(Date,punch_timestamp),job_id,b.employee_id,b.shift_start_punch_id) b
		on a.Employee_id = b.Employee_id and a.shift_id = b.shift_id
	left join (SELECT ROW_NUMBER() OVER(PARTITION BY CONVERT(Date,punch_timestamp) ORDER BY Max(a.punch_timestamp) ASC) AS Row_num,
	 CONVERT(Date,punch_timestamp) dt_meal_out, Max(a.punch_timestamp) Dt_meal_out_Max ,job_id as "Job_ID"
				,b.employee_id as "Employee_id"
				,b.shift_start_punch_id as "Shift_id"
			    from lab_punch a
				join  [dbo].[Lab_Punch_Shift] b
				on a.shift_start_punch_id = b.shift_start_punch_id
				where punch_type_code in ('l') 
				group by  punch_timestamp,CONVERT(Date,punch_timestamp),job_id,b.employee_id,b.shift_start_punch_id) c
		on a.Employee_id = c.Employee_id and a.shift_id = c.shift_id
		)as tmp_meal_punch_time
-------------------------------------------------------------------------------------


--------------BREAK TIMINGS-------------------
Select * into #tmp_break_punch_time from(
select Dt_break_in,b.Row_num as "Row#1",c.Row_num as "Row#2",
 Dt_break_in_Max as "Start_time"
,Dt_break_out_Max as "End_Time"
,b.Job_ID as "Job_ID"
,coalesce(b.employee_id,c.employee_id) as "Employee_id"
,b.Shift_id
from 
	#tmp_punch_det a 
	 join (SELECT ROW_NUMBER() OVER(PARTITION BY CONVERT(Date,punch_timestamp) ORDER BY Max(a.punch_timestamp) ASC) AS Row_num,
	  CONVERT(Date,punch_timestamp) dt_break_in, Max(a.punch_timestamp) Dt_break_in_Max ,job_id as "Job_ID" 
					,b.shift_start_punch_id as "Shift_id"
				,b.employee_id as "Employee_id"
			    from lab_punch a
				join  [dbo].[Lab_Punch_Shift] b
				on a.shift_start_punch_id = b.shift_start_punch_id
				where punch_type_code in ('b')
				group by  punch_timestamp,CONVERT(Date,punch_timestamp),job_id,b.employee_id,b.shift_start_punch_id) b
		on a.Employee_id = b.Employee_id and a.shift_id = b.shift_id
	left join (SELECT ROW_NUMBER() OVER(PARTITION BY CONVERT(Date,punch_timestamp) ORDER BY Max(a.punch_timestamp) ASC) AS Row_num,
	 CONVERT(Date,punch_timestamp) dt_break_out, Max(a.punch_timestamp) Dt_break_out_Max ,job_id as "Job_ID"
				,b.employee_id as "Employee_id"
				,b.shift_start_punch_id as "Shift_id"
			    from lab_punch a
				join  [dbo].[Lab_Punch_Shift] b
				on a.shift_start_punch_id = b.shift_start_punch_id
				where punch_type_code in ('k') 
				group by  punch_timestamp,CONVERT(Date,punch_timestamp),job_id,b.employee_id,b.shift_start_punch_id) c
		on a.Employee_id = c.Employee_id and a.shift_id = c.shift_id
		)as tmp_break_punch_time
---------------------------------------------------------------------------------------

------------FETCH MAX AND POPULATE REASON-------------------------------------
		SELECT * 
		FROM   (SELECT rsd.NAME                                    AS "Site", 
					   a.employee_id                               AS "EmployeeId", 
					   job.NAME                                    AS "Job", 
					   Concat(rsu.last_name, ', ', rsu.first_name) AS "EmployeeName", 
					 --  a.shift_id, 
					   CONVERT(DATE, CONVERT(varchar, shift_start, 23)) AS "Date",
					   shift_start AS "ShiftStart", 
					   shift_end AS "ShiftEnd", 
					   meal_start AS "MealStart", 
					   meal_end AS "MealEnd", 
					   break_start AS "BreakStart", 
					   break_end AS "BreakEnd", 
					   CASE 
						 WHEN shift_start IS NULL 
							  AND shift_end IS NOT NULL THEN 'Missing shift start' 
						 WHEN ( shift_start IS NOT NULL 
								AND shift_end IS NULL ) 
							   OR shift_start > shift_end THEN 'Missing shift end' 
					   END                                         AS "ShiftReason", 
					   CASE 
						 WHEN meal_start IS NULL 
							  AND meal_end IS NOT NULL THEN 'Missing meal start' 
						 WHEN ( meal_start IS NOT NULL 
								AND meal_end IS NULL ) 
							   OR ( meal_start > meal_end ) THEN 'Missing meal end' 
					   END                                         AS "MealReason", 
					   CASE 
						 WHEN break_start IS NULL 
							  AND break_end IS NOT NULL THEN 'Missing Break start' 
						 WHEN ( break_start IS NOT NULL 
								AND break_end IS NULL ) 
							   OR ( break_start > break_end ) THEN 'Missing Break end' 
					   END                                         AS "BreakReason" 

				FROM   (SELECT Max(start_time) AS "Shift_start", 
							   Max(end_time)   AS "Shift_end", 
							   job_id, 
							   employee_id, 
							   shift_id 
						FROM   #tmp_shift_punch_time 
						GROUP  BY job_id, 
								  employee_id, 
								  shift_id) AS a 
					   LEFT JOIN (SELECT Max(start_time) AS "Meal_start", 
										 Max(end_time)   AS "Meal_end", 
										 job_id, 
										 employee_id, 
										 shift_id 
								  FROM   #tmp_meal_punch_time 
								  GROUP  BY job_id, 
											employee_id, 
											shift_id)AS b 
							  ON a.shift_id = b.shift_id 
					   LEFT JOIN (SELECT Max(start_time) AS "Break_start", 
										 Max(end_time)   AS "Break_end", 
										 job_id, 
										 employee_id, 
										 shift_id 
								  FROM   #tmp_break_punch_time 
								  GROUP  BY job_id, 
											employee_id, 
											shift_id) AS c 
							  ON a.shift_id = c.shift_id 
					   LEFT JOIN lab_job job 
							  ON job.job_id = a.job_id 
					   LEFT JOIN rad_sys_user rsu 
							  ON rsu.user_id = a.employee_id 
					   LEFT JOIN lab_employee_business_unit_list BU 
							  ON a.employee_id = BU.employee_id 
					   LEFT JOIN rad_sys_data_accessor rsd 
							  ON BU.business_unit_id = rsd.data_accessor_id) AS d 
		WHERE  d.ShiftReason IS NOT NULL 
				OR d.BreakReason IS NOT NULL 
				OR d.MealReason IS NOT NULL 
------------------------------------------------------------------------------

drop table #tmp_punch_det
drop table #tmp_shift_punch_time
drop table #tmp_meal_punch_time
drop table #tmp_break_punch_time

END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_GetUserProfileInformation]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Exec  [delaPlex_API_GetUserProfileInformation]  1000164

CREATE  PROCEDURE [dbo].[delaPlex_API_GetUserProfileInformation] 
(
	 @employeeId INT 
)
AS
SET NOCOUNT ON 
BEGIN
	SELECT rsd.name AS Site,
		   e.employee_id AS employeeId,
		   rsu.first_name AS firstName,
		   COALESCE(rsu.middle_name,'') AS middleName,
		   COALESCE(rsu.last_name,'') AS lastName,
		   COALESCE(rsu.nickname,'') AS nickName,
		   COALESCE(a.address_line_1,'') AS addressLine1,
		   COALESCE(a.address_line_2,'') AS addressLine2,
		   COALESCE(a.city,'') AS City,
		   COALESCE(a.country_code,'') AS [country],
		   COALESCE(a.home_phone,'') AS [homePhone],
		   COALESCE(a.work_phone,'') AS [workPhone],
		   COALESCE(a.cell_phone,'') AS [cellPhone],
		   COALESCE(e.social_security_number,'') AS [SSN],
		   COALESCE(a.e_mail,'') AS [email],
		   COALESCE(a.state_code,'') AS [State],
		   COALESCE(a.postal_code,'') AS [zipCode],
		   CONVERT(DATE, FORMAT(CONVERT(DATE, e.birth_date),'yyyy-MM-dd')) AS [birthDate],
		   COALESCE(e.emergency_contact_name,'') AS [emergencyContactName],
		   COALESCE(e.emergency_contact_phone_num,'') AS [emergencyContactNumber],
		   COALESCE(e.emergency_contact_relationship_code,'') AS [relationship],
		   COALESCE(e.employee_type_code,'') AS [status],
		   CONVERT(DATE, FORMAT(CONVERT(DATE, e.hire_date),'yyyy-MM-dd')) AS [hireDate],
		   CONVERT(DATE, FORMAT(CONVERT(DATE, e.seniority_date),'yyyy-MM-dd')) AS [seniorityDate],
		   CONCAT('$', lejr.rate) AS [payRate],
		   COALESCE(ncm.contact_method_type_code,'') AS shiftPickup,
		   (SELECT TOP 1 CONVERT(DATE, punch_timestamp)
				FROM Lab_Punch_Audit
				WHERE employee_id = @employeeId
				ORDER BY punch_timestamp DESC) AS [lastPunchDate]
	FROM EMPLOYEE e
	INNER JOIN Rad_Sys_User rsu ON e.employee_id = rsu.user_id
	INNER JOIN[Address] a ON rsu.address_id = a.address_id
	INNER JOIN Lab_Employee_Job_Rate lejr ON e.employee_id = lejr.employee_id
	LEFT JOIN
			(SELECT employee_id,
						Stuff(
							(SELECT ', ' + CASE
												WHEN contact_method_type_code = 'w' THEN 'Work phone'
												WHEN contact_method_type_code = 'h' THEN 'Home phone'
												WHEN contact_method_type_code = 'e' THEN 'Email'
												WHEN contact_method_type_code = 'c' THEN 'Cell Phone'
												WHEN contact_method_type_code = 'f' THEN 'Fax'
												WHEN contact_method_type_code = 't' THEN 'Text'
											END
							FROM Employee_Notification_Contact_Method b
							WHERE a.employee_id = b.employee_id
								FOR XML PATH('')),1, 1, '') contact_method_type_code
			FROM Employee_Notification_Contact_Method a
			GROUP BY employee_id) ncm ON e.employee_id = ncm.employee_id
	INNER JOIN Lab_Employee_Business_Unit_List BU ON bu.employee_id = e.employee_id
	INNER JOIN Rad_Sys_Data_Accessor rsd ON BU.business_unit_id = rsd.Data_accessor_id
	WHERE e.employee_id = @employeeId
END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_Insert_RequestSwap]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[delaPlex_API_Insert_RequestSwap]
	-- Add the parameters for the stored procedure here

	@swapper_id int,
    @swappee_id int,
	@shift_id_swapper int,
	@shift_id_swappee int

------------------------------------------------------------------------

AS
BEGIN
DECLARE @swap_id int; --added--

SET @swap_id = (SELECT MAX(swap_id) + 1 FROM Lab_Sched_Shift_Swap) 

-------------------Insert into Lab_Sched_Shift_Swap - swapper--------------

INSERT INTO lab_sched_Shift_Swap
(swap_id
,swapper_scheduled_shift_id
,swapper_id
,approved_swappee_scheduled_shift_id
,approver_id
,shift_swap_status_code
,shift_swap_status_code_last_updated
,client_id
,last_modified_user_id
,last_modified_timestamp
)
VALUES(@swap_id
,@shift_id_swapper
,@swapper_id
,NULL
,NULL
,'r'								---Requested Shift Swap
,(SELECT CURRENT_TIMESTAMP)
,(SELECT client_id FROM Employee WHERE employee_id = @swapper_id)
,@swapper_id
,(SELECT CURRENT_TIMESTAMP)
)
---------------------------------------------------------------------------------

-------------------Insert into Lab_Sched_Shift_Swap_Swappee - swappee--------------

INSERT INTO Lab_Sched_Shift_Swap_Swappee
(swap_id
,swappee_scheduled_shift_id
,swappee_id
,shift_swappee_status_code
,shift_swappee_status_code_last_updated
,client_id
,last_modified_user_id
,last_modified_timestamp
)
VALUES(@swap_id
,@shift_id_swappee
,@swappee_id
,'r'								---Requested Shift Swap
,(SELECT CURRENT_TIMESTAMP)
,(SELECT client_id FROM Employee WHERE employee_id = @swappee_id)
,@swapper_id
,(SELECT CURRENT_TIMESTAMP)
)

---------------------------------------------------------------------------------

END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_MarkNotificationReadUnread]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[delaPlex_API_MarkNotificationReadUnread] 
(
	 @MailId INT,
	 @pendingFlag VARCHAR(1)

)
AS
SET NOCOUNT ON 
BEGIN

	UPDATE Mail_User_List SET pending_flag = @pendingFlag WHERE mail_id = @MailId
	
END

 

 
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_Update_AcknowledgeEditPunch]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
---exec delaPlex_API_Update_AcknowledgeEditPunch 1000161,1000171
--=============================================
CREATE PROCEDURE [dbo].[delaPlex_API_Update_AcknowledgeEditPunch]
    @Employee_id int,
	@punch_edit_approval_id int
	
AS
BEGIN



--========================================================================
--approval_code = 'a' ----> Acknowledged
--				= 'i' ------> Unread

--========================================================================

--------------Update Lab_Punch_edit_approval----------------------

UPDATE Lab_Punch_edit_approval
SET 
approval_code= 'a'
,last_modified_user_id = @Employee_id
,last_modified_timestamp = CURRENT_TIMESTAMP
,audit_timestamp = CURRENT_TIMESTAMP
WHERE approval_code = 'i'
AND  punch_edit_approval_id = @punch_edit_approval_id

----------------------------------------------------------------------------


END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_Update_ApproveSwap]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
---exec approve_by_swappee 1000179,1000161,1000121
---select * from Lab_Sched_Shift_Swap
---select * from Lab_Sched_Shift_Swap_Swappee
--=============================================
CREATE PROCEDURE [dbo].[delaPlex_API_Update_ApproveSwap]
	@swapper_id int,
    @swappee_id int,
	@swap_id int
	
AS
BEGIN
------------Approve shift swap from swappee----------------------
--================================================================
--shift_swap_status_code = 'w' ---> Awaiting approval from Manager
--						 = 'r' ---> Requested shift swap by Swappper

--shift_swappee_status_code = 'y' ---> Awaiting approval from Manager
--						 = 'r' ---> Requested shift swap by Swappper

--===================================================================

--------------Update Lab_Sched_Shift_Swap -  swapper----------------------

UPDATE Lab_Sched_Shift_Swap
SET 
shift_swap_status_code = 'w'
,shift_swap_status_code_last_updated = CURRENT_TIMESTAMP
,last_modified_user_id = @swappee_id
,last_modified_timestamp = CURRENT_TIMESTAMP
WHERE  shift_swap_status_code = 'r'
AND swap_id = @swap_id

----------------------------------------------------------------------------


--------------Update Lab_Sched_Shift_Swap_Swappee -  swappee----------------------

UPDATE Lab_Sched_Shift_Swap_Swappee
SET 
shift_swappee_status_code = 'y'
,shift_swappee_status_code_last_updated = CURRENT_TIMESTAMP
,last_modified_user_id = @swappee_id
,last_modified_timestamp = CURRENT_TIMESTAMP
WHERE shift_swappee_status_code = 'r'
AND swap_id = @swap_id

----------------------------------------------------------------------------

END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_Update_CancelRequestSwap]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--=============================================
---exec delaPlex_API_Update_CancelRequestSwap 1000179,1000121
---select * from Lab_Sched_Shift_Swap
---select * from Lab_Sched_Shift_Swap_Swappee
--=============================================
CREATE PROCEDURE [dbo].[delaPlex_API_Update_CancelRequestSwap]
    @swapper_id int,
	@swap_id int
	
AS
BEGIN

------------Cancel shift swap request from swapper---------------------------------

--========================================================================
--shift_swap_status_code = 'c' ---> Cancelled by Swapper

--shift_swappee_status_code = 'c' ---> Cancelled by Swapper

--========================================================================

--------------Update Lab_Sched_Shift_Swap -  swapper----------------------

UPDATE Lab_Sched_Shift_Swap
SET 
shift_swap_status_code = 'c'
,shift_swap_status_code_last_updated = CURRENT_TIMESTAMP
,last_modified_user_id = @swapper_id
,last_modified_timestamp = CURRENT_TIMESTAMP
WHERE swap_id = @swap_id

----------------------------------------------------------------------------


--------------Update Lab_Sched_Shift_Swap_Swappee -  swappee----------------------

UPDATE Lab_Sched_Shift_Swap_Swappee
SET 
shift_swappee_status_code = 'c'
,shift_swappee_status_code_last_updated = CURRENT_TIMESTAMP
,last_modified_user_id = @swapper_id
,last_modified_timestamp = CURRENT_TIMESTAMP
WHERE swap_id = @swap_id

----------------------------------------------------------------------------

END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_Update_ClaimAvailableShift]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[delaPlex_API_Update_ClaimAvailableShift] 
(				
	@EmployeeId int,
	@ShiftId int
)	
AS
BEGIN

------------Claim Available shift request---------------------------------


--------------Update Lab_Sched_Shift---------------------------------------

UPDATE Lab_Sched_Shift
	SET employee_id=@EmployeeId,
	last_modified_user_id = @EmployeeId,
	last_modified_timestamp = CURRENT_TIMESTAMP
WHERE scheduled_shift_id=@ShiftId

----------------------------------------------------------------------------

END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_Update_DenySwap]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
---exec delaPlex_API_Update_DenySwap 1000161,1000122
---select * from Lab_Sched_Shift_Swap
---select * from Lab_Sched_Shift_Swap_Swappee
--=============================================
CREATE PROCEDURE [dbo].[delaPlex_API_Update_DenySwap]
    @swappee_id int,
	@swap_id int
	
AS
BEGIN

------------Deny shift swap from swappee---------------------------------

--========================================================================
--shift_swap_status_code = 'x' ---> Denied by Swappee
--						 = 'r' ---> Requested shift swap by Swappper

--shift_swappee_status_code = 'n' ---> Denied by Swappee
--						 = 'r' ---> Requested shift swap by Swappper

--========================================================================

--------------Update Lab_Sched_Shift_Swap -  swapper----------------------

UPDATE Lab_Sched_Shift_Swap
SET 
shift_swap_status_code = 'x'
,shift_swap_status_code_last_updated = CURRENT_TIMESTAMP
,last_modified_user_id = @Swappee_id
,last_modified_timestamp = CURRENT_TIMESTAMP
WHERE shift_swap_status_code = 'r'
AND swap_id = @swap_id

----------------------------------------------------------------------------


--------------Update Lab_Sched_Shift_Swap_Swappee -  swappee----------------------

UPDATE Lab_Sched_Shift_Swap_Swappee
SET 
shift_swappee_status_code = 'n'
,shift_swappee_status_code_last_updated = CURRENT_TIMESTAMP
,last_modified_user_id = @Swappee_id
,last_modified_timestamp = CURRENT_TIMESTAMP
WHERE shift_swappee_status_code = 'r'
AND swap_id = @swap_id

----------------------------------------------------------------------------

END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_Update_DisputedEditPunch]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
---exec delaPlex_API_Update_DisputedEditPunch 1000161,1000171
--=============================================
CREATE PROCEDURE [dbo].[delaPlex_API_Update_DisputedEditPunch]
    @Employee_id int,
	@punch_edit_approval_id int
	
AS
BEGIN



--========================================================================
--approval_code = 'r' ----> Disputed
--				= 'i' ------> Unread

--========================================================================

--------------Update Lab_Punch_edit_approval----------------------

UPDATE Lab_Punch_edit_approval
SET 
approval_code= 'r'
,last_modified_user_id = @Employee_id
,last_modified_timestamp = CURRENT_TIMESTAMP
,audit_timestamp = CURRENT_TIMESTAMP
WHERE approval_code = 'i'
AND  punch_edit_approval_id = @punch_edit_approval_id

----------------------------------------------------------------------------


END
GO
/****** Object:  StoredProcedure [dbo].[delaPlex_API_UpdateUserEmergencyContactInformation]    Script Date: 3/26/2020 4:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[delaPlex_API_UpdateUserEmergencyContactInformation] 
(
	 @employeeId INT ,
	 @emergency_contact_name Varchar(100),
	@emergency_contact_phone_num Varchar(100),
	@emergency_contact_relationship_code Varchar(100),
	@contact_method_type_code Varchar(100)
)
AS
SET NOCOUNT ON 
BEGIN
	UPDATE employee  SET 
		emergency_contact_name =@emergency_contact_name ,
		emergency_contact_phone_num  =@emergency_contact_phone_num,
		emergency_contact_relationship_code =@emergency_contact_relationship_code 
	WHERE employee_id = @employeeId


	UPDATE Employee_Notification_Contact_Method SET contact_method_type_code = @contact_method_type_code 
	WHERE employee_id = @employeeId
END

 
 --SELECT * FROM Employee_Notification_Contact_Method
 --SELECT * FRoM EMployee
GO
