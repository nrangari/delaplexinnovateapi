﻿using delaPlexAPI.Models;
using delaPlexAPI.Models.ESOReceive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Services.MobileUser
{
    public interface IReceiveService
    {
        EsoReceive GetReceivingData(int siteId);
    }
}
