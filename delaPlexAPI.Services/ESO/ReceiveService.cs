﻿using delaPlexAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using delaPlexAPI.Models.Common;
using delaPlexAPI.Models.ESOReceive;
using System.Reflection;

namespace delaPlexAPI.Services.MobileUser
{
    public class ReceiveService :  IReceiveService
    {
        public string RefSessionId { get; set; }

        private ResponceResult _responce;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public EsoReceive GetReceivingData(int siteId)
        {
            //string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Receiveing.json");

            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var filename = Path.Combine(documents, "Receiveing.json");

            EsoReceive items = new EsoReceive();
            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                 items = JsonConvert.DeserializeObject<EsoReceive>(json);
            }

            return items;
        }
    }
}
