﻿using delaPlexAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Services.PrivateApi
{
    public interface IEssPrivateService
    {
        MyTimeCard GetMyTimeCard(string refSessionId, int siteId, DateTime date);
        string UpdateSwapShiftRequest(UpdateShiftSwap updateShiftSwap);
        string ProcessShiftSwap(ProcessShiftSwap processShiftSwap);
        string DeleteMyNotification(DeleteNotificationParam deleteNotificationParam);
        List<MyNotification> GetMyNotification(string refSessionId, int siteId);
        List<ShiftSwap> ShiftSwapAvailableShift(string refSessionId, int shiftID, int siteId);
        decimal GetScheduledHours(string refSessionId, int siteId, DateTime date);
        List<TimeOffType> GetAdjustmentTypes(int siteId, string refSessionId);
        List<MyAvailabilityRequest> GetMyAvailabilityRequest(string refSessionId, int siteId);
        List<EditedPunches> GetEditedPunches(string refSessionId, int siteId, DateTime date, string statusCode);
        List<AvailableShifts> GetUnfilledShift(int employeeId, DateTime date, int siteId, string refSessionId);
        string SetPunchEditStatus(SetPunchEditStatus setPunchEditStatus);
        List<TorHistoryResponce> GetEmployeeTORHistory(TorHistoryApiParam torHistoryApiParam);
        List<TimeOffRequestData> GetEmployeeTimeOffRequest(int siteId, int requestID, string refSessionId);
    }
}
