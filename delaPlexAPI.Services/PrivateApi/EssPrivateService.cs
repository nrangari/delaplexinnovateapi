﻿using delaPlexAPI.Models;
using delaPlexAPI.Models.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace delaPlexAPI.Services.PrivateApi
{
    public class EssPrivateService : IEssPrivateService
    {

        public string RefSessionId { get; set; }

        private ResponceResult _responce;

        public MyTimeCard GetMyTimeCard(string refSessionId, int siteId, DateTime date)
        {
            MyTimeCard myTimeCard = new MyTimeCard();
            List<PayPunchedShift> lstPayPunchedShift = new List<PayPunchedShift>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/"));
                    var response = client
                        .GetAsync($"api/MyTimecard/{date.ToString("yyyy-MM-dd")}?siteId={siteId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    myTimeCard.NetPunchedHours = dynamicObject.NetPunchedHours == null ? 0 : dynamicObject.NetPunchedHours;
                    myTimeCard.NetPayAdjustmentHours = dynamicObject.NetPayAdjustmentHours == null ? 0 : dynamicObject.NetPayAdjustmentHours;

                    foreach (var days in dynamicObject.Days)
                    {
                        foreach (var payPunchedShifts in days.PayPunchedShifts)
                        {
                            List<Details> lstDetails = new List<Details>();
                            foreach (var details in payPunchedShifts.Details)
                            {
                                var detail = new Details
                                {
                                    ID = details.ID,
                                    DetailType = details.DetailType,
                                    Start = details.Start,
                                    End = details.End
                                    //,NetHours = details.NetHours
                                };
                                lstDetails.Add(detail);
                            }
                            var PayPunchedShift = new PayPunchedShift
                            {
                                PunchedShiftID = payPunchedShifts.PunchedShiftID,
                                shiftDate = days.ID,
                                Start = payPunchedShifts.Start,
                                End = payPunchedShifts.End,
                                NetPunchedHours = payPunchedShifts.NetPunchedHours == null ? 0 : payPunchedShifts.NetPunchedHours,
                                details = lstDetails
                            };
                            lstPayPunchedShift.Add(PayPunchedShift);
                            // myTimeCard.payPunchedShifts.Add(payPunchedShifts);
                        }
                    }

                    myTimeCard.payPunchedShifts = lstPayPunchedShift;
                }
            }
            catch (Exception ex)
            {
                return myTimeCard;
            }

            return myTimeCard;
        }

        public string UpdateSwapShiftRequest(UpdateShiftSwap updateShiftSwap)
        {
            string result = "";
            try
            {
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", updateShiftSwap.refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/Commands.asmx/"));
                    //{ "jsChangeCommands":[{"__TYPE":"ESS.Commands.SwapShift.UpdateSwapShiftRequest","SwapShiftID":1000145,"RequestedScheduledShiftID":1002134,"RequestStatus":"AwaitingManager"}]}
                    //{"jsChangeCommands":[{"__TYPE":"ESS.Commands.SwapShift.UpdateSwapShiftRequest","SwapShiftID":1000144,"RequestedScheduledShiftID":1002124,"RequestStatus":"RecipientDenied"}]}


                    string requestStatus = string.Empty;
                    switch (updateShiftSwap.ActionFlag)
                    {
                        case "A":
                            requestStatus = "AwaitingManager";
                            break;
                        case "D":
                            requestStatus = "RecipientDenied";
                            break;
                        case "C":
                            requestStatus = "SenderCanceled";
                            break;
                    }

                    List<ProcessShiftSwapRequest> lstProcessCollection = new List<ProcessShiftSwapRequest>();
                    var process = new ProcessShiftSwapRequest
                    {
                        __TYPE = "ESS.Commands.SwapShift.UpdateSwapShiftRequest",
                        SwapShiftID = updateShiftSwap.SwapShiftID,
                        RequestedScheduledShiftID = updateShiftSwap.RequestedScheduledShiftID,
                        RequestStatus = requestStatus
                    };
                    lstProcessCollection.Add(process);
                    var data = new
                    {
                        jsChangeCommands = lstProcessCollection
                    };

                    //  string json = JsonConvert.SerializeObject(data);
                    HttpResponseMessage response = client1.PostAsJsonAsync("ProcessCommands?siteId=" + updateShiftSwap.siteId, data).Result;

                    var result1 = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var VARIABLE in dynamicObject.data.Errors)
                    {
                        result = VARIABLE.Type;
                    }
                    if (string.IsNullOrEmpty(result)) // (response.StatusCode == HttpStatusCode.OK)
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public string ProcessShiftSwap(ProcessShiftSwap processShiftSwap)
        {
            string result = "";
            try
            {
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", processShiftSwap.refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/Commands.asmx/"));
                    //{ "jsChangeCommands":[{"__TYPE":"ESS.Commands.SwapShift.SubmitSwapShiftRequests","RequestedScheduledShiftIDs":[1002144,1002083],"OriginalScheduledShiftID":1002134}]}

                    List<ProcessCollection> lstProcessCollection = new List<ProcessCollection>();
                    var process = new ProcessCollection
                    {
                        __TYPE = "ESS.Commands.SwapShift.SubmitSwapShiftRequests",
                        RequestedScheduledShiftIDs = processShiftSwap.RequestedScheduledShiftIDs,
                        OriginalScheduledShiftID = processShiftSwap.OriginalScheduledShiftID
                    };
                    lstProcessCollection.Add(process);
                    var data = new
                    {
                        jsChangeCommands = lstProcessCollection
                    };
                    //string json = JsonConvert.SerializeObject(data);
                    HttpResponseMessage response = client1.PostAsJsonAsync("ProcessCommands?siteId=" + processShiftSwap.siteId, data).Result;

                    var result1 = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var VARIABLE in dynamicObject.data.Errors)
                    {
                        result = VARIABLE.Type;
                    }
                    if (string.IsNullOrEmpty(result)) // (response.StatusCode == HttpStatusCode.OK)
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;

        }

        public string DeleteMyNotification(DeleteNotificationParam deleteNotificationParam)
        {
            string result = "";

            try
            {
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", deleteNotificationParam.refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/Commands.asmx/"));
                    //jsChangeCommands":[{"__TYPE":"ESS.Commands.Mail.DeleteMail","MailID":1004559}]

                    List<DeleteNotification> lstDeleteNotification = new List<DeleteNotification>();

                    foreach (var mailid in deleteNotificationParam.MailId)
                    {
                        var process = new DeleteNotification
                        {
                            __TYPE = "ESS.Commands.Mail.DeleteMail",
                            MailID = mailid
                        };
                        lstDeleteNotification.Add(process);
                    }
                    var data = new
                    {
                        jsChangeCommands = lstDeleteNotification
                    };
                    //  string json = JsonConvert.SerializeObject(data);
                    HttpResponseMessage response = client1.PostAsJsonAsync("ProcessCommands?siteId=" + deleteNotificationParam.siteId, data).Result;
                    var result1 = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var VARIABLE in dynamicObject.data.Errors)
                    {
                        result = VARIABLE.Type;
                    }
                    if (string.IsNullOrEmpty(result)) // (response.StatusCode == HttpStatusCode.OK)
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }
        public List<MyNotification> GetMyNotification(string refSessionId, int siteId)
        {

            List<MyNotification> lstMyNotification = new List<MyNotification>();
            using (var client1 = new HttpClient())
            {
                client1.DefaultRequestHeaders.Accept.Clear();
                client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client1.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/MailInbox.asmx/"));
                var data = new
                {
                    numberOfMessages = 100
                };
                var response = client1.PostAsJsonAsync("GetTopMail?siteId=" + siteId, data).Result;

                var result = response.Content.ReadAsStringAsync().Result;

                dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                foreach (var o in dynamicObject.data)
                {
                    var message = new Message
                    {
                        Parameters = o.Message.Parameters,
                        Text = o.Message.Text
                    };

                    var myNotification = new MyNotification
                    {
                        Message = message,
                        Date = o.Date,
                        FromFirstName = o.FromFirstName,
                        FromLastName = o.FromLastName,
                        ID = o.ID,
                        IsRead = o.IsRead,
                        IsSystemAlert = o.IsSystemAlert,
                        Priority = o.Priority
                    };
                    lstMyNotification.Add(myNotification);
                }
            }
            return lstMyNotification;
        }

        public List<ShiftSwap> ShiftSwapAvailableShift(string refSessionId, int shiftID, int siteId)
        {
            List<ShiftSwap> lstShiftSwap = new List<ShiftSwap>();
            try
            {
                var data = new
                {
                    myShiftID = shiftID
                };
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/SwapShift.asmx/"));
                    var response = client1.PostAsJsonAsync("GetAvailableShifts?siteId=" + siteId, data).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var o in dynamicObject.data)
                    {
                        var ShiftSwap = new ShiftSwap
                        {
                            ShiftDate = o.ShiftStart.ToString("yyyy-MM-dd"),
                            ShiftEnd = o.ShiftEnd.ToString("HH:mm"),
                            FixedShift = o.FixedShift,
                            NetHoursDelta = o.NetHoursDelta,
                            JobNames = o.JobNames,
                            ShiftID = o.ShiftID,
                            EmployeeName = o.EmployeeName,
                            ShiftStart = o.ShiftStart.ToString("HH:mm"),
                            JobColor = o.JobColor
                        };
                        lstShiftSwap.Add(ShiftSwap);
                    }
                }
            }
            catch (Exception ex)
            {
                // ignored
            }

            return lstShiftSwap;
        }
        public decimal GetScheduledHours(string refSessionId, int siteId, DateTime date)
        {
            decimal NetScheduledHours = 0;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/"));
                    var response = client
                        .GetAsync($"api/MySchedule/{date.ToString("yyyy-MM-dd")}?siteId={siteId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    NetScheduledHours = dynamicObject.NetScheduledHours == null ? 0 : dynamicObject.NetScheduledHours;
                }
            }
            catch (Exception ex)
            {
                return NetScheduledHours;
            }
            return NetScheduledHours;
        }

        public List<TimeOffType> GetAdjustmentTypes(int siteId, string refSessionId)
        {
            List<TimeOffType> lsttimeOffTypess = new List<TimeOffType>();
            try
            {
                //string refSessionId = GetRefSessionIdFromClientAdminUser();
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/TimeOffRequest.asmx/"));
                    var response = client
                        .GetAsync($"GetAdjustmentTypes?siteId={siteId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    foreach (var timeOffTypes in dynamicObject.data)
                    {
                        var timeOffType = new TimeOffType
                        {
                            timeOffTypeID = timeOffTypes.Value,
                            timeOffType = timeOffTypes.Name
                        };

                        lsttimeOffTypess.Add(timeOffType);
                    }
                }
            }
            catch (Exception ex)
            {
                // ignored
                return lsttimeOffTypess;
            }
            return lsttimeOffTypess;
        }

        public List<MyAvailabilityRequest> GetMyAvailabilityRequest(string refSessionId, int siteId)
        {

            List<MyAvailabilityRequest> lstAvailabilityRequests = new List<MyAvailabilityRequest>();
            using (var client1 = new HttpClient())
            {
                client1.DefaultRequestHeaders.Accept.Clear();
                client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client1.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/api/"));
                var response = client1.GetAsync("myavailabilityrequest?siteId=" + siteId).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                foreach (var o in dynamicObject)
                {

                    List<AvailabilityRequestRange> lstAvailabilityRequestRange =
                        new List<AvailabilityRequestRange>();

                    foreach (var AvailabilityRequestRange in o.AvailabilityRequestRange)
                    {
                        var availabilityRequestRange = new AvailabilityRequestRange
                        {
                            ID = AvailabilityRequestRange.ID,
                            DayOfWeek = AvailabilityRequestRange.DayOfWeek,
                            StartOffset = AvailabilityRequestRange.StartOffset,
                            EndOffset = AvailabilityRequestRange.EndOffset,
                            AvailabilityType = AvailabilityRequestRange.AvailabilityType
                        };
                        lstAvailabilityRequestRange.Add(availabilityRequestRange);
                    }

                    var AvailabilityRequests = new MyAvailabilityRequest
                    {
                        ID = o.ID,
                        Start = o.Start,
                        LastStatusUpdateUserID = o.LastStatusUpdateUserID,
                        LastStatusUpdateUserName = o.LastStatusUpdateUserName,
                        LastStatusUpdateTimestamp = o.LastStatusUpdateTimestamp,
                        RequestedTimestamp = o.RequestedTimestamp,
                        End = o.End,
                        Comment = o.Comment,
                        Status = o.Status,
                        AvailabilityRequestRange = lstAvailabilityRequestRange
                    };

                    lstAvailabilityRequests.Add(AvailabilityRequests);

                }
            }

            return lstAvailabilityRequests;
        }

        public List<EditedPunches> GetEditedPunches(string refSessionId, int siteId, DateTime date, string statusCode)
        {
            List<EditedPunches> lstEditedPunches = new List<EditedPunches>();
            try
            {
                var data = new
                {
                    date = string.Concat(date.ToString("yyyy-MM-dd"), "T00:00:00"),
                    //"2018-10-11T00:00:00",
                    statusCode = statusCode
                };
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/TnA.asmx/"));
                    var response = client1.PostAsJsonAsync("GetPunchEdits?siteId=" + siteId, data).Result;
                    var result = response.Content.ReadAsStringAsync().Result;


                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var o in dynamicObject.data)
                    {
                        var EditedPunch = new EditedPunches
                        {
                            ScheduledShiftEndTime = o.ScheduledShiftEndTime == "" ? null : o.ScheduledShiftEndTime,
                            Comments = o.Comments,
                            ScheduledShiftStartTime =
                                o.ScheduledShiftStartTime == "" ? null : o.ScheduledShiftStartTime,
                            ScheduledJob = o.ScheduledJob,
                            PunchTypeCode = o.PunchTypeCode,
                            StatusCode = o.StatusCode,
                            Reason = o.Reason,
                            NewPunchTime = o.NewPunchTime == "" ? null : o.NewPunchTime,
                            ChangeType = o.ChangeType,
                            EditedOn = o.EditedOn,
                            OldJob = o.OldJob,
                            ID = o.ID,
                            OldPunchTime = o.OldPunchTime == "" ? null : o.OldPunchTime,
                            NewJob = o.NewJob,
                            EditedBy = o.EditedBy,
                            PunchDate = o.PunchDate == null ? "" : o.PunchDate
                        };
                        lstEditedPunches.Add(EditedPunch);
                    }

                    // result = response1.StatusCode == HttpStatusCode.OK ? "Success" : "Error";
                }
            }
            catch (Exception ex)
            {
                return lstEditedPunches;
            }

            return lstEditedPunches;
        }

        public List<AvailableShifts> GetUnfilledShift(int employeeId, DateTime date, int siteId, string refSessionId)
        {
            List<AvailableShifts> lstAvailableShifts = new List<AvailableShifts>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/"));
                    var response = client
                        .GetAsync($"api/AvailableUnfilledShifts?selectedWeekDay={date.ToString("yyyy-MM-dd")}&siteId={siteId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    foreach (var o in dynamicObject)
                    {
                        var availableShifts = new AvailableShifts
                        {
                            ShiftId = o.ShiftID,
                            Date = o.Start.ToString("yyyy-MM-dd"),
                            ShiftStart = o.Start.ToString("HH:mm"),
                            ShiftEnd = o.End.ToString("HH:mm"),
                            Site = o.SiteName,
                            Job = o.JobNames
                        };
                        lstAvailableShifts.Add(availableShifts);
                    }
                }
            }
            catch (Exception e)
            {
                return lstAvailableShifts;
            }

            return lstAvailableShifts;

        }

        public string SetPunchEditStatus(SetPunchEditStatus setPunchEditStatus)
        {
            string result = string.Empty;

            try
            {
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", setPunchEditStatus.REFSSessionID);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/Commands.asmx/"));
                    if (setPunchEditStatus.StatusType == "a")
                    {
                        //Acknowldge
                        List<SetPunchEditStatusParamAcknowldge> lstSetPunchEditStatusParam = new List<SetPunchEditStatusParamAcknowldge>();

                        foreach (var punchEditId in setPunchEditStatus.PunchEditID)
                        {
                            var process = new SetPunchEditStatusParamAcknowldge
                            {
                                __TYPE = "ESS.Commands.PunchEdit.SetPunchEditStatus",
                                PunchEditID = punchEditId,
                                StatusType = setPunchEditStatus.StatusType
                            };
                            lstSetPunchEditStatusParam.Add(process);
                        }

                        var data = new
                        {
                            jsChangeCommands = lstSetPunchEditStatusParam
                        };
                        //  string json = JsonConvert.SerializeObject(data);
                        HttpResponseMessage response = client1
                            .PostAsJsonAsync("ProcessCommands?siteId=" + setPunchEditStatus.SiteId, data).Result;


                        var result1 = response.Content.ReadAsStringAsync().Result;

                        dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                        foreach (var error in dynamicObject.data.Errors)
                        {
                            result = error.Type;
                        }
                        if (string.IsNullOrEmpty(result)) // (response.StatusCode == HttpStatusCode.OK)
                        {
                            result = "Success";
                        }
                        //if (response.StatusCode == HttpStatusCode.OK)
                        //{
                        //    result = "Success";
                        //}
                        //else result = response.ToString();
                    }
                    else
                    {
                        //Dispute
                        List<SetPunchEditStatusParamDispute> lstSetPunchEditStatusParamDispute = new List<SetPunchEditStatusParamDispute>();

                        foreach (var punchEditId in setPunchEditStatus.PunchEditID)
                        {
                            var processDispute = new SetPunchEditStatusParamDispute
                            {
                                __TYPE = "ESS.Commands.PunchEdit.SetPunchEditStatus",
                                PunchEditID = punchEditId,
                                StatusType = setPunchEditStatus.StatusType,
                                Comment = setPunchEditStatus.Comment
                            };
                            lstSetPunchEditStatusParamDispute.Add(processDispute);
                        }

                        var data = new
                        {
                            jsChangeCommands = lstSetPunchEditStatusParamDispute
                        };
                        //  string json = JsonConvert.SerializeObject(data);
                        var response = client1
                            .PostAsJsonAsync("ProcessCommands?siteId=" + setPunchEditStatus.SiteId, data).Result;
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            result = "Success";
                        }
                        else result = response.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return result;
        }

        public List<TorHistoryResponce> GetEmployeeTORHistory(TorHistoryApiParam torHistoryApiParam)
        {
            List<TorHistoryResponce> lstTorHistory = new List<TorHistoryResponce>();
            try
            {
                DateTime dt = torHistoryApiParam.startTime.AddMonths(-16);

                var data = new
                {
                    startTime = string.Concat(dt.ToString("yyyy-MM-dd"), "T00:00:00"),
                    includeRequested = true,
                    includeApproved = true,
                    includeDenied = true,
                    includeCanceled = true
                };
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", torHistoryApiParam.RefSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/TimeOffRequest.asmx/"));
                    var response = client1.PostAsJsonAsync($"GetEmployeeTORHistory?siteId={torHistoryApiParam.siteId}", data).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var o in dynamicObject.data)
                    {
                        string statusCode = o.StatusCode;
                        string status = TimeOffRequestStatusCode.TimeOffStatusCode(statusCode);
                        var torHistoryResponce = new TorHistoryResponce
                        {
                            Submitted = o.Submitted,// 2018 -08-30T04:54:35",
                            TimeOffID = o.TimeOffID, //: 1000123,
                            Start = o.Start, //2018-09-09T00:00:00",
                            End = o.End,// "2018-09-20T23:59:00",
                            StatusCode = status//o.StatusCode//": "a"
                        };
                        lstTorHistory.Add(torHistoryResponce);
                    }

                    // result = response1.StatusCode == HttpStatusCode.OK ? "Success" : "Error";
                }
            }
            catch (Exception ex)
            {
                return lstTorHistory;
            }
            return lstTorHistory;
        }


        public List<TimeOffRequestData> GetEmployeeTimeOffRequest(int siteId, int requestID, string refSessionId)
        {
            List<TimeOffRequestData> lstTimeOff = new List<TimeOffRequestData>();
            List<TimeOffRequestComments> lstTimeOffComment = new List<TimeOffRequestComments>();

            try
            {
                var data = new
                {
                    requestID = requestID
                };
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/TimeOffRequest.asmx/"));
                    var response = client1.PostAsJsonAsync("GetRequest?siteId=" + siteId, data).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    string status;
                    string statusCode;
                    foreach (var o in dynamicObject.data)
                    {
                        foreach (var audits in o.Audits)
                        {
                            statusCode = audits.StatusCode;
                            status = TimeOffRequestStatusCode.TimeOffStatusCode(statusCode);
                            var timeOffRequestComments = new TimeOffRequestComments
                            {
                                Timestamp = audits.Timestamp,
                                AuthorFirstName = audits.AuthorFirstName,
                                AuthorID = audits.AuthorID,
                                AuthorLastName = audits.AuthorLastName,
                                Comment = audits.Comment,
                                StatusCode = status//  audits.StatusCode
                            };
                            lstTimeOffComment.Add(timeOffRequestComments);
                        }
                        statusCode = o.StatusCode;
                        status = TimeOffRequestStatusCode.TimeOffStatusCode(statusCode);
                        var timeOffRequestData = new TimeOffRequestData
                        {
                            Start = o.Start,
                            Comment = o.Comment,
                            EmployeeID = o.EmployeeID,
                            End = o.End,
                            HalfDay = o.HalfDay,
                            ID = o.ID,
                            IsTrackedAsWholeDays = o.IsTrackedAsWholeDays,
                            LastModifiedTimestamp = o.LastModifiedTimestamp,
                            PayAdjustmentID = o.PayAdjustmentID,
                            PayAdjustmentName = o.PayAdjustmentName,
                            StatusCode = status,// o.StatusCode,
                            timeOffRequestComments = lstTimeOffComment
                        };
                        lstTimeOff.Add(timeOffRequestData);
                    }

                    // result = response1.StatusCode == HttpStatusCode.OK ? "Success" : "Error";
                }
            }
            catch (Exception ex)
            {
                return lstTimeOff;
            }


            return lstTimeOff;
        }
    }
}
