﻿using delaPlexAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Services.MobileUser
{
    public interface IMobileUserService
    {
        ResponceResult LoginUser(string userName, string password);

        List<EmployeeAccruals> GetEmployeeAccruals(int employeeId);

        List<TimeOffType> GetAdjustmentTypes(int siteId, string refSessionId);
    }
}
