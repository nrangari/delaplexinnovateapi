﻿using delaPlexAPI.Models;
using delaPlexAPI.Models.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace delaPlexAPI.Services.MobileUser
{
    public class MobileUserService : IMobileUserService
    {
        public string RefSessionId { get; set; }

        private ResponceResult _responce;
        public ResponceResult LoginUser(string userName, string password)
        {
            return ValidateMobileUser(userName, password);

        }

        private ResponceResult ValidateMobileUser(string userName, string password)
        {
            _responce = new ResponceResult();
            try
            {

                var request = WebRequest.Create(string.Concat(AppSettings.JdaServerUrl, "retail/data/login")) as HttpWebRequest;
                request.CookieContainer = new CookieContainer();
                request.Method = "POST";
                string postData = string.Concat("loginName=", userName, "&password=", password);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                var response = request.GetResponse() as HttpWebResponse;
                string siteName = string.Empty;
                if (response != null)
                {
                    RefSessionId = response.Cookies["REFSSessionID"].Value.ToString();
                    Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd();
                    dynamic values = JObject.Parse(responseFromServer);
                    siteName = values.data.siteId;
                    Console.WriteLine(responseFromServer);
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                }

                if (!string.IsNullOrEmpty(RefSessionId))
                {
                    _responce = GetMobileUserId(userName, siteName);
                    _responce.CanAccessEssApplication = IsMobileEssAccess(RefSessionId, siteName);
                    //_responce.UserID = _responce.UserID; //GetMobileUserId(userName);
                    _responce.REFSessionId = RefSessionId;
                    _responce.returnStatus = "Success";
                    _responce.HttpStatusCode = 200;
                }
                else
                {
                    _responce.UserID = "0";
                    _responce.returnStatus = "Fail";
                    _responce.HttpStatusCode = 500;
                }
            }
            catch (Exception ex)
            {
                return _responce;
            }

            return _responce;
        }


        public string GetRefSessionIdFromClientAdminUser()
        {
            var request = WebRequest.Create(string.Concat(AppSettings.JdaServerUrl, "retail/data/login")) as HttpWebRequest;
            request.CookieContainer = new CookieContainer();
            request.Method = "POST";
            string postData = string.Concat("loginName=", GetApiUserName(), "&password=", GetApiUserPassword());
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            var response = request.GetResponse() as HttpWebResponse;

            if (response == null) return string.Empty;

            string refSessionId = response.Cookies["REFSSessionID"].Value.ToString();
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            dataStream = response.GetResponseStream();
            if (dataStream != null)
            {
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                Console.WriteLine(responseFromServer);
                reader.Close();
            }

            dataStream?.Close();
            response.Close();

            return refSessionId;

        }

        private ResponceResult GetMobileUserId(string userName, string siteName)
        {
            string employeeId = string.Empty;
            ResponceResult responceResult = new ResponceResult();

            string adminRefSessionId = GetRefSessionIdFromClientAdminUser();

            if (!string.IsNullOrEmpty(adminRefSessionId))
            {
                // Code to get Employee Details
                WebRequest tRequest =
                    WebRequest.Create(string.Concat(AppSettings.JdaServerUrl, "retail/data/retailwebapi/api/", AppSettings.JdaApiVersion, "/users?loginName=", userName));
                tRequest.Method = "GET";
                tRequest.Headers.Add($"REFSSessionID:{adminRefSessionId}");
                tRequest.ContentType = "application/json";

                using (WebResponse response = tRequest.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        if (stream != null)
                            using (StreamReader tReader = new StreamReader(stream))
                            {
                                string sResponseFromServer = tReader.ReadToEnd();
                                dynamic values = JObject.Parse(sResponseFromServer);

                                responceResult = new ResponceResult
                                {
                                    UserID = values.userID,
                                    siteId = values.defaultOrgHierarchyID,
                                    firstName = values.name.firstName,
                                    lastName = values.name.lastName,
                                    middleName = values.name.middleName,
                                    ForcePasswordChange = values.forcePasswordChange
                                };


                                foreach (var userHierarchies in values.userHierarchyCollection.userHierarchies)
                                {
                                    string strSiteName = userHierarchies.organizationalHierarchyCode;
                                    if (siteName == strSiteName)
                                    {
                                        responceResult.siteId = userHierarchies.organizationalHierarchyID;
                                        break;
                                    }
                                }
                                //employeeId = values.userID;
                            }
                    }
                }
            }

            return responceResult;
        }

        private bool IsMobileEssAccess(string refSessionId, string siteName)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client
                        .GetAsync($"retail/portal/globals.js?siteId={siteName}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (result.Contains("Employee-Self-Service"))
                        return true;
                    else
                        return false;
                    // dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    //  NetScheduledHours = dynamicObject.NetScheduledHours == null ? 0 : dynamicObject.NetScheduledHours;
                }
            }
            catch (Exception ex)
            {
                return false;
                //  return NetScheduledHours;
            }
            return false;
        }

        private static string GetApiUserName()
        {
            return ConfigurationManager.AppSettings["ApiUserName"].ToString();
        }

        private static string GetApiUserPassword()
        {
            return ConfigurationManager.AppSettings["ApiPassword"].ToString();
        }

        public UserInformation GetEmployeeDetails(string refSessionId, string employeeId)
        {
            UserInformation employee = new UserInformation();
            WebRequest tRequest =
                WebRequest.Create(string.Concat(AppSettings.JdaServerUrl, "retail/data/retailwebapi/api/", AppSettings.JdaApiVersion, "/users/", employeeId));
            tRequest.Method = "GET";
            tRequest.Headers.Add($"REFSSessionID:{refSessionId}");
            tRequest.ContentType = "application/json";

            using (WebResponse response = tRequest.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    if (stream != null)
                        using (StreamReader tReader = new StreamReader(stream))
                        {
                            string sResponseFromServer = tReader.ReadToEnd();
                            employee = AssignValues(sResponseFromServer);
                        }
                }
            }

            return employee;
        }

        private UserInformation AssignValues(string responceFromServer)
        {
            dynamic values = JObject.Parse(responceFromServer);

            var userInformation = new UserInformation
            {
                addressLine1 = values.contactInfo.addressLine1 + " " + values.contactInfo.addressLine2,
                homePhone = values.contactInfo.homePhone,
                cellPhone = values.contactInfo.cellPhone,
                workPhone = values.contactInfo.workPhone,
                email = values.contactInfo.email,
                city = values.contactInfo.city,
                state = values.contactInfo.state,
                countryCode = values.contactInfo.countryCode,
                zipCode = values.contactInfo.zipCode,
            };

            return userInformation;
        }


        public List<EmployeeAccruals> GetEmployeeAccruals(int employeeId)
        {
            List<EmployeeAccruals> lstEmployeeAccruals = new List<EmployeeAccruals>();
            try
            {
                string refSessionId = GetRefSessionIdFromClientAdminUser();
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client
                        .GetAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/employeeAccruals?employeeId={employeeId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    foreach (var employeeAccruals in dynamicObject.employeeAccruals)
                    {
                        var employeeAccrual = new EmployeeAccruals
                        {
                            Id = employeeAccruals.id,
                            EmployeeId = employeeAccruals.employeeID,
                            TimeOffTypeId = employeeAccruals.timeOffTypeID,
                            AccruedAmount = employeeAccruals.accruedAmount,
                            CarryOverAmount = employeeAccruals.carryOverAmount,
                            ServiceLevelCarryOverAmount = employeeAccruals.serviceLevelCarryOverAmount,
                            UsedAmount = employeeAccruals.usedAmount,
                            ApprovedAmount = employeeAccruals.approvedAmount,
                            RequestedAmount = employeeAccruals.requestedAmount,
                            AvailableAmount = employeeAccruals.availableAmount,
                        };

                        lstEmployeeAccruals.Add(employeeAccrual);
                    }
                }
            }
            catch (Exception ex)
            {
                // ignored
                return lstEmployeeAccruals;
            }


            return lstEmployeeAccruals;
        }

        public List<TimeOffType> GetAdjustmentTypes(int siteId, string refSessionId)
        {
            List<TimeOffType> lsttimeOffTypess = new List<TimeOffType>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/services/TimeOffRequest.asmx/"));
                    var response = client
                        .GetAsync($"GetAdjustmentTypes?siteId={siteId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    foreach (var timeOffTypes in dynamicObject.data)
                    {
                        var timeOffType = new TimeOffType
                        {
                            timeOffTypeID = timeOffTypes.Value,
                            timeOffType = timeOffTypes.Name
                        };

                        lsttimeOffTypess.Add(timeOffType);
                    }
                }
            }
            catch (Exception ex)
            {
                // ignored
                return lsttimeOffTypess;
            }

            return lsttimeOffTypess;
        }

        public MyTimeCard GetMyTimeCard(string refSessionId, int siteId, DateTime date)
        {
            MyTimeCard myTimeCard = new MyTimeCard();
            List<PayPunchedShift> lstPayPunchedShift = new List<PayPunchedShift>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(string.Concat(AppSettings.JdaServerUrl, "retail/data/ess/"));
                    var response = client
                        .GetAsync($"api/MyTimecard/{date.ToString("yyyy-MM-dd")}?siteId={siteId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    myTimeCard.NetPunchedHours =
                        dynamicObject.NetPunchedHours == null ? 0 : dynamicObject.NetPunchedHours;
                    myTimeCard.NetPayAdjustmentHours = dynamicObject.NetPayAdjustmentHours == null
                        ? 0
                        : dynamicObject.NetPayAdjustmentHours;

                    foreach (var days in dynamicObject.Days)
                    {
                        foreach (var payPunchedShifts in days.PayPunchedShifts)
                        {
                            List<Details> lstDetails = new List<Details>();
                            foreach (var details in payPunchedShifts.Details)
                            {
                                var detail = new Details
                                {
                                    ID = details.ID,
                                    DetailType = details.DetailType,
                                    Start = details.Start,
                                    End = details.End
                                };
                                lstDetails.Add(detail);
                            }

                            var PayPunchedShift = new PayPunchedShift
                            {
                                PunchedShiftID = payPunchedShifts.PunchedShiftID,
                                shiftDate = days.ID,
                                Start = payPunchedShifts.Start,
                                End = payPunchedShifts.End,
                                NetPunchedHours = payPunchedShifts.NetPunchedHours == null
                                    ? 0
                                    : payPunchedShifts.NetPunchedHours,
                                details = lstDetails
                            };
                            lstPayPunchedShift.Add(PayPunchedShift);
                        }
                    }

                    myTimeCard.payPunchedShifts = lstPayPunchedShift;
                }
            }
            catch (Exception ex)
            {
                return myTimeCard;
            }

            return myTimeCard;
        }

        public decimal GetScheduledHours(string refSessionId, int siteId, DateTime date)
        {
            decimal NetScheduledHours = 0;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client
                        .GetAsync($"retail/data/ess/api/MySchedule/{date.ToString("yyyy-MM-dd")}?siteId={siteId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    NetScheduledHours = dynamicObject.NetScheduledHours == null ? 0 : dynamicObject.NetScheduledHours;
                }
            }
            catch (Exception ex)
            {
                return NetScheduledHours;
            }

            return NetScheduledHours;
        }



        public string ClaimAvailableShift(ClaimShift claimShift)
        {
            string result = "";
            try
            {

                string refSessionId = GetRefSessionIdFromClientAdminUser();
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response1 = client
                        .PutAsJsonAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/scheduledShifts/{claimShift.scheduledShiftId}/availableShift",
                            claimShift).Result;
                    if (response1.StatusCode == HttpStatusCode.OK)
                    {
                        result = "Success";
                    }
                    else result = response1.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

            return result;
        }

        public List<ShiftSwap> ShiftSwapAvailableShift(string refSessionId, int shiftID, int siteId)
        {
            List<ShiftSwap> lstShiftSwap = new List<ShiftSwap>();
            try
            {
                var data = new
                {
                    myShiftID = shiftID
                };
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client.PostAsJsonAsync("retail/data/ess/services/SwapShift.asmx/GetAvailableShifts?siteId=" + siteId, data).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var o in dynamicObject.data)
                    {
                        var ShiftSwap = new ShiftSwap
                        {
                            ShiftDate = o.ShiftStart.ToString("yyyy-MM-dd"),
                            ShiftEnd = o.ShiftEnd.ToString("HH:mm"),
                            FixedShift = o.FixedShift,
                            NetHoursDelta = o.NetHoursDelta,
                            JobNames = o.JobNames,
                            ShiftID = o.ShiftID,
                            EmployeeName = o.EmployeeName,
                            ShiftStart = o.ShiftStart.ToString("HH:mm"),
                            JobColor = o.JobColor
                        };
                        lstShiftSwap.Add(ShiftSwap);
                    }
                }
            }
            catch (Exception ex)
            {
                // ignored
            }

            return lstShiftSwap;
        }

        public List<MyNotification> GetMyNotification(string refSessionId, int siteId)
        {

            List<MyNotification> lstMyNotification = new List<MyNotification>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                var data = new
                {
                    numberOfMessages = 100
                };
                var response = client.PostAsJsonAsync("retail/data/ess/services/MailInbox.asmx/GetTopMail?siteId=" + siteId, data).Result;

                var result = response.Content.ReadAsStringAsync().Result;

                dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                foreach (var o in dynamicObject.data)
                {
                    var message = new Message
                    {
                        Parameters = o.Message.Parameters,
                        Text = o.Message.Text
                    };

                    var myNotification = new MyNotification
                    {
                        Message = message,
                        Date = o.Date,
                        FromFirstName = o.FromFirstName,
                        FromLastName = o.FromLastName,
                        ID = o.ID,
                        IsRead = o.IsRead,
                        IsSystemAlert = o.IsSystemAlert,
                        Priority = o.Priority
                    };
                    lstMyNotification.Add(myNotification);
                }
            }

            return lstMyNotification;
        }

        public string CreateTimeOffRequest(TimeOffRequest timeOff)
        {
            string result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", timeOff.refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);

                    List<AddTimeOffRequest> lstAddTimeOffRequest = new List<AddTimeOffRequest>();
                    var process = new AddTimeOffRequest
                    {
                        StartDate = timeOff.start,
                        EndDate = timeOff.end,
                        HalfDay = timeOff.HalfDay,
                        TimeOffTypeID = timeOff.timeOffTypeID,
                        __TYPE = "ESS.Commands.Scheduling.AddTimeOffRequest",
                    };
                    lstAddTimeOffRequest.Add(process);

                    var data = new
                    {
                        jsChangeCommands = lstAddTimeOffRequest
                    };
                    HttpResponseMessage response =
                        client.PostAsJsonAsync("retail/data/ess/services/Commands.asmx/ProcessCommands?siteId=" + timeOff.siteId, data).Result;
                    var result1 = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var error in dynamicObject.data.Errors)
                    {
                        result = error.Type;
                    }

                    if (string.IsNullOrEmpty(result)) // (response.StatusCode == HttpStatusCode.OK)
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }


        public string UpdateTimeOffRequest(TimeOffRequest timeOff)
        {
            string result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", timeOff.refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);

                    List<UpdateTimeOffRequest> lstUpdateTimeOffRequest = new List<UpdateTimeOffRequest>();
                    string modifiedDateTime = string.Concat(DateTime.Now.ToString("yyyy-MM-dd"), "T", DateTime.Now.ToString("hh:mm:ss"));

                    var process = new UpdateTimeOffRequest
                    {
                        StartDate = timeOff.start,
                        EndDate = timeOff.end,
                        HalfDay = timeOff.HalfDay,
                        TimeOffTypeID = Convert.ToString(timeOff.timeOffTypeID),
                        __TYPE = "ESS.Commands.Scheduling.UpdateTimeOffRequest",
                        TorID = timeOff.timeOffRequestID,
                        LastModifiedTimestamp = modifiedDateTime,
                        Comment = timeOff.comments
                    };
                    lstUpdateTimeOffRequest.Add(process);

                    var data = new
                    {
                        jsChangeCommands = lstUpdateTimeOffRequest
                    };
                    HttpResponseMessage response =
                        client.PostAsJsonAsync("retail/data/ess/services/Commands.asmx/ProcessCommands?siteId=" + timeOff.siteId, data).Result;
                    var result1 = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var error in dynamicObject.data.Errors)
                    {
                        result = error.Type;
                    }

                    if (string.IsNullOrEmpty(result)) // (response.StatusCode == HttpStatusCode.OK)
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        public string CancelTimeOffRequest(TimeOffRequest timeOff)
        {
            string result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", timeOff.refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);

                    List<CancelTimeOffRequest> lstUpdateTimeOffRequest = new List<CancelTimeOffRequest>();
                    var process = new CancelTimeOffRequest
                    {
                        __TYPE = "ESS.Commands.Scheduling.CancelTimeOffRequest",
                        TorID = timeOff.timeOffRequestID,
                        StartDate = timeOff.start,
                        EndDate = timeOff.end,
                        LastModifiedTimestamp = timeOff.LastModifiedTimestamp,
                        TimeOffTypeID = timeOff.timeOffTypeID,
                    };
                    lstUpdateTimeOffRequest.Add(process);

                    var data = new
                    {
                        jsChangeCommands = lstUpdateTimeOffRequest
                    };
                    HttpResponseMessage response =
                        client.PostAsJsonAsync("retail/data/ess/services/Commands.asmx/ProcessCommands?siteId=" + timeOff.siteId, data).Result;
                    var result1 = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var error in dynamicObject.data.Errors)
                    {
                        result = error.Type;
                    }

                    if (string.IsNullOrEmpty(result))
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        public string DeleteMyNotification(DeleteNotificationParam deleteNotificationParam)
        {
            string result = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", deleteNotificationParam.refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);

                    List<DeleteNotification> lstDeleteNotification = new List<DeleteNotification>();
                    foreach (var mailid in deleteNotificationParam.MailId)
                    {
                        var process = new DeleteNotification
                        {
                            __TYPE = "ESS.Commands.Mail.DeleteMail",
                            MailID = mailid
                        };
                        lstDeleteNotification.Add(process);
                    }

                    var data = new
                    {
                        jsChangeCommands = lstDeleteNotification
                    };
                    HttpResponseMessage response = client
                        .PostAsJsonAsync("retail/data/ess/services/Commands.asmx/ProcessCommands?siteId=" + deleteNotificationParam.siteId, data).Result;
                    var result1 = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var error in dynamicObject.data.Errors)
                    {
                        result = error.Type;
                    }

                    if (string.IsNullOrEmpty(result))
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        public string ProcessShiftSwap(ProcessShiftSwap processShiftSwap)
        {
            string result = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", processShiftSwap.refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);

                    List<ProcessCollection> lstProcessCollection = new List<ProcessCollection>();
                    var process = new ProcessCollection
                    {
                        __TYPE = "ESS.Commands.SwapShift.SubmitSwapShiftRequests",
                        RequestedScheduledShiftIDs = processShiftSwap.RequestedScheduledShiftIDs,
                        OriginalScheduledShiftID = processShiftSwap.OriginalScheduledShiftID
                    };
                    lstProcessCollection.Add(process);
                    var data = new
                    {
                        jsChangeCommands = lstProcessCollection
                    };
                    HttpResponseMessage response = client
                        .PostAsJsonAsync($"retail/data/ess/services/Commands.asmx/ProcessCommands?siteId={processShiftSwap.siteId}", data).Result;

                    var result1 = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var error in dynamicObject.data.Errors)
                    {
                        result = error.Type;
                    }

                    if (string.IsNullOrEmpty(result))
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;

        }

        public string UpdateSwapShiftRequest(UpdateShiftSwap updateShiftSwap)
        {
            string result = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", updateShiftSwap.refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    string requestStatus = string.Empty;
                    switch (updateShiftSwap.ActionFlag)
                    {
                        case "A":
                            requestStatus = "AwaitingManager";
                            break;
                        case "D":
                            requestStatus = "RecipientDenied";
                            break;
                        case "C":
                            requestStatus = "SenderCanceled";
                            break;
                    }

                    List<ProcessShiftSwapRequest> lstProcessCollection = new List<ProcessShiftSwapRequest>();
                    var process = new ProcessShiftSwapRequest
                    {
                        __TYPE = "ESS.Commands.SwapShift.UpdateSwapShiftRequest",
                        SwapShiftID = updateShiftSwap.SwapShiftID,
                        RequestedScheduledShiftID = updateShiftSwap.RequestedScheduledShiftID,
                        RequestStatus = requestStatus
                    };
                    lstProcessCollection.Add(process);
                    var data = new
                    {
                        jsChangeCommands = lstProcessCollection
                    };

                    HttpResponseMessage response = client
                        .PostAsJsonAsync("retail/data/ess/services/Commands.asmx/ProcessCommands?siteId=" + updateShiftSwap.siteId, data).Result;

                    var result1 = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);
                    foreach (var error in dynamicObject.data.Errors)
                    {
                        result = error.Type;
                    }

                    if (string.IsNullOrEmpty(result))
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        public string CreateEmployeeAvailability(EmployeeAvailability availability)
        {
            string responceString = string.Empty;
            try
            {
                string refSessionId = GetRefSessionIdFromClientAdminUser();
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client.PostAsJsonAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/employeeAvailability", availability).Result;
                    var result = response.Content.ReadAsStringAsync().Result;


                    IEnumerable<string> values;
                    string session = string.Empty;
                    if (response.Headers.TryGetValues("REFS-Message", out values))
                    {
                        responceString = values.FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error";
            }
            return responceString;
        }

        public string UpdateEmployeeAvailabilityRequest(EmployeeAvailabilityPrivateApi availability)
        {
            string result = string.Empty;
            var data = new EmployeeAvailabilityPrivateApiPrams
            {
                ID = availability.ID,
                Comment = availability.Comment,
                Start = availability.Start,
                End = availability.End,
                LastStatusUpdateTimestamp = availability.LastStatusUpdateTimestamp,
                LastStatusUpdateUserID = availability.LastStatusUpdateUserID,
                LastStatusUpdateUserName = availability.LastStatusUpdateUserName,
                Rank = availability.Rank,
                RequestedTimestamp = availability.RequestedTimestamp,
                Status = availability.Status,
                availabilityRequestRange = availability.availabilityRequestRange,
            };
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", availability.RefSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .PutAsJsonAsync($"retail/data/ess/api/myavailabilityrequest/{availability.ID}?siteId={availability.SiteId}",
                            data).Result;
                    var result1 = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);

                    try
                    {
                        IEnumerable<string> values;
                        string session = string.Empty;
                        if (response.Headers.TryGetValues("REFS-Message", out values))
                        {
                            result = values.FirstOrDefault();
                            if (result.ToUpper() != "OK")
                            {
                                int start = result.IndexOf(">");
                                int end = result.IndexOf(")");
                                string json = result.Substring(start + 1).Replace(')', ' ');
                                dynamic resultObject = JsonConvert.DeserializeObject<dynamic>(json);
                                result = resultObject.ErrorCode;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        result = e.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public string CreateEmployeeAvailabilityUsingPrivateAPI(EmployeeAvailabilityPrivateApi availability)
        {
            string result = string.Empty;
            var data = new EmployeeAvailabilityPrivateApiPrams
            {
                ID = availability.ID,
                Comment = availability.Comment,
                Start = availability.Start,
                End = availability.End,
                LastStatusUpdateTimestamp = availability.LastStatusUpdateTimestamp,
                LastStatusUpdateUserID = availability.LastStatusUpdateUserID,
                LastStatusUpdateUserName = availability.LastStatusUpdateUserName,
                Rank = availability.Rank,
                RequestedTimestamp = availability.RequestedTimestamp,
                Status = availability.Status,
                availabilityRequestRange = availability.availabilityRequestRange,
            };
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", availability.RefSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .PostAsJsonAsync("retail/data/ess/api/myavailabilityrequest/0?siteId=" + availability.SiteId,
                            data).Result;
                    var result1 = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);

                    try
                    {
                        IEnumerable<string> values;
                        string session = string.Empty;
                        if (response.Headers.TryGetValues("REFS-Message", out values))
                        {
                            result = values.FirstOrDefault();
                            if (result != "Created")
                            {
                                int start = result.IndexOf(">");
                                int end = result.IndexOf(")");
                                string json = result.Substring(start + 1).Replace(')', ' ');
                                dynamic resultObject = JsonConvert.DeserializeObject<dynamic>(json);
                                result = resultObject.ErrorCode;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        result = e.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public EmployeeAvailability GetMyCurrentAvailability(int Employeeid, DateTime date)
        {
            List<EmployeeAvailability> lstEmployeeAvailabilities = new List<EmployeeAvailability>();
            try
            {

                string refSessionId = GetRefSessionIdFromClientAdminUser();
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client
                        .GetAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/employeeAvailability?employeeId={Employeeid}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var employeeAvailability in dynamicObject.employeeAvailability)
                    {
                        List<GeneralAvailability> lstGeneralAvailabilities = new List<GeneralAvailability>();
                        List<PreferredAvailability> lstPreferredAvailabilities = new List<PreferredAvailability>();
                        foreach (var generalAvailability in employeeAvailability.generalAvailability)
                        {
                            var GeneralAvailability = new GeneralAvailability
                            {
                                id = generalAvailability.id,
                                dayOfWeek = generalAvailability.dayOfWeek,
                                endTimeOffset = generalAvailability.endTimeOffset,
                                startTimeOffset = generalAvailability.startTimeOffset,
                                weekNumber = generalAvailability.weekNumber
                            };

                            lstGeneralAvailabilities.Add(GeneralAvailability);
                        }

                        foreach (var preferredAvailability in employeeAvailability.preferredAvailability)
                        {
                            var PreferredAvailability = new PreferredAvailability
                            {
                                id = preferredAvailability.id,
                                dayOfWeek = preferredAvailability.dayOfWeek,
                                endTimeOffset = preferredAvailability.endTimeOffset,
                                startTimeOffset = preferredAvailability.startTimeOffset,
                                weekNumber = preferredAvailability.weekNumber
                            };
                            lstPreferredAvailabilities.Add(PreferredAvailability);
                        }

                        var EmployeeAvailability = new EmployeeAvailability
                        {
                            id = employeeAvailability.id,
                            cycleBaseDate = employeeAvailability.cycleBaseDate,
                            effectiveFrom = employeeAvailability.effectiveFrom,
                            employeeID = employeeAvailability.employeeID,
                            endsAfter = employeeAvailability.endsAfter,
                            numberOfWeeks = employeeAvailability.numberOfWeeks,
                            GeneralAvailability = lstGeneralAvailabilities,
                            PreferredAvailability = lstPreferredAvailabilities
                        };

                        if (EmployeeAvailability.effectiveFrom <= date && EmployeeAvailability.endsAfter >= date)
                        {
                            lstEmployeeAvailabilities.Add(EmployeeAvailability);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return lstEmployeeAvailabilities.Select(x => x)
                    .FirstOrDefault(); //.Select(x => x).OrderBy(x => x.effectiveFrom).FirstOrDefault();
            }

            return lstEmployeeAvailabilities.Select(x => x).FirstOrDefault();
        }


        public string UpdateEmployeeInformation(EmployeeInformation e)
        {
            string result = string.Empty;
            var data = new EmployeeInformationParam
            {
                ID = e.ID,
                LastName = e.LastName,
                FirstName = e.FirstName,
                MiddleName = e.MiddleName,
                NickName = e.NickName,
                Suffix = e.Suffix,
                BadgeNumber = e.BadgeNumber,
                HomePhone = e.HomePhone,
                WorkPhone = e.WorkPhone,
                CellPhone = e.CellPhone,
                Email = e.Email,
                SSN = e.SSN,
                BirthDate = e.BirthDate,
                EmergencyContactName = e.EmergencyContactName,
                EmergencyContactPhone = e.EmergencyContactPhone,
                EmergencyContactRelationship = e.EmergencyContactRelationship,
                Uri = "employeeinformation/" + e.ID.ToString(),
                Address = e.Address,
                Notifications = e.Notifications,
            };

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", e.refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .PutAsJsonAsync($"retail/data/ess/api/EmployeeInformation/{e.ID}?siteId=" + e.siteId, data)
                        .Result;
                    var result1 = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result1);

                    try
                    {
                        foreach (var error in dynamicObject.data.Errors)
                        {
                            result = error.Type;
                        }
                    }
                    catch (Exception ex)
                    {
                        IEnumerable<string> values;
                        string session = string.Empty;
                        if (response.Headers.TryGetValues("REFS-Message", out values))
                        {
                            result = values.FirstOrDefault();
                            if (result != "OK")
                            {
                                int start = result.IndexOf(">");
                                int end = result.IndexOf(")");
                                string json = result.Substring(start + 1).Replace(')', ' ');
                                dynamic resultObject = JsonConvert.DeserializeObject<dynamic>(json);
                                result = resultObject.ErrorCode;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public List<PendingMyApprovals> GetPendingApprovals(int siteId, string refSessionId)
        {
            List<PendingMyApprovals> lstPendingMyApprovals = new List<PendingMyApprovals>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .GetAsync($"retail/data/ess/services/SwapShift.asmx/GetPendingApprovals?siteId={siteId}")
                        .Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var data in dynamicObject.data)
                    {
                        var pendingApprovalData = new PendingMyApprovalData
                        {
                            BuUnit = "",
                            SwapId = data.SwapID,
                            Swapper = data.SwapperName,
                            Status = data.SwappeeStatusCode,
                            GainedShiftId = data.GainedShift.ShiftID,
                            GainedShiftShiftStart = data.GainedShift.ShiftStart,
                            GainedShiftShiftEnd = data.GainedShift.ShiftEnd,
                            LostShiftId = data.LostShift.ShiftID,
                            LostShiftShiftStart = data.LostShift.ShiftStart,
                            LostShiftShiftEnd = data.LostShift.ShiftEnd,
                            Role = data.GainedShift.JobNames
                        };


                        var pendingApproval = new PendingMyApprovals
                        {
                            BuUnit = pendingApprovalData.BuUnit,
                            GainedShift = string.Concat(pendingApprovalData.GainedShiftShiftStart.ToString("yyyy-MM-dd"), " ", pendingApprovalData.GainedShiftShiftStart.ToString("HH:mm"),
                                " - ", pendingApprovalData.GainedShiftShiftEnd.ToString("HH:mm")),
                            GainedShiftId = pendingApprovalData.GainedShiftId,
                            LostShift = string.Concat(pendingApprovalData.LostShiftShiftStart.ToString("yyyy-MM-dd"), " ", pendingApprovalData.LostShiftShiftStart.ToString("HH:mm")
                                , " - ", pendingApprovalData.LostShiftShiftEnd.ToString("HH:mm")),
                            LostShiftId = pendingApprovalData.LostShiftId,

                            Role = pendingApprovalData.Role,
                            SwapId = pendingApprovalData.SwapId,
                            Swapper = pendingApprovalData.Swapper,
                            Status = GetPendingApprovalStatus(pendingApprovalData.Status, pendingApprovalData.Swapper),
                            StatusCode = pendingApprovalData.Status
                        };

                        lstPendingMyApprovals.Add(pendingApproval);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return lstPendingMyApprovals;
        }

        public string GetPendingApprovalStatus(string statusCode, string swapper)
        {
            string status = string.Empty;

            switch (statusCode)
            {
                case "a":
                    status = string.Concat("Approved by", " : Manager", "  Schedules swapped");
                    break;
                case "c":
                    status = string.Concat("Request Canceled", " Requested by ", swapper);
                    break;
                case "r":
                    status = string.Concat("Requested by", " : ", swapper);
                    break;
                case "y":
                    status = string.Concat("Awaiting Approval by ", "Manager", " Requested By ", swapper);
                    break;
                case "x":
                    status = string.Concat("Denied By ", " Manager ", " Requested by ", swapper);
                    break;
                case "i":
                    status = string.Concat("Request Canceled   Schedule was modified", " Requested by ", swapper);
                    break;
            }
            return status;
        }


        public string GetRequestedbyMeStatus(string statusCode, string swapper)
        {
            string status = string.Empty;

            switch (statusCode)
            {
                case "a":
                    status = string.Concat("Approved by", " : Manager", "  Schedules swapped");
                    break;
                case "c":
                    status = string.Concat("Request Canceled");
                    break;
                case "r":
                    status = string.Concat("Awaiting Approval by ", " : ", swapper);
                    break;
                case "y":
                    status = string.Concat("Awaiting Approval by ", "Manager", " Approved By ", swapper);
                    break;
                case "x":
                    status = string.Concat("Denied By ", " Manager");
                    break;
                case "i":
                    status = string.Concat("Request Canceled   Schedule was modified");
                    break;
                case "n":
                    status = string.Concat("Denied By ", swapper);
                    break;
            }
            return status;
        }

        public List<RequestedByMe> RequestedByMe(int siteId, string refSessionId)
        {
            List<RequestedByMe> lstPendingMyApprovals = new List<RequestedByMe>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .GetAsync($"retail/data/ess/services/SwapShift.asmx/GetRequestedByMe?siteId={siteId}")
                        .Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var data in dynamicObject.data)
                    {
                        var pendingApprovalData = new PendingMyApprovalData
                        {
                            BuUnit = "",
                            SwapId = data.SwapID,
                            Swapper = data.SwappeeName,
                            Status = data.RequestStatus,
                            GainedShiftId = data.GainedShift.ShiftID,
                            GainedShiftShiftStart = data.GainedShift.ShiftStart,
                            GainedShiftShiftEnd = data.GainedShift.ShiftEnd,
                            LostShiftId = data.LostShift.ShiftID,
                            LostShiftShiftStart = data.LostShift.ShiftStart,
                            LostShiftShiftEnd = data.LostShift.ShiftEnd,
                            Role = data.GainedShift.JobNames
                        };

                        var pendingApproval = new RequestedByMe
                        {
                            BuUnit = pendingApprovalData.BuUnit,
                            GainedShift = string.Concat(pendingApprovalData.GainedShiftShiftStart.ToString("yyyy-MM-dd"), " ", pendingApprovalData.GainedShiftShiftStart.ToString("HH:mm"),
                                " - ", pendingApprovalData.GainedShiftShiftEnd.ToString("HH:mm")),
                            GainedShiftId = pendingApprovalData.GainedShiftId,
                            LostShift = string.Concat(pendingApprovalData.LostShiftShiftStart.ToString("yyyy-MM-dd"), " ", pendingApprovalData.LostShiftShiftStart.ToString("HH:mm")
                                , " - ", pendingApprovalData.LostShiftShiftEnd.ToString("HH:mm")),
                            LostShiftId = pendingApprovalData.LostShiftId,
                            Role = pendingApprovalData.Role,
                            SwapId = pendingApprovalData.SwapId,
                            Swapper = pendingApprovalData.Swapper,
                            Status = GetRequestedbyMeStatus(pendingApprovalData.Status, pendingApprovalData.Swapper),
                            StatusCode = pendingApprovalData.Status
                        };

                        lstPendingMyApprovals.Add(pendingApproval);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return lstPendingMyApprovals;
        }

        public List<State> GetStates(int siteId, int countryId, string refSessionId)
        {
            List<State> lstStates = new List<State>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .GetAsync($"retail/data/ess/api/State?countryID={countryId}&siteId={siteId}")
                        .Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var data in dynamicObject)
                    {
                        var states = new State
                        {
                            StateId = data.ID,
                            StateCode = data.Code,
                            StateName = data.Name
                        };
                        lstStates.Add(states);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return lstStates;
        }


        public TimeOffRequestValidation GetRTOandTorPeriod(int siteId, string refSessionId)
        {
            TimeOffRequestValidation timeOffRequestValidation = new TimeOffRequestValidation();
            try
            {
                //http://retail-vm01/retail/data/ess/services/TnA.asmx/GetClientSettings?siteId=1000108 
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .GetAsync($"retail/data/ess/services/TnA.asmx/GetClientSettings?siteId={siteId}")
                        .Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var data in dynamicObject.data)
                    {
                        timeOffRequestValidation = new TimeOffRequestValidation
                        {
                            min_days_notice_RTO = data.MinDaysNoticeForTimeOffRequests,
                            tor_max_days = data.MaxDaysForTimeOffRequest,
                            MinDaysNoticeForTimeOffRequests = data.MinDaysAvailableEmployeeThreshold,
                            MinDaysAvailableEmployeeThreshold = data.MinDaysAvailableEmployeeThreshold
                        };
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return timeOffRequestValidation;
        }

        private string GetAvailabilityCode(int Type)
        {
            string str = string.Empty;

            switch (Type)
            {
                case 0:
                    str = "All Day";
                    break;
                case 1:
                    str = "Unavailable";
                    break;
                //case 2:
                //    break;
                case 3:
                    str = "No Preferred";
                    break;
            }
            return str;
        }




        public EmployeeNewAvailability GetMyAvailability(int siteId, string refSessionId)
        {

            List<GeneralPrefferdAvailability> lstGeneralPrefferdAvailability = new List<GeneralPrefferdAvailability>();
            EmployeeNewAvailability _objEmployeeAvailability = new EmployeeNewAvailability();
            //string refSessionId = "";
            //int siteId = 1000108;
            // dynamic strJson = null;
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .GetAsync($"retail/data/ess/services/Employee.asmx/GetMyAvailability?siteId={siteId}")
                        .Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);


                    //    strJson = dynamicObject.data;
                    foreach (var DayRows in dynamicObject.data.DayRows)
                    {
                        List<AvailabilityDetails> lstAvailabilityDetails = new List<AvailabilityDetails>();
                        int dayofWeek = DayRows.DayOfWeek;
                        foreach (var WeekColumns in DayRows.WeekColumns)
                        {
                            // List<TimeSpans> lstTimeSpan = new List<TimeSpans>();
                            TimeSpans objTimeSpans = new TimeSpans();
                            foreach (var TimeSpans in WeekColumns.TimeSpans)
                            {
                                objTimeSpans = new TimeSpans
                                {
                                    startTimeOffset = TimeSpans.Start,
                                    endTimeOffset = TimeSpans.End
                                };
                                //lstTimeSpan.Add(timeSpans);
                            }
                            int AvailabilityRangeType = WeekColumns.AvailabilityRangeType;
                            var AvailabilityDetails = new AvailabilityDetails
                            {
                                // TimeSpans = lstTimeSpan,
                                AvailabilityRangeType = AvailabilityRangeType,
                                AvailabilityRangeTypeDesc = GetAvailabilityCode(AvailabilityRangeType),
                                AvailabilityType = WeekColumns.AvailabilityType,
                                weekNumber = 1,
                                endTimeOffset = objTimeSpans.endTimeOffset,
                                startTimeOffset = objTimeSpans.startTimeOffset

                            };
                            lstAvailabilityDetails.Add(AvailabilityDetails);
                        }

                        var generalPrefferdAvailability = new GeneralPrefferdAvailability
                        {
                            dayOfWeek = GetDayOfWeek(dayofWeek),
                            AvailabilityDetails = lstAvailabilityDetails
                        };

                        lstGeneralPrefferdAvailability.Add(generalPrefferdAvailability);
                    }
                    _objEmployeeAvailability = new EmployeeNewAvailability
                    {
                        effectiveFrom = dynamicObject.data.DateRange.Start,
                        endsAfter = dynamicObject.data.DateRange.End,
                        Availability = lstGeneralPrefferdAvailability
                    };
                }

            }
            catch //()
            {
                return _objEmployeeAvailability;
            }

            return _objEmployeeAvailability;

            //    string refSessionId = GetRefSessionIdFromClientAdminUser();
            //    using (var client = new HttpClient())
            //    {
            //        client.DefaultRequestHeaders.Accept.Clear();
            //        client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
            //        client.DefaultRequestHeaders.Accept.Add(
            //            new MediaTypeWithQualityHeaderValue("application/json"));
            //        client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
            //        var response = client
            //            .GetAsync($"retail/data/retailwebapi/api/v1-beta5/employeeAvailability?employeeId={Employeeid}").Result;
            //        var result = response.Content.ReadAsStringAsync().Result;

            //        dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
            //        foreach (var employeeAvailability in dynamicObject.employeeAvailability)
            //        {
            //            var _EmployeeAvailability = new EmployeeNewAvailability
            //            {
            //                effectiveFrom = employeeAvailability.effectiveFrom,
            //                endsAfter = employeeAvailability.endsAfter,
            //                //  PreferredAvailability = lstPreferredAvailabilities
            //            };
            //            if (_EmployeeAvailability.effectiveFrom <= date && _EmployeeAvailability.endsAfter >= date)
            //            {
            //                List<GeneralPrefferdAvailability> lstTempPrefferAvailabilities =
            //                    new List<GeneralPrefferdAvailability>();
            //                List<GeneralPrefferdAvailability> lstGeneralAvailabilities =
            //                    new List<GeneralPrefferdAvailability>();

            //                string lastDayOfWeek = string.Empty;
            //                string CurrentDayofWeek = string.Empty;
            //                int CurrentDay = 0;
            //                int lastDay = 0;
            //                foreach (var generalAvailability in employeeAvailability.generalAvailability)
            //                {
            //                    lastDayOfWeek = lstGeneralAvailabilities.Count > 0
            //                        ? lstGeneralAvailabilities[lstGeneralAvailabilities.Count - 1].dayOfWeek
            //                        : "";
            //                    CurrentDayofWeek = generalAvailability.dayOfWeek;
            //                    CurrentDay = GetDayofWeekNumber(CurrentDayofWeek);

            //                    if (GetDayOfWeekInSequence(CurrentDayofWeek, lastDayOfWeek))
            //                    {
            //                        var GeneralAvailability = new GeneralPrefferdAvailability
            //                        {
            //                            id = generalAvailability.id,
            //                            dayOfWeek = generalAvailability.dayOfWeek,
            //                            endTimeOffset = generalAvailability.endTimeOffset,
            //                            startTimeOffset = generalAvailability.startTimeOffset,
            //                            weekNumber = generalAvailability.weekNumber,
            //                            AvailabilityType = "g"
            //                        };
            //                        lstGeneralAvailabilities.Add(GeneralAvailability);
            //                    }
            //                    else
            //                    {

            //                        int dayNum = lastDayOfWeek == ""
            //                            ? GetDayofWeekNumber("Sunday")
            //                            : GetDayofWeekNumber(lastDayOfWeek) + 1;

            //                        lastDay = lastDayOfWeek == "" ? 0 : GetDayofWeekNumber(lastDayOfWeek);
            //                        for (int i = lastDay + 1; i < CurrentDay; i++)
            //                        {
            //                            var GeneralAvailability = new GeneralPrefferdAvailability
            //                            {
            //                                id = 0, //generalAvailability.id,
            //                                dayOfWeek = GetDayOfWeek(i),
            //                                endTimeOffset = "Unavailable",
            //                                //generalAvailability.endTimeOffset,
            //                                startTimeOffset = "Unavailable",
            //                                //generalAvailability.startTimeOffset,
            //                                weekNumber = 0, // generalAvailability.weekNumber,
            //                                AvailabilityType = "g"
            //                            };
            //                            lstGeneralAvailabilities.Add(GeneralAvailability);
            //                        }

            //                        if (CurrentDay > dayNum)
            //                        {
            //                            var GeneralAvailability = new GeneralPrefferdAvailability
            //                            {
            //                                id = generalAvailability.id,
            //                                dayOfWeek = generalAvailability.dayOfWeek,
            //                                endTimeOffset = generalAvailability.endTimeOffset,
            //                                startTimeOffset = generalAvailability.startTimeOffset,
            //                                weekNumber = generalAvailability.weekNumber,
            //                                AvailabilityType = "g"
            //                            };
            //                            lstGeneralAvailabilities.Add(GeneralAvailability);
            //                        }

            //                        CurrentDay = GetDayofWeekNumber(CurrentDayofWeek);
            //                    }
            //                }


            //                if (CurrentDay < 6 )
            //                {
            //                    for (int i = CurrentDay + 1; i <= 6; i++)
            //                    {
            //                        var GeneralAvailability = new GeneralPrefferdAvailability
            //                        {
            //                            id = 0, //generalAvailability.id,
            //                            dayOfWeek = GetDayOfWeek(i),
            //                            endTimeOffset = "Unavailable",
            //                            //generalAvailability.endTimeOffset,
            //                            startTimeOffset = "Unavailable",
            //                            //generalAvailability.startTimeOffset,
            //                            weekNumber = 0, // generalAvailability.weekNumber,
            //                            AvailabilityType = "g"
            //                        };
            //                        lstGeneralAvailabilities.Add(GeneralAvailability);
            //                    }
            //                }

            //                CurrentDay = 0;
            //                foreach (var preferredAvailability in employeeAvailability.preferredAvailability)
            //                {

            //                    lastDayOfWeek = lstTempPrefferAvailabilities.Count > 0
            //                        ? lstTempPrefferAvailabilities[lstTempPrefferAvailabilities.Count - 1].dayOfWeek
            //                        : "";
            //                    CurrentDayofWeek = preferredAvailability.dayOfWeek;
            //                    CurrentDay = GetDayofWeekNumber(CurrentDayofWeek);
            //                    if (GetDayOfWeekInSequence(CurrentDayofWeek, lastDayOfWeek))
            //                    {
            //                        var PreferredAvailability = new GeneralPrefferdAvailability
            //                        {
            //                            id = preferredAvailability.id,
            //                            dayOfWeek = preferredAvailability.dayOfWeek,
            //                            endTimeOffset = preferredAvailability.endTimeOffset,
            //                            startTimeOffset = preferredAvailability.startTimeOffset,
            //                            weekNumber = preferredAvailability.weekNumber,
            //                            AvailabilityType = "p"

            //                        };
            //                        lstTempPrefferAvailabilities.Add(PreferredAvailability);
            //                        lstGeneralAvailabilities.Add(PreferredAvailability);
            //                    }
            //                    else
            //                    {
            //                        int dayNum = lastDayOfWeek == ""
            //                            ? GetDayofWeekNumber("Sunday")
            //                            : GetDayofWeekNumber(lastDayOfWeek) + 1;

            //                        lastDay = lastDayOfWeek == "" ? 0 : GetDayofWeekNumber(lastDayOfWeek);

            //                        for (int i = lastDay + 1; i < CurrentDay; i++)
            //                        {
            //                            var PreferredAvailability = new GeneralPrefferdAvailability
            //                            {
            //                                id = 0,
            //                                dayOfWeek = GetDayOfWeek(i),
            //                                endTimeOffset = "Unavailable", //preferredAvailability.endTimeOffset,
            //                                startTimeOffset = "Unavailable", //preferredAvailability.startTimeOffset,
            //                                weekNumber = 0,
            //                                AvailabilityType = "p"

            //                            };
            //                            lstTempPrefferAvailabilities.Add(PreferredAvailability);
            //                            lstGeneralAvailabilities.Add(PreferredAvailability);
            //                        }

            //                        if (CurrentDay > dayNum)
            //                        {
            //                            var PreferredAvailability1 = new GeneralPrefferdAvailability
            //                            {
            //                                id = preferredAvailability.id,
            //                                dayOfWeek = preferredAvailability.dayOfWeek,
            //                                endTimeOffset = preferredAvailability.endTimeOffset,
            //                                startTimeOffset = preferredAvailability.startTimeOffset,
            //                                weekNumber = preferredAvailability.weekNumber,
            //                                AvailabilityType = "p"

            //                            };
            //                            lstTempPrefferAvailabilities.Add(PreferredAvailability1);
            //                            lstGeneralAvailabilities.Add(PreferredAvailability1);

            //                        }
            //                    }
            //                }

            //                if (CurrentDay < 6)
            //                {
            //                    for (int i = CurrentDay + 1; i <= 6; i++)
            //                    {
            //                        var PreferredAvailability = new GeneralPrefferdAvailability
            //                        {
            //                            id = 0,
            //                            dayOfWeek = GetDayOfWeek(i),
            //                            endTimeOffset = "Unavailable", //preferredAvailability.endTimeOffset,
            //                            startTimeOffset = "Unavailable", //preferredAvailability.startTimeOffset,
            //                            weekNumber = 0,
            //                            AvailabilityType = "p"

            //                        };
            //                        lstGeneralAvailabilities.Add(PreferredAvailability);
            //                    }
            //                }

            //                CurrentDay = 0;

            //                var EmployeeAvailability = new EmployeeNewAvailability
            //                {
            //                    id = employeeAvailability.id,
            //                    cycleBaseDate = employeeAvailability.cycleBaseDate,
            //                    effectiveFrom = employeeAvailability.effectiveFrom,
            //                    employeeID = employeeAvailability.employeeID,
            //                    endsAfter = employeeAvailability.endsAfter,
            //                    numberOfWeeks = employeeAvailability.numberOfWeeks,
            //                    Availability = lstGeneralAvailabilities,
            //                    //  PreferredAvailability = lstPreferredAvailabilities
            //                };


            //                objEmployeeAvailability = EmployeeAvailability;
            //                break;
            //                //  lstEmployeeAvailabilities.Add(EmployeeAvailability);
            //            }
            //        }
            //    }
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //}

            // return objEmployeeAvailability;
        }

        private bool GetDayOfWeekInSequence(string dayOfWeek, string LastdayoffWeek)
        {
            //try
            //{
            string day = string.Empty;
            int CurrentDayofWeekNumber = GetDayofWeekNumber(dayOfWeek);
            if (LastdayoffWeek == string.Empty && CurrentDayofWeekNumber == 0)
            {
                return true;
            }
            else if (LastdayoffWeek == string.Empty && CurrentDayofWeekNumber > 0)
            {
                return false;
            }
            else
            {
                int lastDayOffWeekNumber = GetDayofWeekNumber(LastdayoffWeek);
                int result = CurrentDayofWeekNumber - (lastDayOffWeekNumber + 1);
                return result == 0 ? true : false;
            }
            //}
            //catch (Exception e)
            //{
            //    return true;
            //}
        }

        private int GetDayofWeekNumber(string dayOfWeek)
        {
            int dayNumber = 0;
            switch (dayOfWeek)
            {
                case "Sunday":
                    dayNumber = 0;
                    break;
                case "Monday":
                    dayNumber = 1;
                    break;
                case "Tuesday":
                    dayNumber = 2;
                    break;
                case "Wednesday":
                    dayNumber = 3;
                    break;
                case "Thursday":
                    dayNumber = 4;
                    break;
                case "Friday":
                    dayNumber = 5;
                    break;
                case "Saturday":
                    dayNumber = 6;
                    break;
            }

            return dayNumber;
        }

        private string GetDayOfWeek(int dayOfWeek)
        {

            string day = string.Empty;
            switch (dayOfWeek)
            {
                case 0:
                    day = DayOfWeek.Sunday.ToString();
                    break;
                case 1:
                    day = DayOfWeek.Monday.ToString();
                    break;
                case 2:
                    day = DayOfWeek.Tuesday.ToString();
                    break;
                case 3:
                    day = DayOfWeek.Wednesday.ToString();
                    break;
                case 4:
                    day = DayOfWeek.Thursday.ToString();
                    break;
                case 5:
                    day = DayOfWeek.Friday.ToString();
                    break;
                case 6:
                    day = DayOfWeek.Saturday.ToString();
                    break;
            }

            return day;
        }

        public List<MyUnpairedPunches> GetUnpairedPuncheses(string refSessionId, int siteId)
        {
            List<MyUnpairedPunches> lstMyUnpairedPuncheses = new List<MyUnpairedPunches>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .GetAsync($"retail/data/ess/api/myunpairedpunches/currentunpairedpunches?siteId={siteId}")
                        .Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    foreach (var data in dynamicObject)
                    {
                        string punchesCode = data.UnpairedPunchCodes;

                        var myUnpairedPunches = new MyUnpairedPunches
                        {
                            PunchedShiftID = data.PunchedShiftID,
                            Site = data.SiteName,
                            ShiftStart = data.Start,
                            Job = data.JobNames,
                            Reason = PunchesCodeReason(punchesCode)
                        };

                        lstMyUnpairedPuncheses.Add(myUnpairedPunches);
                    }
                }
            }
            catch //()
            {
                return lstMyUnpairedPuncheses;
            }
            return lstMyUnpairedPuncheses;
        }

        private string PunchesCodeReason(string Punchescode)
        {
            List<string> lstCodeDescription = new List<string>();
            var codes = Punchescode.Split(',');
            foreach (var code in codes)
            {
                switch (code)
                {
                    case "s":
                        lstCodeDescription.Add("Missing Shift End");
                        break;
                    case "m":
                        lstCodeDescription.Add("Missing Meal End");
                        break;
                    case "b":
                        lstCodeDescription.Add("Missing Break End");
                        break;
                }
            }
            return string.Join(",", lstCodeDescription);
        }

    }
}
