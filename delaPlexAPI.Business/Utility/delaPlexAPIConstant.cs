﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delaPlexAPI.Business.Utility
{
    public class DelaPlexApiConstant
    {
        public const string Success = "User created successfully.";
        public const string Failed = "User creation failed.";
    }
}
