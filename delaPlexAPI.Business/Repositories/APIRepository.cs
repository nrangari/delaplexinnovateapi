﻿using delaPlexAPI.Interfaces.Repositories;
using delaPlexAPI.Models;
using delaPlexAPI.Models.Common;
using delaPlexAPI.Services.MobileUser;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using ScheduledDetail = delaPlexAPI.Models.ScheduledDetail;

namespace delaPlexAPI.Business.Repositories
{
    public class ApiRepository : IApiRepository
    {
        readonly MobileUserService _mobileService = new MobileUserService();

        //Done
        #region My Profile
        public int GetSiteId(int employeeId)
        {
            int siteId = 0;
            try
            {
                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_API_GetSiteId";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@EmployeeId", employeeId));
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            siteId = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            return siteId;
        }

        public UserInformation GetEmployeeProfile(string employeeId)
        {
            List<UserInformation> lstUserInformation;
            using (var db = new delaPlexAPIEntities())
            {
                db.Database.Connection.Open();
                DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "delaPlex_API_GetUserProfileInformation";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@employeeId", employeeId));

                using (var reader = cmd.ExecuteReader())
                {
                    lstUserInformation = reader.MapToList<UserInformation>();
                }
            }

            UserInformation userInformation = lstUserInformation.Select(x => x).FirstOrDefault();
            return userInformation;
        }

        public UserInformation GetEmployeeInformation(int EmployeeId, string employeeRefSessionId, int siteId)
        {

            string refSessionId = _mobileService.GetRefSessionIdFromClientAdminUser();
            UserInformation userInformation = new UserInformation();

            //Keep this code for Future 
            //dynamic payrollInfo;
            //using (var client = new HttpClient())
            //{
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
            //    client.DefaultRequestHeaders.Accept.Add(
            //        new MediaTypeWithQualityHeaderValue("application/json"));
            //    client.BaseAddress = new Uri("http://retail-vm01/retail/data/retailwebapi/");
            //    var response = client.GetAsync($"api/v1-beta5/employees/{EmployeeId}")
            //        .Result;
            //    var result = response.Content.ReadAsStringAsync().Result;
            //    payrollInfo = JsonConvert.DeserializeObject<dynamic>(result);
            //}

            var employeeInformation = GetEmployeeDetails(employeeRefSessionId, siteId);
            var employmentInformation = GetEmployeeEmploymentDetails(employeeRefSessionId, siteId);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                var response = client.GetAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/users/{EmployeeId}")
                    .Result;

                var result = response.Content.ReadAsStringAsync().Result;

                dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                var UserPersonalInfo = new UserInformation
                {
                    employeeId = dynamicObject.userID,
                    // = dynamicObject.name.loginName,
                    firstName = dynamicObject.name.firstName,
                    lastName = dynamicObject.name.lastName,
                    middleName = dynamicObject.name.middleName,
                    nickName = dynamicObject.name.nickName,
                    suffix = dynamicObject.name.suffix,
                    title = dynamicObject.name.title,
                };
                var contactInfo = new UserInformation
                {
                    //  dynamicObject.contactInfo.id,
                    addressLine1 = dynamicObject.contactInfo.addressLine1,
                    addressLine2 = dynamicObject.contactInfo.addressLine2,
                    city = dynamicObject.contactInfo.city,
                    state = dynamicObject.contactInfo.state,
                    countryCode = dynamicObject.contactInfo.countryCode,
                    zipCode = dynamicObject.contactInfo.zipCode,
                    homePhone = dynamicObject.contactInfo.homePhone,
                    cellPhone = dynamicObject.contactInfo.cellPhone,
                    workPhone = dynamicObject.contactInfo.workPhone,
                    email = dynamicObject.contactInfo.email,
                    // dynamicObject.contactInfo.self,
                };

                userInformation = new UserInformation
                {
                    suffix = UserPersonalInfo.suffix,
                    title = UserPersonalInfo.title,
                    employeeId = UserPersonalInfo.employeeId,
                    firstName = UserPersonalInfo.firstName,
                    lastName = UserPersonalInfo.lastName,
                    middleName = UserPersonalInfo.middleName,
                    nickName = UserPersonalInfo.nickName,
                    addressLine1 = contactInfo.addressLine1,
                    addressLine2 = contactInfo.addressLine2,
                    city = contactInfo.city,
                    stateCode = contactInfo.state,
                    countryCode = contactInfo.countryCode,
                    zipCode = contactInfo.zipCode,
                    homePhone = contactInfo.homePhone,
                    cellPhone = contactInfo.cellPhone,
                    workPhone = contactInfo.workPhone,
                    email = contactInfo.email,
                    birthDate = employeeInformation.birthDate,
                    hireDate = employmentInformation.hireDate,
                    seniorityDate = employmentInformation.seniorityDate,
                    payRate = employmentInformation.payRate,
                    lastPunchDate = employmentInformation.lastPunchDate,
                    status = employmentInformation.status,
                    LastActivePayPeriod = employmentInformation.LastActivePayPeriod,
                    emergencyContactName = employeeInformation.emergencyContactName,
                    emergencyContactNumber = employeeInformation.emergencyContactNumber,
                    relationship = employeeInformation.relationship,
                    SSN = employeeInformation.SSN,
                    country = employeeInformation.country,
                    state = employeeInformation.state,
                    shiftPickup = employeeInformation.shiftPickup,
                    BadgeNumber = employeeInformation.BadgeNumber,
                    AddressId = employeeInformation.AddressId,
                    countryId = employeeInformation.countryId
                };
            }

            return userInformation;
        }

        private UserInformation GetEmployeeDetails(string refSessionId, int siteId)
        {
            UserInformation objUserInformation = new UserInformation();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .GetAsync($"retail/data/ess/api/EmployeeInformation/0?siteId={siteId}")
                        .Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);


                    List<string> lstNotification = new List<string>();
                    string shiftPickUp = string.Empty;
                    foreach (var notifications in dynamicObject.Notifications)
                    {

                        foreach (var method in notifications.Methods)
                        {

                            bool shift = method.Selected;
                            if (shift)
                            {
                                // shiftPickUp += VARIABLE1.ID;
                                string str = method.ID;

                                switch (str)
                                {
                                    case "Email":
                                        lstNotification.Add("Email");
                                        break;
                                    case "TextCellPhone":
                                        lstNotification.Add("Text");
                                        break;
                                    case "CallHomePhone":
                                        lstNotification.Add("Home Phone");
                                        break;
                                    case "CallWorkPhone":
                                        lstNotification.Add("Work Phone");
                                        break;
                                    case "CallCellPhone":
                                        lstNotification.Add("Cell Phone");
                                        break;
                                }

                            }
                        }
                        shiftPickUp = String.Join(",", lstNotification);

                    }

                    objUserInformation = new UserInformation
                    {
                        BadgeNumber = dynamicObject.BadgeNumber,
                        emergencyContactName = dynamicObject.EmergencyContactName,
                        emergencyContactNumber = dynamicObject.EmergencyContactPhone,
                        relationship = dynamicObject.EmergencyContactRelationship,
                        birthDate = dynamicObject.BirthDate,
                        SSN = dynamicObject.SSN,
                        AddressId = dynamicObject.Address.ID,
                        countryId = dynamicObject.Address.Country.ID,
                        country = dynamicObject.Address.Country.Name,
                        state = dynamicObject.Address.State.Name,
                        stateCode = dynamicObject.Address.State.Code,
                        shiftPickup = shiftPickUp
                    };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return objUserInformation;
        }

        private UserInformation GetEmployeeEmploymentDetails(string refSessionId, int siteId)
        {

            UserInformation objUserInformation = new UserInformation();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    HttpResponseMessage response = client
                        .GetAsync($"retail/data/ess/api/EmploymentDetails/0?siteId={siteId}")
                        .Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    DateTime payPeriodStart = dynamicObject.LastActivePayPeriodStart;
                    DateTime payPeriodend = dynamicObject.LastActivePayPeriodEnd;


                    objUserInformation = new UserInformation
                    {
                        hireDate = dynamicObject.HireDate,
                        seniorityDate = dynamicObject.SeniorityDate,
                        payRate = dynamicObject.PayRate,
                        lastPunchDate = dynamicObject.LastPunchedShiftDate,
                        status = dynamicObject.IsActive,
                        LastActivePayPeriod = string.Concat(payPeriodStart.ToString("MM-dd-yyyy"), " - ", payPeriodend.ToString("MM-dd-yyyy"))
                    };
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return objUserInformation;
        }

        public string UpdateUserProfileInformation(UpdateUserInformation updateUserInformation)
        {
            string result = "Error";
            MobileUserService mobileService = new MobileUserService();
            try
            {
                string refSessionId = mobileService.GetRefSessionIdFromClientAdminUser();
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response1 = client1.PutAsJsonAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/users/{updateUserInformation.id}/contactInfo",
                        updateUserInformation).Result;
                    if (response1.StatusCode == HttpStatusCode.OK)
                    {
                        result = "Success";
                        UpdateEmergencyContactInformation(updateUserInformation);
                    }
                    else
                    {
                        IEnumerable<string> values;
                        string session = string.Empty;
                        if (response1.Headers.TryGetValues("REFS-Message", out values))
                        {
                            result = values.FirstOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        private void UpdateEmergencyContactInformation(UpdateUserInformation updateUserInformation)
        {
            using (var db = new delaPlexAPIEntities())
            {
                db.Database.Connection.Open();
                DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "delaPlex_API_UpdateUserEmergencyContactInformation";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@employeeId", updateUserInformation.id));
                cmd.Parameters.Add(new SqlParameter("@emergency_contact_name",
                    updateUserInformation.emergencyContactName));
                cmd.Parameters.Add(new SqlParameter("@emergency_contact_phone_num",
                    updateUserInformation.emergencyContactNumber));
                cmd.Parameters.Add(new SqlParameter("@emergency_contact_relationship_code",
                    updateUserInformation.relationship));
                cmd.Parameters.Add(new SqlParameter("@contact_method_type_code", updateUserInformation.shiftPickup));
                cmd.ExecuteNonQuery();
            }
        }

        public List<State> GetStateList(int employeeId)
        {
            List<State> lstStates;
            using (var db = new delaPlexAPIEntities())
            {
                db.Database.Connection.Open();
                DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "delaPlex_API_GetCountryState";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@employeeId", employeeId));

                using (var reader = cmd.ExecuteReader())
                {
                    lstStates = reader.MapToList<State>();
                }
            }
            return lstStates;
        }

        public List<State> GetStates(int siteId, int countryId, string refSessionId)
        {
            MobileUserService objMobileUserService = new MobileUserService();
            return objMobileUserService.GetStates(siteId, countryId, refSessionId);
        }

        public List<ShiftPickUp> GetShiftPickUp()
        {
            return ShiftPickUpInformation.GetShiftPickList();
        }

        public List<EmergencyContactRelationship> GetEmergencyContactRelationshipCode()
        {
            List<EmergencyContactRelationship> lstEmergencyContactRelationship;
            using (var db = new delaPlexAPIEntities())
            {
                db.Database.Connection.Open();
                DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "delaPlex_API_GetEmergencyRelationShip";
                cmd.CommandType = CommandType.StoredProcedure;
                using (var reader = cmd.ExecuteReader())
                {
                    lstEmergencyContactRelationship = reader.MapToList<EmergencyContactRelationship>();
                }
            }

            return lstEmergencyContactRelationship;
        }

        public string UpdateEmployeeInformation(EmployeeInformation employeeInformation)
        {
            MobileUserService obj = new MobileUserService();
            return obj.UpdateEmployeeInformation(employeeInformation);
        }

        #endregion


        #region TimeOffRequest

        public string CreateTimeOffRequest_v1(TimeOffRequest timeOff)
        {
            MobileUserService mobileService = new MobileUserService();
            return mobileService.CreateTimeOffRequest(timeOff);
        }

        public string CreateTimeOffRequest(TimeOffRequest timeOff)
        {
            MobileUserService mobileService = new MobileUserService();
            string result = "Error";
            try
            {
                string refSessionId = mobileService.GetRefSessionIdFromClientAdminUser();
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response1 = client1.PostAsJsonAsync("retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + "/timeOffRequests?ignoreWarnings=false", timeOff).Result;

                    if (response1.StatusCode == HttpStatusCode.Created)
                    {
                        result = "Success";
                    }
                    else
                    {
                        IEnumerable<string> values;
                        string session = string.Empty;
                        if (response1.Headers.TryGetValues("REFS-Message", out values))
                        {
                            result = values.FirstOrDefault();
                        }

                        //Ignore code to get the exception
                        int start = result.IndexOf("{");
                        result = result.Substring(start);
                        result = result.Substring(0, result.IndexOf(")"));
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        public string UpdateTimeOffRequest_v1(TimeOffRequest timeOff)
        {
            MobileUserService mobileService = new MobileUserService();
            return mobileService.UpdateTimeOffRequest(timeOff);
        }

        public string UpdateTimeOffRequest(TimeOffRequest timeOff)
        {
            string result = "Error";
            MobileUserService mobileService = new MobileUserService();
            try
            {
                string refSessionId = mobileService.GetRefSessionIdFromClientAdminUser();
                using (var client1 = new HttpClient())
                {
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response1 = client1.PutAsJsonAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/timeOffRequests/{timeOff.timeOffRequestID}?ignoreWarnings=false", timeOff).Result;

                    if (response1.StatusCode == HttpStatusCode.OK)
                    {
                        result = "Success";
                    }
                    else
                    {
                        IEnumerable<string> values;
                        string session = string.Empty;
                        if (response1.Headers.TryGetValues("REFS-Message", out values))
                        {
                            result = values.FirstOrDefault();
                        }

                        //Ignore code to get the exception
                        int start = result.IndexOf("{");
                        result = result.Substring(start);
                        result = result.Substring(0, result.IndexOf(")"));
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }

        public string CancelTimeOffRequest(TimeOffRequest timeOff)
        {
            MobileUserService mobileService = new MobileUserService();
            return mobileService.CancelTimeOffRequest(timeOff);
        }

        public List<TimeOff> GetMyTimeOff(int employeeId, string flag)
        {
            List<TimeOff> lstTimeOffs = new List<TimeOff>();
            List<TimeOffType> lstTimeOffTypes;
            List<TimeOffApprover> lstTimeOffApprover;
            DateTime todayDate = DateTime.Now.AddDays(-1);
            try
            {

                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_API_GetTimeOffType";
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (var reader = cmd.ExecuteReader())
                    {
                        lstTimeOffTypes = reader.MapToList<TimeOffType>();
                    }
                }

                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_API_GetTimeOffApprover";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@employeeId", employeeId));
                    using (var reader = cmd.ExecuteReader())
                    {
                        lstTimeOffApprover = reader.MapToList<TimeOffApprover>();
                    }
                }

                string refSessionId = _mobileService.GetRefSessionIdFromClientAdminUser();
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client.GetAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/timeOffRequests/employees/{employeeId}/years/0")
                        .Result;

                    var result = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    foreach (var o in dynamicObject.timeOffRequestCollection)
                    {
                        string timeOffType = lstTimeOffTypes
                            .Where(x => x.timeOffTypeID == Convert.ToInt32(o.timeOffTypeID))
                            .Select(x => x.timeOffType).FirstOrDefault();

                        string LastModifiedBy = lstTimeOffApprover.Where(x => x.TimeOffId == Convert.ToInt32(o.timeOffRequestID))
                            .Select(x => x.LastModifiedBy).FirstOrDefault();


                        var timeOff = new TimeOff
                        {
                            employeeID = o.employeeID,
                            timeOffRequestID = o.timeOffRequestID,
                            timeOffTypeID = o.timeOffTypeID,
                            comments = o.comments,
                            end = o.end,
                            start = o.start,
                            hoursRequested = o.hoursRequested,
                            status = o.status,
                            timeOffType = timeOffType,
                            LastModifiedBy = LastModifiedBy
                        };
                        lstTimeOffs.Add(timeOff);
                    }
                }
            }
            catch (Exception ex)
            {
                return lstTimeOffs;
            }

            // h is History , f is future
            DateTime TodayDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            if (flag == "h")
            {
                return lstTimeOffs.Where(x => x.end < TodayDate).Select(x => x).OrderByDescending(x => x.start)
                    .ToList();
            }
            else
            {
                return lstTimeOffs.Where(x => x.start >= TodayDate).Select(x => x).OrderBy(x => x.start).ToList();
            }
        }


        public List<TimeOff> GetMyHistoryTimeOff(int siteId, string refSessionId)
        {
            //return _mobileService.GetMyHistoryTimeOff(siteId, refSessionId);

            List<TimeOff> lstTimeOffs = new List<TimeOff>();
            List<TimeOffType> lstTimeOffType = _mobileService.GetAdjustmentTypes(siteId, refSessionId);

            using (var client1 = new HttpClient())
            {
                client1.DefaultRequestHeaders.Accept.Clear();
                client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client1.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                var data = new
                {
                    endDate = DateTime.Now,
                };

                var response = client1.PostAsJsonAsync("retail/data/ess/services/TimeOffRequest.asmx/GetRequests?siteId=" + siteId, data).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                foreach (var o in dynamicObject.data)
                {

                    string statusCode = o.StatusCode;
                    string status = TimeOffRequestStatusCode.TimeOffStatusCode(statusCode);

                    string timeoffType = o.PayAdjustmentName;
                    int TimetypeID = lstTimeOffType.Where(x => x.timeOffType == timeoffType)
                        .Select(x => x.timeOffTypeID).FirstOrDefault();

                    var timeOffs = new TimeOff
                    {
                        employeeID = o.EmployeeID,
                        timeOffRequestID = o.ID,
                        timeOffTypeID = TimetypeID,// o.PayAdjustmentID,
                        comments = o.comment,
                        end = o.End,
                        start = o.Start,
                        hoursRequested = 0,
                        status = status,
                        timeOffType = o.PayAdjustmentName,
                        LastModifiedBy = o.LastModifiedUserName
                    };
                    lstTimeOffs.Add(timeOffs);
                }
            }
            return lstTimeOffs;
        }

        public List<TimeOffType> GetAdjustmentTypes(int siteId, string refSessionId)
        {
            return _mobileService.GetAdjustmentTypes(siteId, refSessionId);
        }

        public List<TimeOff> GetFutureTimeOff(int employeeId, string refSessionId, int siteId)
        {
            List<TimeOff> lstTimeOffs = new List<TimeOff>();
            List<TimeOffType> lstTimeOffType = _mobileService.GetAdjustmentTypes(siteId, refSessionId);

            using (var client1 = new HttpClient())
            {
                client1.DefaultRequestHeaders.Accept.Clear();
                client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client1.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                var data = new
                {
                    startDate = DateTime.Now,
                };

                var response = client1.PostAsJsonAsync("retail/data/ess/services/TimeOffRequest.asmx/GetRequests?siteId=" + siteId, data).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                foreach (var o in dynamicObject.data)
                {

                    string statusCode = o.StatusCode;
                    string status = TimeOffRequestStatusCode.TimeOffStatusCode(statusCode);

                    string timeoffType = o.PayAdjustmentName;
                    int TimetypeID = lstTimeOffType.Where(x => x.timeOffType == timeoffType)
                        .Select(x => x.timeOffTypeID).FirstOrDefault();

                    var timeOffs = new TimeOff
                    {
                        employeeID = o.EmployeeID,
                        timeOffRequestID = o.ID,
                        timeOffTypeID = TimetypeID,// o.PayAdjustmentID,
                        comments = o.comment,
                        end = o.End,
                        start = o.Start,
                        hoursRequested = 0,
                        status = status,
                        timeOffType = o.PayAdjustmentName,
                        LastModifiedBy = o.LastModifiedUserName
                    };
                    lstTimeOffs.Add(timeOffs);
                }
            }
            return lstTimeOffs;
        }

        public int GetRTOPeriod()
        {
            int minDaysRequired = 0;
            using (var db = new delaPlexAPIEntities())
            {
                db.Database.Connection.Open();
                DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "delaPlex_API_getRTOPeriod";
                cmd.CommandType = CommandType.StoredProcedure;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        minDaysRequired = reader.GetInt32(0);
                    }
                }
            }

            return minDaysRequired;
        }

        public TimeOffRequestValidation GetRTOandTorPeriod(int siteId, string refSessionId)
        {

            //List<TimeOffRequestValidation> lstTimeOffRequestValidation = new List<TimeOffRequestValidation>();
            //using (var db = new delaPlexAPIEntities())
            //{
            //    db.Database.Connection.Open();
            //    DbCommand cmd = db.Database.Connection.CreateCommand();
            //    cmd.CommandText = "delaPlex_API_getRTOPeriod";
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    using (var reader = cmd.ExecuteReader())
            //    {
            //        lstTimeOffRequestValidation = reader.MapToList<TimeOffRequestValidation>();
            //    }
            //}
            //return lstTimeOffRequestValidation.Select(x => x).FirstOrDefault();

            //ToDo
            //Remove the SQL code and uncomment the below code
            //http://retail-vm01/retail/data/ess/services/TnA.asmx/GetClientSettings?siteId=1000108 

            MobileUserService mobileUserService = new MobileUserService();
            return mobileUserService.GetRTOandTorPeriod(siteId, refSessionId);
        }


        #endregion


        #region UnPaired Punches

        public List<MyUnpairedPunches> LstMyUnpairedPunches { get; } = new List<MyUnpairedPunches>();

        public List<MyUnpairedPunches> GetUnpairedPunches(string refSessionId, int siteId)//(int employeeId)
        {

            //List<MyUnpairedPunches> lstMyUnpairedPunches;
            //using (var db = new delaPlexAPIEntities())
            //{
            //    db.Database.Connection.Open();
            //    DbCommand cmd = db.Database.Connection.CreateCommand();
            //    cmd.CommandText = "delaPlex_Api_GetUnpairedPunches";
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.Add(new SqlParameter("@emp_id", employeeId));

            //    using (var reader = cmd.ExecuteReader())
            //    {
            //        lstMyUnpairedPunches = reader.MapToList<MyUnpairedPunches>();
            //    }
            //}

            //return lstMyUnpairedPunches ?? new List<MyUnpairedPunches>();

            return _mobileService.GetUnpairedPuncheses(refSessionId, siteId);
        }

        #endregion


        #region MySchedule

        public string GetMySchedule(int employeeId, DateTime date)
        {
            string result = string.Empty;
            try
            {
                string refSessionId = _mobileService.GetRefSessionIdFromClientAdminUser();
                using (var client1 = new HttpClient())
                {
                    string date1 = date.ToString("yyyy-MM-dd");
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client1.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client1
                        .GetAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/scheduledShifts/employee/{employeeId}/laborWeek/{date1}").Result;
                    result = response.Content.ReadAsStringAsync().Result;

                }
            }
            catch
            {
                // ignored
                return result;
            }

            return result;

        }

        public List<ShiftSwap> GetMyShiftSwaps(int employeeId)
        {
            List<ShiftSwap> lstMyShiftsShiftSwaps;
            using (var db = new delaPlexAPIEntities())
            {
                db.Database.Connection.Open();
                DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "delaPlex_API_GetRequestedShiftSwap";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@employeeId", employeeId));

                using (var reader = cmd.ExecuteReader())
                {
                    lstMyShiftsShiftSwaps = reader.MapToList<ShiftSwap>();
                }
            }

            return lstMyShiftsShiftSwaps ?? new List<ShiftSwap>();
        }

        public List<MyEditedPunches> GetMyEditedPunches(int employeeId)
        {
            List<MyEditedPunches> lstEditedPunches;
            using (var db = new delaPlexAPIEntities())
            {
                db.Database.Connection.Open();
                DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "delaPlex_API_GetEditedPunches";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@employeeId", employeeId));

                using (var reader = cmd.ExecuteReader())
                {
                    lstEditedPunches = reader.MapToList<MyEditedPunches>();
                }
            }

            return lstEditedPunches ?? new List<MyEditedPunches>();
        }

        public List<ScheduleDates> GetMonthlyScheduleDate(int month)
        {
            List<ScheduleDates> lstList = new List<ScheduleDates>();
            try
            {
                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_API_GetMonthlyScheduleDate";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Month", month));
                    using (var reader = cmd.ExecuteReader())
                    {
                        lstList = reader.MapToList<ScheduleDates>();
                    }
                }
            }
            catch (Exception ex)
            {
                return lstList;
            }

            return lstList;
        }

        private List<ScheduleHours> GetScheduledHours(int employeeId)
        {
            List<ScheduleHours> lstScheduleHours;
            using (var db = new delaPlexAPIEntities())
            {
                db.Database.Connection.Open();
                DbCommand cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "delaPlex_API_GetDailyScheduledhours";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@employeeId", employeeId));
                using (var reader = cmd.ExecuteReader())
                {
                    lstScheduleHours = reader.MapToList<ScheduleHours>();
                }
            }

            return lstScheduleHours;
        }

        public List<ScheduledJobs> GetMyScheduleFormatted(int employeeId, DateTime date)
        {
            List<ScheduledJobs> lstScheduledJobs = new List<ScheduledJobs>();
            List<ScheduledDetail> lstScheduledDetails = new List<ScheduledDetail>();
            List<ScheduleHours> lstScheduleHours = GetScheduledHours(employeeId);
            try
            {
                string refSessionId = _mobileService.GetRefSessionIdFromClientAdminUser();
                using (var client = new HttpClient())
                {
                    string date1 = date.ToString("yyyy-MM-dd");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client
                        .GetAsync($"retail/data/retailwebapi/api/" + AppSettings.JdaApiVersion + $"/scheduledShifts/employee/{employeeId}/laborWeek/{date1}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;

                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);


                    foreach (var scheduledShifts in dynamicObject.scheduledShifts)
                    {

                        foreach (var scheduledJobs in scheduledShifts.scheduledJobs)
                        {
                            lstScheduledDetails = new List<Models.ScheduledDetail>();
                            foreach (var sch in scheduledJobs.scheduledDetail)
                            {
                                var scheduleDetails = new ScheduledDetail
                                {
                                    detailTypeCode = sch.detailTypeCode,
                                    startTime = sch.startTime,
                                    endTime = sch.endTime
                                };

                                if (scheduleDetails.detailTypeCode != "Role")
                                {
                                    lstScheduledDetails.Add(scheduleDetails);
                                }
                            }

                            int scheduledShiftId = scheduledShifts.scheduledShiftId;
                            string hours = lstScheduleHours.Where(x => x.ShiftId == scheduledShiftId)
                                .Select(x => x.Hours).FirstOrDefault();

                            var scheduleJobs = new ScheduledJobs
                            {
                                scheduledShiftId = scheduledShifts.scheduledShiftId,
                                startTime = scheduledShifts.startTime,
                                endTime = scheduledShifts.endTime,
                                Hours = hours,
                                scheduledDetails = lstScheduledDetails
                            };

                            lstScheduledJobs.Add(scheduleJobs);
                        }

                        //var scheduleJobs = new ScheduledJobs
                        //{
                        //    scheduledShiftId = scheduledShifts.scheduledShiftId,
                        //    startTime = scheduledShifts.startTime,
                        //    endTime = scheduledShifts.endTime,
                        //    scheduledDetails = lstScheduledDetails
                        //};

                        //lstScheduledJobs.Add(scheduleJobs);
                    }
                }
            }
            catch (Exception ex)
            {
                // ignored
                return lstScheduledJobs;
            }

            return lstScheduledJobs;

        }

        public List<RequestedByMe> RequestedByMe(int employeeId)
        {
            List<RequestedByMe> lstRequestedByMe = new List<RequestedByMe>();
            try
            {
                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_api_GetRequestedByMe";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@SwapperId", employeeId));
                    using (var reader = cmd.ExecuteReader())
                    {
                        lstRequestedByMe = reader.MapToList<RequestedByMe>();
                    }
                }
            }
            catch (Exception e)
            {
                return lstRequestedByMe;
            }

            return lstRequestedByMe;

        }

        public List<PendingMyApprovals> GetPendingMyApprovals(int employeeId)
        {
            List<PendingMyApprovals> lstPendingMyApprovals = new List<PendingMyApprovals>();
            try
            {
                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_api_GetPendingMyApprovals";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@SwappeeId", employeeId));
                    using (var reader = cmd.ExecuteReader())
                    {
                        lstPendingMyApprovals = reader.MapToList<PendingMyApprovals>();
                    }
                }
            }
            catch (Exception e)
            {
                return lstPendingMyApprovals;
            }

            return lstPendingMyApprovals;
        }

        public List<AvailableShifts> GetAvailableShift(int employeeId)
        {
            List<AvailableShifts> lstAvailableShifts = new List<AvailableShifts>();
            try
            {
                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_API_GetAvailableShift";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@EmployeeId", employeeId));
                    using (var reader = cmd.ExecuteReader())
                    {
                        lstAvailableShifts = reader.MapToList<AvailableShifts>();
                    }
                }
            }
            catch (Exception e)
            {
                return lstAvailableShifts;
            }

            return lstAvailableShifts;

        }

        public List<AvailableShifts> GetUnfilledShift(int employeeId, DateTime date, int siteId, string refSessionId)
        {
            List<AvailableShifts> lstAvailableShifts = new List<AvailableShifts>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                    var response = client
                        .GetAsync($"retail/data/ess/api/AvailableUnfilledShifts?selectedWeekDay={date.ToString("yyyy-MM-dd")}&siteId={siteId}").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);

                    foreach (var o in dynamicObject)
                    {
                        var availableShifts = new AvailableShifts
                        {
                            ShiftId = o.ShiftID,
                            Date = o.Start.ToString("yyyy-MM-dd"),
                            ShiftStart = o.Start.ToString("HH:mm"),
                            ShiftEnd = o.End.ToString("HH:mm"),
                            Site = o.SiteName,
                            Job = o.JobNames
                        };
                        lstAvailableShifts.Add(availableShifts);
                    }
                }
            }
            catch (Exception e)
            {
                return lstAvailableShifts;
            }

            return lstAvailableShifts;

        }

        public MyTimeCard GetMyTimeCard(string refSessionId, int siteId, DateTime date)
        {
            return _mobileService.GetMyTimeCard(refSessionId, siteId, date);
        }

        public decimal GetScheduledHours(string refSessionId, int siteId, DateTime date)
        {
            return _mobileService.GetScheduledHours(refSessionId, siteId, date);
        }

        public string ClaimAvailableShift(ClaimShift claimShift)
        {
            string result = string.Empty;
            try
            {
                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_API_Update_ClaimAvailableShift";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@EmployeeId", claimShift.EmployeeId));
                    cmd.Parameters.Add(new SqlParameter("@ShiftId", claimShift.scheduledShiftId));
                    using (var reader = cmd.ExecuteReader())
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception e)
            {
                result = e.Message.ToString();

            }

            return result;
            // return _mobileService.ClaimAvailableShift(claimShift);
        }

        public List<ShiftSwap> ShiftSwapAvailableShift(string refSessionId, int shiftID, int siteId)
        {
            return _mobileService.ShiftSwapAvailableShift(refSessionId, shiftID, siteId);

        }

        public string ProcessShiftSwap(ProcessShiftSwap processShiftSwap)
        {
            return _mobileService.ProcessShiftSwap(processShiftSwap);
        }

        public string UpdateSwapShiftRequest(UpdateShiftSwap updateShiftSwap)
        {
            return _mobileService.UpdateSwapShiftRequest(updateShiftSwap);
        }
        public List<PendingMyApprovals> GetPendingApprovals(int siteId, string refSesssionId)
        {
            MobileUserService obj = new MobileUserService();
            return obj.GetPendingApprovals(siteId, refSesssionId);
        }

        public List<RequestedByMe> RequestedByMe(int siteId, string refSesssionId)
        {
            MobileUserService obj = new MobileUserService();
            return obj.RequestedByMe(siteId, refSesssionId);
        }

        #endregion


        #region Notification

        public List<MyNotification> GetMyNotification(string refSessionId, int siteId)
        {
            return _mobileService.GetMyNotification(refSessionId, siteId);
        }

        public string DeleteMyNotification(DeleteNotificationParam deleteNotificationParam)
        {
            return _mobileService.DeleteMyNotification(deleteNotificationParam);
        }

        public string MarkNotificationReadUnread(UpdateNotification updateNotification)
        {
            string result = string.Empty;
            try
            {
                string flag = updateNotification.read ? "y" : "n";
                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_API_MarkNotificationReadUnread";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@MailId", updateNotification.MailID));
                    cmd.Parameters.Add(new SqlParameter("@pendingFlag", flag));
                    using (var reader = cmd.ExecuteReader())
                    {
                        result = "Success";
                    }
                }
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }

            return result;
        }


        #endregion 


        #region Edited Punches

        public List<EditedPunches> GetEditedPunches(string refSessionId, int siteId, DateTime date, string statusCode)
        {
            List<EditedPunches> lstEditedPunches = new List<EditedPunches>();
            try
            {
                //var data = new
                //{
                //    date = string.Concat(date.ToString("yyyy-MM-dd"), "T00:00:00"),
                //    //"2018-10-11T00:00:00",
                //    statusCode = statusCode
                //};

                //using (var client1 = new HttpClient())
                //{
                //    client1.DefaultRequestHeaders.Accept.Clear();
                //    client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                //    client1.DefaultRequestHeaders.Accept.Add(
                //        new MediaTypeWithQualityHeaderValue("application/json"));
                //    client1.BaseAddress = new Uri("http://retail-vm01/retail/data/ess/services/TnA.asmx/");
                //    var response = client1.PostAsJsonAsync("GetPunchEdits?siteId=" + siteId, data).Result;
                //    var result = response.Content.ReadAsStringAsync().Result;
                //{ACKNOWLEDGED:"a",UNREAD:"i",DISPUTED:"r",UNAPPROVED:"u",CLOSED:"c"

                dynamic dynamicObject = statusCode == "a" ? GetAcknowldeResult(refSessionId, siteId, date, statusCode) : GetUndreadAndDisputeResult(refSessionId, siteId, date, statusCode); //JsonConvert.DeserializeObject<dynamic>(result);
                foreach (var o in dynamicObject.data)
                {
                    string PunchTypeCode = o.PunchTypeCode;

                    var EditedPunch = new EditedPunches
                    {
                        ScheduledShiftEndTime = o.ScheduledShiftEndTime == "" ? null : o.ScheduledShiftEndTime,
                        Comments = o.Comments,
                        ScheduledShiftStartTime =
                            o.ScheduledShiftStartTime == "" ? null : o.ScheduledShiftStartTime,
                        ScheduledJob = o.ScheduledJob,
                        PunchTypeCode = o.PunchTypeCode,
                        StatusCode = o.StatusCode,
                        Reason = o.Reason,
                        NewPunchTime = o.NewPunchTime == "" ? null : o.NewPunchTime,
                        ChangeType = GetPunchTypeCode(PunchTypeCode),// o.ChangeType,
                        EditedOn = o.EditedOn,
                        OldJob = o.OldJob,
                        ID = o.ID,
                        OldPunchTime = o.OldPunchTime == "" ? null : o.OldPunchTime,
                        NewJob = o.NewJob,
                        EditedBy = o.EditedBy,
                        PunchDate = o.PunchDate == null ? "" : o.PunchDate
                    };
                    lstEditedPunches.Add(EditedPunch);
                }

                // result = response1.StatusCode == HttpStatusCode.OK ? "Success" : "Error";
                //}
            }
            catch (Exception ex)
            {
                return lstEditedPunches;
            }

            return lstEditedPunches;
        }


        private string GetPunchTypeCode(string code)
        {
            //SHIFT_START:"s",SHIFT_END:"t",MEAL_START:"m",MEAL_END:"l",BREAK_START:"b",BREAK_END:"k",JOB_TRANSFER:"j"
            //   SHIFT_START: "s",SHIFT_END: "t",MEAL_START: "m",MEAL_END: "l",BREAK_START: "b",BREAK_END: "k",JOB_TRANSFER: "j"
            switch (code)
            {
                case "s":
                    return "Shift Start";
                case "t":
                    return "Shift End";
                case "m":
                    return "Meal Start";
                case "l":
                    return "Meal End";
                case "b":
                    return "Break Start";
                case "k":
                    return "Break End";
                case "j":
                    return "Job Transfer";
            }

            return string.Empty;
        }
        private dynamic GetUndreadAndDisputeResult(string refSessionId, int siteId, DateTime date, string statusCode)
        {
            dynamic dynamicObject;
            var data = new
            {
                statusCode = statusCode
            };
            using (var client1 = new HttpClient())
            {
                client1.DefaultRequestHeaders.Accept.Clear();
                client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client1.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                var response = client1.PostAsJsonAsync("retail/data/ess/services/TnA.asmx/GetPunchEdits?siteId=" + siteId, data).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
            }
            return dynamicObject;
        }

        private dynamic GetAcknowldeResult(string refSessionId, int siteId, DateTime date, string statusCode)
        {
            dynamic dynamicObject;
            var data = new
            {
                date = string.Concat(date.ToString("yyyy-MM-dd"), "T00:00:00"),
                //"2018-10-11T00:00:00",
                statusCode = statusCode
            };
            using (var client1 = new HttpClient())
            {
                client1.DefaultRequestHeaders.Accept.Clear();
                client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client1.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                var response = client1.PostAsJsonAsync("retail/data/ess/services/TnA.asmx/GetPunchEdits?siteId=" + siteId, data).Result;
                var result = response.Content.ReadAsStringAsync().Result;


                dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
            }

            return dynamicObject;
        }

        public List<PunchTypeCode> GetPunchTypeCode()
        {
            List<PunchTypeCode> lstPunchTypeCode = new List<PunchTypeCode>();

            try
            {
                using (var db = new delaPlexAPIEntities())
                {
                    db.Database.Connection.Open();
                    DbCommand cmd = db.Database.Connection.CreateCommand();
                    cmd.CommandText = "delaPlex_API_GetPuncheTypeCode";
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (var reader = cmd.ExecuteReader())
                    {
                        lstPunchTypeCode = reader.MapToList<PunchTypeCode>();
                    }
                }
            }
            catch (Exception e)
            {
                return lstPunchTypeCode;
            }
            return lstPunchTypeCode;
        }


        #endregion


        #region Availability

        public string UpdateEmployeeAvailabilityRequest(EmployeeAvailabilityPrivateApi cancelAvailabilityRequest)
        {
            return _mobileService.UpdateEmployeeAvailabilityRequest(cancelAvailabilityRequest);
        }
        public List<MyAvailabilityRequest> GetMyAvailabilityRequest(string refSessionId, int siteId)
        {

            List<MyAvailabilityRequest> lstAvailabilityRequests = new List<MyAvailabilityRequest>();
            using (var client1 = new HttpClient())
            {
                client1.DefaultRequestHeaders.Accept.Clear();
                client1.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                client1.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client1.BaseAddress = new Uri(AppSettings.JdaServerUrl);
                var response = client1.GetAsync("retail/data/ess/api/myavailabilityrequest?siteId=" + siteId).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                foreach (var o in dynamicObject)
                {

                    List<AvailabilityRequestRange> lstAvailabilityRequestRange =
                        new List<AvailabilityRequestRange>();

                    foreach (var AvailabilityRequestRange in o.AvailabilityRequestRange)
                    {
                        var availabilityRequestRange = new AvailabilityRequestRange
                        {
                            ID = AvailabilityRequestRange.ID,
                            DayOfWeek = AvailabilityRequestRange.DayOfWeek,
                            StartOffset = AvailabilityRequestRange.StartOffset,
                            EndOffset = AvailabilityRequestRange.EndOffset,
                            AvailabilityType = AvailabilityRequestRange.AvailabilityType
                        };
                        lstAvailabilityRequestRange.Add(availabilityRequestRange);
                    }


                    //To match the status with persona
                    string status = o.Status;
                    status = status == "RequestedPreferred" ? "Awaiting Approval" : status;

                    var AvailabilityRequests = new MyAvailabilityRequest
                    {
                        ID = o.ID,
                        Start = o.Start,
                        LastStatusUpdateUserID = o.LastStatusUpdateUserID,
                        LastStatusUpdateUserName = o.LastStatusUpdateUserName,
                        LastStatusUpdateTimestamp = o.LastStatusUpdateTimestamp,
                        RequestedTimestamp = o.RequestedTimestamp,
                        End = o.End,
                        Comment = o.Comment,
                        Status = status,
                        AvailabilityRequestRange = lstAvailabilityRequestRange
                    };

                    lstAvailabilityRequests.Add(AvailabilityRequests);

                }
            }

            return lstAvailabilityRequests;
        }

        public string CreateEmployeeAvailability(EmployeeAvailability availability)
        {
            return _mobileService.CreateEmployeeAvailability(availability);
        }

        public EmployeeAvailability GetMyCurrentAvailability(int Employeeid, DateTime date)
        {
            return _mobileService.GetMyCurrentAvailability(Employeeid, date);
        }

        public string CreateEmployeeAvailabilityUsingPrivateAPI(EmployeeAvailabilityPrivateApi availability)
        {
            MobileUserService obj = new MobileUserService();
            return obj.CreateEmployeeAvailabilityUsingPrivateAPI(availability);
        }


        public EmployeeNewAvailability GetMyAvailability(int siteId, string refSessionId)//(int Employeeid, DateTime date)
        {
            MobileUserService objMobileUserService = new MobileUserService();
            //return objMobileUserService.GetMyAvailability(Employeeid, date);
            return objMobileUserService.GetMyAvailability(siteId, refSessionId);
            //(int siteId, string refSessionId)
        }

        #endregion


        #region Accurals

        public List<EmployeeAccruals> GetEmployeeAccruals(int employeeId)
        {
            return _mobileService.GetEmployeeAccruals(employeeId);
        }


        #endregion

    }
}

